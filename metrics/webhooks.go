package main

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-org/release-tools/metrics/internal/handlers"
)

func initWebhooks() ([]handlers.Pluggable, []error) {
	tokenMap, errors := getJobWebhookTokens()

	return []handlers.Pluggable{handlers.NewJobWebhook(tokenMap)}, errors
}

func getJobWebhookTokens() (map[string]string, []error) {
	var errors []error
	tokens := make(map[string]string)

	for project, envVarName := range jobWebhookTokenEnvVars() {
		token, found := os.LookupEnv(envVarName)

		if !found {
			errors = append(errors, fmt.Errorf("%s not provided", envVarName))
			continue
		}
		tokens[project] = token
	}

	return tokens, errors
}

func jobWebhookTokenEnvVars() map[string]string {
	return map[string]string{
		handlers.ProjectReleaseToolsOps:          "JOB_WEBHOOK_RELEASE_TOOLS_OPS_TOKEN",
		handlers.ProjectDeployer:                 "JOB_WEBHOOK_DEPLOYER_OPS_TOKEN",
		handlers.ProjectQualityStagingCanaryOps:  "JOB_WEBHOOK_QUALITY_STAGING_CANARY_OPS_TOKEN",
		handlers.ProjectQualityStagingOps:        "JOB_WEBHOOK_QUALITY_STAGING_OPS_TOKEN",
		handlers.ProjectK8sWorkloadsGitlabComOps: "JOB_WEBHOOK_K8S_WORKLOADS_GITLAB_COM_OPS_TOKEN",
		handlers.ProjectCNGDev:                   "JOB_WEBHOOK_CNG_DEV_TOKEN",
		handlers.ProjectOmnibusDev:               "JOB_WEBHOOK_OMNIBUS_DEV_TOKEN",
	}
}
