package pipelines

import (
	"context"
	"fmt"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	gitlab "gitlab.com/gitlab-org/api/client-go"
)

func TestFetchPipelinesPaginationAndLimits(t *testing.T) {
	client := newMockPipelinesClient(t)
	obs := observer{
		ctx: context.Background(),
		log: logrus.New(),
		ops: client,
	}

	name := func(idx int) string {
		return fmt.Sprintf("Deployment pipeline - 42.%d.202206172020", idx)
	}
	runningPipelines := 2*limit - 1

	success := gitlab.PipelineInfo{
		Name:   name(runningPipelines + 1),
		Status: string(gitlab.Success),
		Source: "api",
	}

	apiPipelines := make([]*gitlab.PipelineInfo, 0)

	for i := 0; i < runningPipelines; i++ {
		apiPipelines = append(apiPipelines, &gitlab.PipelineInfo{
			Name:   name(runningPipelines - i),
			Status: string(gitlab.Running),
			Source: "api",
		})
	}
	apiPipelines = append(apiPipelines, &success)

	// an old pipeline that should not be counted
	apiPipelines = append(apiPipelines, &gitlab.PipelineInfo{
		Name:   name(runningPipelines + 2),
		Status: string(gitlab.Failed),
		Source: "api",
	})

	// first page
	client.On("ListProjectPipelines",
		releaseToolsProject,
		mock.MatchedBy(func(opts *gitlab.ListProjectPipelinesOptions) bool {
			return opts.Status == nil && *opts.Source == "api" &&
				opts.ListOptions.PerPage == limit
		}),
		mock.Anything).Once().Return(apiPipelines[:limit], &gitlab.Response{NextPage: 2}, nil)

	// second page
	client.On("ListProjectPipelines",
		releaseToolsProject,
		mock.MatchedBy(func(opts *gitlab.ListProjectPipelinesOptions) bool {
			return opts.Status == nil && *opts.Source == "api" &&
				opts.ListOptions.Page == 2
		}),
		mock.Anything).Once().Return(apiPipelines[limit:], &gitlab.Response{NextPage: 3}, nil)

	pipelines, err := obs.fetchPipelines()
	assert.NoError(t, err)
	assert.Lenf(t, pipelines, len(apiPipelines)-2, "expected %d pipelines, got %d. pipelines should not contain the last successful pipeline", len(apiPipelines)-2, len(pipelines))
}

func TestFetchPipelinesFiltering(t *testing.T) {
	client := newMockPipelinesClient(t)
	obs := observer{
		ctx: context.Background(),
		log: logrus.New(),
		ops: client,
	}

	apiPipelines := []*gitlab.PipelineInfo{
		// not ok: not a product version
		{
			Name:   "master",
			Status: string(gitlab.Running),
			Source: "api",
		},
		// ok: a running deployment
		{
			Name:   "Deployment pipeline - 1.3.202201012230",
			Status: string(gitlab.Running),
			Source: "api",
		},
		// limit: the last successful deployment
		{
			Name:   "Deployment pipeline - 1.2.202201012230",
			Status: string(gitlab.Success),
			Source: "api",
		},
		// not ok: past the limit
		{
			Name:   "Deployment pipeline - 1.0.202201012230",
			Status: string(gitlab.Failed),
			Source: "api",
		},
	}

	client.On("ListProjectPipelines",
		releaseToolsProject,
		mock.Anything,
		mock.Anything).Once().Return(apiPipelines, &gitlab.Response{NextPage: 2}, nil)

	pipelines, err := obs.fetchPipelines()
	assert.NoError(t, err)
	assert.Lenf(t, pipelines, 1, "this should only return the running coordinated pipeline")
}
