<!--

Please do not add any additional steps to this template. Instead, add a job to the
security_release pipeline. Development guidelines can be found in doc/release-pipelines.md.

If you make any changes to this template in Release Tools, also make sure to
update any existing release issues (if necessary). If you've edited an active release issue
please consider making the same change in the template.

-->

## First steps

- [ ] Start the `security_release_prepare:start` job in the security pipeline: <%= release_pipeline.web_url %>
   - [ ] Ensure the `security_release:prepare` stage completes before continuing to the next section.

## Two days before due date

- [ ] Start the `security_release_disable_issue_processor_schedule:start` job in the security pipeline: <%= release_pipeline.web_url %>
- [ ] Ensure the `Managed versioning check` job is executed successfully (Slack message)
  - [ ] Check the comments of the [security tracking issue](<%= security_release_tracking_issue&.web_url %>) for a list of issues for [projects under GitLab managed versioning](https://gitlab.com/gitlab-org/release/docs/-/tree/master/components/managed-versioning#components-under-managed-versioning).
  - [ ] If there are any, modify this issue to include the list of issues, and the manual steps to process them. Instructions are provided in the comment.
  - [ ] If a Gitaly or a KAS security fix is included in the upcoming patch release, modify this issue to follow the steps in the "[How to deal with Gitaly and KAS security fixes](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_kas_security_merge_requests.md)" guide. These steps can be run in parallel, or after the `security_release_early_merge:start` step.
- [ ] Start the `security_release_early_merge:start` job of the security pipeline: <%= release_pipeline.web_url %>
- [ ] Verify that the table of issues in the security tracking issue has been updated showing the default MRs have
  been merged or set to Merge When Pipeline Succeeds (MWPS/auto-merge).

## One day before the due date

If this date is on a weekend, do this work on the next working day.

- [ ] Verify successful merge of the MWPS MRs by:
  - Successful job completion of `security_release_early_merge:verify_pending_merge` job of the security pipeline: <%= release_pipeline.web_url%>. There should be a slack message in `#f_upcoming_release` about "Pending merge" task completing successfully.
- [ ] Start the `security_release_early_merge:verify_pending_deploy` job of the security pipeline: <%= release_pipeline.web_url %>
- [ ] Ensure the `security_release_early_merge:verify_pending_deploy` job completed successfully.
   NOTE: This only checks `gitlab-org/security/gitlab`. If other projects have security MRs you should verify those manually.
- [ ] Make sure to [execute the post-deploy migration pipeline] to ensure that all post-deploy migrations have been executed:
  `/chatops run post_deploy_migrations execute`
- [ ] Start the `security_release_release_preparation:start` job of the security pipeline: <%= release_pipeline.web_url %>
- [ ] Start the `security_release_backport_merge:start` job of the security pipeline: <%= release_pipeline.web_url %>
- [ ] If any merge requests could not be merged, investigate what needs to be done to resolve the issues. Do **not** proceed unless it has been determined safe to do so.
- [ ] If all the security issues have been deployed to production, consider tagging. Note that after merging, the stable branch pipelines take around 3 hours to finish, so you need to wait accordingly.

## On the Due Date

### Packaging

- [ ] Start the `security_release_tag:check_component_branch_pipeline_status` job of the security pipeline to check if all branches of Managed Versioned Components succeeded: <%= release_pipeline.web_url %>
  - Note the job could fail because branch pipelines are still running or due to flaky failures. Investigate the issues and retry the job to perform a new validation.
    If the failure is deemed safe (flaky), the release process shouldn't be blocked and you're free to continue with the tagging steps.
- [ ] Start the `security_release_tag:start` job of the security pipeline to tag the patch release versions: <%= release_pipeline.web_url %>
  - `security_release_tag:check_omnibus_packages_tagging` job will be triggered 150 minutes after this job has finished. If the situation allows feel free to start it earlier.
- [ ] Verify that the `security_release_tag:check_omnibus_packages_tagging` job has completed successfully on the pipeline: <%= release_pipeline.web_url %>
- [ ] Check that the CNG Images are built. Do not play any manual jobs.
  <% versions.each do |version| -%>
  - [ ] <%= version.to_patch %>: [CNG builds](https://dev.gitlab.org/gitlab/charts/components/images/-/pipelines?ref=<%= version.tag(ee: true) %>)
  <% end %>

- [ ] Check that the Helm chart is built by making sure the `publish_tagged_package` job succeeded on each version:
  <% versions.each do |version| -%>
  - [ ] [<%= "#{helm_tag(version)}" %>](https://dev.gitlab.org/gitlab/charts/gitlab/-/pipelines/?ref=<%= "#{helm_tag(version)}" %>)
  <% end %>

### Deploy

- [ ] After tagging, the release.gitlab.net instance was automatically updated with the latest patch release. Corroborate if the deployment was successful by verifying that the `security_release_tag:verify_release_deploy` job succeeded.
  - A Slack notification with `Validate release.gitlab.net running <%= versions.first.to_patch %> has successfully executed` will be sent to `#f_upcoming_release`.

## Release

**Consider notifying to the AppSec counterpart before publishing to sync on the time of releasing the blog post. Emails to the security mailing list are normally handled as a follow up task and should not delay release tasks**

- [ ] Start the `security_release_publish:start` job of the security pipeline to publish the patch release versions: <%= release_pipeline.web_url %>
  Lists of running and failed pipelines are printed at the end of the job logs of the `security_release_publish:<version>` jobs.
  **Make sure to check the pipelines and retry any failed pipelines.**
- [ ] Verify with AppSec release managers if the blog post is ready to be published. **Do not proceed until AppSec has given green light**
- [ ] Start the `security_release_publish:move_blog_post` job of the security pipeline: <%= release_pipeline.web_url %>
- [ ] Start the `security_release_verify:start` job of the security pipeline: <%= release_pipeline.web_url %>. **Considering publishing the packages (the previous step of starting  `security_release_publish:start`) takes around 30 minutes, you might need to wait before starting this job**
- [ ] Check that the CNG Images are published. Each version has 4 tags for `ee`, `fips`, `ubi` and the branch itself.
  <% versions.each do |version| -%>
  - [ ] <%= version.to_patch %>: [CNG builds](https://dev.gitlab.org/gitlab/charts/components/images/-/tags?sort=updated_desc&search=v<%= version.to_patch %>)
  <% end %>

- [ ] Check that the Helm chart is published by making sure the `release_package` job succeeded on each version:
  <% versions.each do |version| -%>
  - [ ] [<%= "#{helm_tag(version)}" %>](https://dev.gitlab.org/gitlab/charts/gitlab/-/pipelines/?ref=<%= "#{helm_tag(version)}" %>)
  <% end %>

- [ ] Start the `security_release_create_versions:start` job of the security pipeline: <%= release_pipeline.web_url %>. After the versions are created, the `Vulnerability Type` column should indicate `No` for the new version.

### Final steps

- [ ] Start the `security_release_finalize:start` job in the security pipeline: <%= release_pipeline.web_url %>
If conflicts are found while syncing the repositories, [manual intervention will be needed to sync the repositories](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/security/manually-sync-release-tag.md).

[execute the post-deploy migration pipeline]: https://gitlab.com/gitlab-org/release/docs/-/tree/master/general/post_deploy_migration#how-to-execute-post-deploy-migrations
