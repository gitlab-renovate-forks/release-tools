#!/usr/bin/env bash

imageUrl="https://dashboards.gitlab.net/render/d-solo/delivery-deployment_slo/delivery-deployment-slo?orgId=1&theme=light&var-PROMETHEUS_DS=Global&panelId=7&width=1000&height=500&tz=UTC"

curl --silent -o deployment-slo-apdex.png -H "Authorization: Bearer $GRAFANA_API_TOKEN" "${imageUrl}"
