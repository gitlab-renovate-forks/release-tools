# frozen_string_literal: true

require_relative 'lib/release_tools'
require_relative 'lib/release_tools/tasks'

# TODO: helpers for backward compatibility
include ReleaseTools::Tasks::Helper

# monkeypatching to ensure uncaught exception are logged on elastic search
module Rake
  class Task
    include ::SemanticLogger::Loggable

    alias_method :invoke_without_log, :invoke

    def invoke(*args)
      begin
        invoke_without_log(*args)
      ensure
        last_exception = $!
        return if last_exception.nil?

        logger.fatal('Task failed', last_exception)
      end
    end
  end
end

Dir.glob('lib/tasks/*.rake').each { |task| import(task) }

# Undocumented; executed via CI schedule
task :close_expired_qa_issues do
  ReleaseTools::Qa::IssueCloser.new.execute
end

desc "Publish packages, tags, stable branches, CNG images, and Helm chart for a specified version"
task :publish, [:version] do |_t, args|
  version = get_version(args)
  all_played_pipelines = []
  all_failed_pipelines = []

  # Ensure any exceptions raised by this new service don't fail the build, since
  # all destructive behaviors are behind feature flags.
  ReleaseTools::TraceSection.collapse('Syncing remotes', icon: '🔄') do
    ReleaseTools::ErrorTracking.with_exception_captured do
      ReleaseTools::Services::SyncRemotesService
        .new(version)
        .execute
    end
  end

  # Omnibus GitLab, CNG, and Helm depends on tag having been pushed to
  # GitLab.com, so we run publishing after the remote sync.
  played_pipelines, failed_pipelines =
    ReleaseTools::TraceSection.collapse('Publishing Omnibus', icon: '📤') do
      ReleaseTools::Services::OmnibusPublishService
        .new(version)
        .execute
    end
  all_played_pipelines.concat(played_pipelines)
  all_failed_pipelines.concat(failed_pipelines)

  played_pipelines, failed_pipelines =
    ReleaseTools::TraceSection.collapse('Publishing CNG', icon: '📤') do
      ReleaseTools::Services::CNGPublishService
        .new(version)
        .execute
    end
  all_played_pipelines.concat(played_pipelines)
  all_failed_pipelines.concat(failed_pipelines)

  chart_version = ReleaseTools::Helm::HelmVersionFinder.new.execute(version)

  played_pipelines, failed_pipelines =
    ReleaseTools::TraceSection.collapse('Publishing Helm', icon: '📤') do
      ReleaseTools::Services::HelmChartPublishService
        .new(chart_version)
        .execute
    end
  all_played_pipelines.concat(played_pipelines)
  all_failed_pipelines.concat(failed_pipelines)

  # Tag new GitLab Operator version.
  operator_version = ReleaseTools::GitlabOperator::VersionFinder.new.execute(chart_version)
  ReleaseTools::TraceSection.collapse('GitLab Operator release', icon: '📦') do
    ReleaseTools::PublicRelease::GitlabOperatorRelease
      .new(operator_version, chart_version, version)
      .execute
  end

  if all_played_pipelines.any?
    puts <<~MESSAGE
      The publishing jobs have started. Please keep an eye on the following
      pipelines and make sure that they all pass:

      - #{all_played_pipelines.map(&:web_url).join("\n- ")}
    MESSAGE
  else
    puts <<~ERROR.bold.yellow
      No pipelines were found. This means none of the publishing pipelines have
      any manual jobs that hadn't been triggered yet.
    ERROR
  end

  if all_failed_pipelines.any?
    puts <<~MESSAGE.bold.red
      Some tag pipelines have failed. Please have a look at the failed pipelines
      and make sure they pass. After they pass, you can play the manual release
      job if necessary:

      - #{all_failed_pipelines.map(&:web_url).join("\n- ")}
    MESSAGE
  end
end
