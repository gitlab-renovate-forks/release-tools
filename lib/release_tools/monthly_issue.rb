# frozen_string_literal: true

module ReleaseTools
  class MonthlyIssue < Issue
    include ::SemanticLogger::Loggable

    RELEASE_TYPE = 'monthly'

    def create
      release_pipeline if monthly_release_pipeline?

      super
    end

    def title
      "Release #{version.to_minor}"
    end

    def labels
      'Monthly Release,team::Delivery'
    end

    def project
      ::ReleaseTools::Project::Release::Tasks
    end

    def assignees
      ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
    rescue ReleaseManagers::Schedule::VersionNotFoundError
      nil
    end

    def current_stable_branch
      ReleaseTools::Versions.current_stable_branch
    end

    def monthly_release_pipeline?
      Feature.enabled?(:monthly_release_pipeline)
    end

    def release_date
      @release_date ||= Date.parse(
        ReleaseTools::GitlabReleasesGemClient.upcoming_releases[version.to_minor]
      )
    end

    def formated_date(date)
      date.strftime("%A, %b %-d")
    end

    def preparation_start_day
      formated_date(release_date - 6.days)
    end

    def candidate_selection_day
      formated_date(release_date - 3.days)
    end

    def rc_tag_day
      formated_date(release_date - 2.days)
    end

    def tag_day
      formated_date(release_date - 1.day)
    end

    def release_day
      formated_date(release_date)
    end

    def ordinalized_release_date
      release_date.day.ordinalize
    end

    def helm_tag
      "v#{helm_version}"
    end

    protected

    def template_path
      File.expand_path('../../templates/monthly.md.erb', __dir__)
    end

    def helm_version
      ReleaseTools::Helm::HelmVersionFinder.new.execute(version)
    end

    def pipeline_variables
      { MONTHLY_RELEASE_PIPELINE: 'true' }
    end
  end
end
