# frozen_string_literal: true

module ReleaseTools
  class GitlabReleaseClient < GitlabClient
    RELEASE_API_ENDPOINT = 'https://release.gitlab.net/api/v4'

    def self.client
      @client ||= Gitlab.client(
        endpoint: RELEASE_API_ENDPOINT,
        private_token: ENV.fetch('RELEASE_BOT_RELEASE_TOKEN', nil),
        httparty: httparty_opts
      )
    end

    class << self
      extend Forwardable

      def_delegator :client, :version
    end

    def self.current_version
      version.version
    end
  end
end
