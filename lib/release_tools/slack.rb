# frozen_string_literal: true

require 'slack-ruby-block-kit'

require 'release_tools/slack/bookmark'
require 'release_tools/slack/webhook'
require 'release_tools/slack/utilities'
require 'release_tools/slack/auto_deploy_notification'
require 'release_tools/slack/coordinated_pipeline_diffs_notification'
require 'release_tools/slack/coordinated_pipeline_notification'
require 'release_tools/slack/coordinated_pipeline_tag_notification'
require 'release_tools/slack/chatops_notification'
require 'release_tools/slack/internal_release/package_availability_notifier'
require 'release_tools/slack/merge_train_notification'
require 'release_tools/slack/message'
require 'release_tools/slack/patch_release_merge_requests_notification'
require 'release_tools/slack/pre_notification'
require 'release_tools/slack/post_deploy_migrations_notification'
require 'release_tools/slack/post_deploy_pipeline_initial_notification'
require 'release_tools/slack/qa_notification'
require 'release_tools/slack/release_pipeline_helpers'
require 'release_tools/slack/release_pipeline_start_notifier'
require 'release_tools/slack/release_environment/deploy_notification'
require 'release_tools/slack/release_environment/qa_notification'
require 'release_tools/slack/release_job_end_notifier'
require 'release_tools/slack/search'
require 'release_tools/slack/security/app_sec_notifier'
require 'release_tools/slack/security/mirror_message'
require 'release_tools/slack/security/team_notifier'
require 'release_tools/slack/tag_notification'

module ReleaseTools
  module Slack
    NOTIFICATION_TESTS = 'C047XHV7PQQ'

    # When SLACK_TEST is set to 'true', set all notification channels to NOTIFICATION_TESTS
    if ENV.fetch('SLACK_TEST', 'false') == 'true'
      ANNOUNCEMENTS, F_AGENT_FOR_KUBERNETES, F_UPCOMING_RELEASE, G_DEDICATED_TEAM, G_DEVELOPMENT_ANALYTICS,
        G_RUNNER, GITALY_ALERTS, RELEASE_MANAGERS, RELEASES, SEC_APPSEC, S_DEVELOPER_EXPERIENCE, TEAM = [NOTIFICATION_TESTS] * 12
    else
      ANNOUNCEMENTS = 'C8PKBH3M5'
      F_AGENT_FOR_KUBERNETES = 'C015U1TKV4M'
      F_UPCOMING_RELEASE = 'C0139MAV672'
      G_DEDICATED_TEAM = 'C025LECQY0M'
      G_DEVELOPMENT_ANALYTICS = 'C064M4D2V37'
      G_RUNNER = 'CBQ76ND6W'
      GITALY_ALERTS = 'C4MU5R2MD'
      RELEASE_MANAGERS = 'S0127FU8PDE'
      RELEASES = 'C0XM5UU6B'
      SEC_APPSEC = 'CNZPN8BT3'
      S_DEVELOPER_EXPERIENCE = 'C07TWBRER7H'
      TEAM = 'T02592416'
    end

    ANNOUNCEMENTS_NAME = 'announcements'
  end
end
