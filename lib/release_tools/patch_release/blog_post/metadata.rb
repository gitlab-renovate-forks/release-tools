# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    module BlogPost
      class Metadata
        include Utils

        RELEASE_NAME = 'Patch Release'
        CRITICAL_TITLE_HEADER = 'Critical'
        SECURITY_TAG = 'security'
        SINGLE_VERSION_TEMPLATE_FILENAME = 'patch_release_single_version_template'
        MULTIPLE_VERSIONS_TEMPLATE_FILENAME = 'patch_release_template'

        attr_reader :patch_coordinator, :versions

        def initialize(patch_coordinator:, versions:)
          @patch_coordinator = patch_coordinator
          @versions = versions
        end

        def blog_title
          "GitLab #{title_header} #{release_name}: #{versions_str}".squish
        end

        def blog_description
          "Learn more about GitLab #{title_header} #{release_name}: #{versions_str} for GitLab Community Edition (CE) and Enterprise Edition (EE).".squish
        end

        def blog_tags
          SECURITY_TAG unless patch_coordinator.single_version?
        end

        def canonical_path
          return if patch_coordinator.single_version?

          date = Time.now.utc + 1.day
          "/releases/#{date.strftime('%Y/%m/%d')}/#{release_name_hyphen}-gitlab-#{hyphenated_version}-released/"
        end

        def title_header
          critical_patch_release? ? CRITICAL_TITLE_HEADER : ''
        end

        def author_gitlab
          ENV.fetch('USER', 'GITLAB_USERNAME')
        end

        def release_name
          RELEASE_NAME
        end

        def template_path
          file_name = if patch_coordinator.single_version?
                        SINGLE_VERSION_TEMPLATE_FILENAME
                      else
                        MULTIPLE_VERSIONS_TEMPLATE_FILENAME
                      end

          "templates/blog_posts/#{file_name}.html.md.erb"
        end

        private

        def critical_patch_release?
          ReleaseTools::SharedStatus.critical_patch_release?
        end
      end
    end
  end
end
