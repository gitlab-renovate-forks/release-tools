# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    module BlogPost
      # This module is responsible for providing version-related utility methods.
      # `versions` and `release_name` must be defined by the including class.
      module Utils
        def hyphenated_version
          versions.first.tr(', ', '--').tr('.', '-') if versions.present?
        end

        def versions_str
          versions.join(', ') unless versions.nil?
        end

        def release_name_hyphen
          release_name.downcase.tr(' ', '-') unless release_name.nil?
        end
      end
    end
  end
end
