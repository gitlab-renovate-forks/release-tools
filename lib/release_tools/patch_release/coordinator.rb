# frozen_string_literal: true

module ReleaseTools
  module PatchRelease
    class Coordinator
      SEVERITIES = %w(
        severity::1
        severity::2
        severity::3
        severity::4
        none
      ).freeze

      attr_reader :versions

      def initialize(version: nil)
        @versions = establish_versions(version)
      end

      # Returns a list of pressure of the last three versions per project
      # Example:
      #
      # [
      #   { version: '15.5', pressure: [{'gitlab-org/gitlab': 1},{'gitlab-org/omnibus-gitlab': 1}]},
      #   { version: '15.4', pressure: [{'gitlab-org/gitlab': 2},{'gitlab-org/omnibus-gitlab': 2}]},
      #   { version: '15.3', pressure: [{'gitlab-org/gitlab': 3},{'gitlab-org/omnibus-gitlab': 3}]}
      # ]
      def pressure_per_project
        versions.map do |version|
          version_data = { version: version.to_minor, pressure: {} }

          projects.each do |project|
            version_data[:pressure][project.path] = UnreleasedCommits.new(version, project).total_pressure
          end

          version_data
        end
      end

      # Returns a list of merge requests and pressure of the last three versions
      # Example:
      #
      # [
      #   {
      #     version: '15.5',
      #     pressure: 2,
      #     merge_requests: [{'gitlab-org/gitlab': [{'id': 1},...]},{'gitlab-org/omnibus-gitlab'...}]
      #   },
      #   {
      #     version: '15.4',
      #     pressure: 2,
      #     merge_requests: [{'gitlab-org/gitlab': [{'id': 1},...]},{'gitlab-org/omnibus-gitlab'...}]
      #   },
      #   {
      #     version: '15.3',
      #     pressure: 3,
      #     merge_requests: [{'gitlab-org/gitlab': [{'id': 1},...]},{'gitlab-org/omnibus-gitlab'...}]
      #   }
      # ]
      def merge_requests(with_patch_version: false)
        @merge_requests ||=
          versions.map do |version|
            version_data = {
              version: with_patch_version ? version : version.to_minor,
              pressure: 0,
              merge_requests: {}
            }

            projects.map do |project|
              merge_requests = UnreleasedMergeRequests.new(project, version.previous_patch).execute
              version_data[:merge_requests][project.to_s] = merge_requests
              version_data[:pressure] += merge_requests.count
            end

            version_data
          end
      end

      # Returns a list of pressure of the last three versions per severity (severity::1 to severity::4).
      # 'none' indicates merge requests without a severity label assigned
      #
      # Example:
      # [
      #   { version: '15.5', pressure: { 'severity::1': 1, 'severity::2': 1, 'severity::3': 3, 'severity::4': 2, none: 6 } },
      #   { version: '15.4', pressure: { 'severity::1': 1, 'severity::2': 1, 'severity::3': 3, 'severity::4': 2, none: 6 } },
      #   { version: '15.3', pressure: { 'severity::1': 1, 'severity::2': 1, 'severity::3': 3, 'severity::4': 2, none: 6 } }
      #  #  ]
      def pressure_per_severity
        merge_requests.map do |version_data|
          pressure = SEVERITIES.to_h { |severity| [severity, 0] }

          version_data[:merge_requests].each_value do |merge_requests|
            merge_requests.each do |merge_request|
              severity_value = fetch_severity_assigned(merge_request)

              pressure[severity_value] += 1
            end
          end

          { version: version_data[:version], pressure: pressure }
        end
      end

      # Returns the patch release pressure for all the versions
      def pressure
        merge_requests(with_patch_version: true)
          .sum { |version| version[:pressure] }
      end

      # Indicates if the coordinator evalutes a single version
      def single_version?
        versions.one?
      end

      private

      def projects
        ReleaseTools::ManagedVersioning::PROJECTS
      end

      def fetch_severity_assigned(merge_request)
        label = merge_request.labels.grep(/^severity::/).first

        if label
          value = /\d/.match(label)[0]
          "severity::#{value}"
        else
          'none'
        end
      end

      def establish_versions(version)
        if version.nil?
          ReleaseTools::Versions.next_versions
        else
          [ReleaseTools::Version.new(version)]
        end
      end
    end
  end
end
