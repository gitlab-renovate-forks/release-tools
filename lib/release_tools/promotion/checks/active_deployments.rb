# frozen_string_literal: true

module ReleaseTools
  module Promotion
    module Checks
      module ActiveDeployments
        include ::ReleaseTools::Promotion::Check

        ENVIRONMENT_NAMES = {
          'gprd' => 'production',
          'gstg' => 'staging'
        }.freeze

        def name
          "#{env_name} deployments"
        end

        def fine?
          chef_client.environment_enabled?
        end

        def to_slack_blocks
          text = StringIO.new
          text.puts("#{icon(self)} *#{name}*")

          unless fine?
            text.puts("#{env_name} environment locked: There's an active #{env_name} deployment, see the <#{pipeline_url}|deployer pipeline>")
          end

          [
            {
              type: 'section',
              text: mrkdwn(text.string)
            }
          ]
        end

        def to_issue_body
          text = StringIO.new
          text.puts("#{icon(self)} #{fine? ? 'no ' : ''}active deployment\n\n")

          unless fine?
            text.puts("#{env_name} environment locked: There's an active #{env_name} deployment, see the [deployer pipeline](#{pipeline_url})")
          end

          text.string
        end

        def pipeline_url
          chef_client.pipeline_url
        end

        def env
          raise NotImplementedError
        end

        def env_name
          ENVIRONMENT_NAMES[env]
        end

        def chef_client
          @chef_client ||= ::ReleaseTools::Chef::Client.new(env)
        end
      end
    end
  end
end
