# frozen_string_literal: true

require 'prometheus/client'
require 'prometheus/client/push'

require 'release_tools/metrics/push'
require 'release_tools/metrics/client'

require 'release_tools/metrics/auto_build_pressure'
require 'release_tools/metrics/auto_deploy_building_package_state'
require 'release_tools/metrics/auto_deploy_package_state'
require 'release_tools/metrics/auto_deploy_pressure'
require 'release_tools/metrics/coordinated_deployment/duration'
require 'release_tools/metrics/coordinated_deployment/track_start_time'

require 'release_tools/metrics/environment_state/common'
require 'release_tools/metrics/environment_state/common_staging_production'
require 'release_tools/metrics/environment_state/production'
require 'release_tools/metrics/environment_state/production_canary'
require 'release_tools/metrics/environment_state/staging'
require 'release_tools/metrics/environment_state/staging_canary'

require 'release_tools/metrics/mirror_status'
require 'release_tools/metrics/monthly_release_status'
require 'release_tools/metrics/patch_release_pressure'
require 'release_tools/metrics/patch_release_status'
require 'release_tools/metrics/release_metric_updater'
require 'release_tools/metrics/track_release_tagged'
require 'release_tools/deployments/blockers_metrics'

module ReleaseTools
  module Metrics
  end
end
