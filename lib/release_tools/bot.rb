# frozen_string_literal: true

module ReleaseTools
  # Module to hold constants related to the gitlab-release-tools-bot user.
  module Bot
    USERNAME = 'gitlab-release-tools-bot'
    GITLAB_COM_ID = '2_324_599'
  end
end
