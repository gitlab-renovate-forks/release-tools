# frozen_string_literal: true

module ReleaseTools
  module Monthly
    module TagDay
      class ComponentBranchVerifier
        def execute
          branches_status = ReleaseTools::Services::BranchesStatusService.new(
            release_type: :monthly,
            versions: monthly_version
          )

          branches_status.execute
        end

        private

        def monthly_version
          [ReleaseTools::Version.new(GitlabReleases.active_version)]
        end
      end
    end
  end
end
