# frozen_string_literal: true

module ReleaseTools
  class DynamicPipelineBase
    def initialize(versions)
      @versions = versions
    end

    def generate
      base_config.merge(stages).merge(jobs).to_yaml
    end

    private

    attr_reader :versions

    def base_config
      {}
    end

    def stages
      raise NotImplementedError
    end

    def job(version)
      raise NotImplementedError
    end

    def jobs
      raise NotImplementedError
    end
  end
end
