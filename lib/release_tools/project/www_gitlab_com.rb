# frozen_string_literal: true

module ReleaseTools
  module Project
    class WWWGitlabCom < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-com/www-gitlab-com.git',
        security:  'git@gitlab.com:gitlab-org/security/www-gitlab-com.git',
        dev:       'git@dev.gitlab.org:gitlab/www-gitlab-com.git'
      }.freeze

      IDS = {
        canonical: 7_764,
        security: 21_005_471
      }.freeze
    end
  end
end
