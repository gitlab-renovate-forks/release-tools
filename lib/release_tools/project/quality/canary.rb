# frozen_string_literal: true

module ReleaseTools
  module Project
    module Quality
      class Canary < Base
        REMOTES = {
          canonical: 'git@gitlab.com:gitlab-org/quality/canary.git',
          ops: 'git@ops.gitlab.net:gitlab-org/quality/canary.git'
        }.freeze

        def self.environment
          'gprd-cny'
        end
      end
    end
  end
end
