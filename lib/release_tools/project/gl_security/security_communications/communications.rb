# frozen_string_literal: true

module ReleaseTools
  module Project
    module GlSecurity
      module SecurityCommunications
        class Communications < BaseProject
          REMOTES = {
            canonical: 'git@gitlab.com:gitlab-com/gl-security/security-communications/communications.git'
          }.freeze
        end
      end
    end
  end
end
