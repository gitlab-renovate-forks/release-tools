# frozen_string_literal: true

module ReleaseTools
  module Project
    class Jihu < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-jh/gitlab-jh-enablement.git'
      }.freeze

      def self.metadata_project_name
        'jihu'
      end
    end
  end
end
