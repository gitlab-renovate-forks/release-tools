# frozen_string_literal: true

module ReleaseTools
  module Project
    class MergeTrain < BaseProject
      REMOTES = {
        canonical: 'git@ops.gitlab.net:gitlab-org/merge-train.git',
        ops: 'git@ops.gitlab.net:gitlab-org/merge-train.git'
      }.freeze

      IDS = {
        ops: 154
      }.freeze
    end
  end
end
