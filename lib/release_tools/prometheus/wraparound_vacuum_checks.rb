# frozen_string_literal: true

module ReleaseTools
  module Prometheus
    # WraparoundVacuumChecks gathers autovacuum metrics from Prometheus
    class WraparoundVacuumChecks
      # List of tables that take more than 20 minutes to vacuum in
      # transaction wraparound prevention mode.
      # The order is important to correctly extract the table names
      # from the migration file names.
      TABLES_WITH_LONG_VACUUM_TIME = %i[
        ci_build_trace_metadata
        ci_builds_metadata
        ci_builds
        ci_job_artifacts
        ci_pipelines_config
        ci_pipelines
        ci_stages
      ].freeze

      TABLES_WITH_LONG_VACUUM_TIME_REGEXP = Regexp.new(TABLES_WITH_LONG_VACUUM_TIME.join('|')).freeze

      QUERY_TEMPLATE = <<~QUERY.strip
        sum(rate(pg_stat_activity_autovacuum_age_in_seconds{relname=~"%<tables>s", type="patroni-ci"}[1m]))
      QUERY

      def self.running?(pending_post_migrations)
        table_names = pending_post_migrations
          .flat_map { |name| name.scan(TABLES_WITH_LONG_VACUUM_TIME_REGEXP) }

        new(table_names).running?
      end

      def initialize(tables)
        @tables = tables
      end

      def running?
        return false if Feature.disabled?(:pdm_vacuum_check)
        return false if tables.empty?

        !query_result.empty?
      end

      private

      attr_reader :tables

      def run_query
        Retriable.retriable do
          Query.new.run(query)
        end
      end

      def query_result
        @query_result ||= run_query['data'].fetch('result', [])
      end

      def query
        wraparound_tables = tables
          .map { |table| "#{table} \\\\(to prevent wraparound\\\\)" }
          .join('|')

        format(QUERY_TEMPLATE, tables: wraparound_tables)
      end
    end
  end
end
