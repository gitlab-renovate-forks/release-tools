# frozen_string_literal: true

require 'state_machines/core'

module ReleaseTools
  module Metrics
    module EnvironmentState
      class Staging
        include ::SemanticLogger::Loggable
        extend StateMachines::MacroMethods
        include EnvironmentState::Common
        include EnvironmentState::CommonStagingProduction

        state_machine :current_state, initial: lambda(&:query_current_state) do
          event :start do
            transition %i[initial ready locked awaiting_promotion] => :locked
          end

          event :success do
            transition %i[initial locked] => :ready
          end

          after_transition to: :ready do |env, _transition|
            # Try to move to awaiting_promotion. It won't move if there are no packages available for promotion.
            env.baking_complete
          end

          event :baking_complete do
            # gstg and gprd go from ready state to awaiting_promotion only if there are new packages available
            # for promotion.
            transition %i[initial ready] => :awaiting_promotion, if: :packages_available_for_promotion?
          end
        end

        def environment
          'gstg'
        end

        def stage
          'main'
        end
      end
    end
  end
end
