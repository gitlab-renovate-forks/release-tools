# frozen_string_literal: true

require 'state_machines/core'

module ReleaseTools
  module Metrics
    module EnvironmentState
      module Common
        METRIC = 'auto_deploy_environment_state'

        attr_reader :current_state

        def initialize
          @prometheus = ReleaseTools::Prometheus::Query.new

          super
        end

        # Triggers the given event and returns true if it was successful.
        def perform_event(event)
          logger.info('Attempting to trigger event', event: event, current_state: current_state, environment: environment, stage: stage)
          fire_current_state_event(event)
        end

        # Queries Prometheus to find the current value of `env_state` label of the metric and determine
        # if the label value can be changed to new_state.
        # Since Prometheus scrapes delivery-metrics every 15 seconds,
        # there is a 15 second delay in propogating metric changes to Prometheus. This
        # 15 second delay could result in us retrieving a wrong value as the current
        # value of the metric from Prometheus.
        # We need to complete https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20083.
        # This issue will make Delivery-metrics a more reliable source of current
        # metric values.
        # Once this is complete, we can add a Get API to delivery-metrics
        # and call the delivery-metrics API to get the current value of the metric instead
        # of Prometheus.
        def query_current_state
          response = @prometheus.run(query)

          response.dig('data', 'result').each do |metric|
            next unless metric['value'][1] == '1'

            return metric['metric']['env_state']
          end

          :initial
        end

        private

        attr_reader :client

        def query
          "#{METRIC}{target_env=\"#{environment}\",target_stage=\"#{stage}\"}"
        end
      end
    end
  end
end
