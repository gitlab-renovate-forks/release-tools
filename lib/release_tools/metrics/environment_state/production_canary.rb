# frozen_string_literal: true

require 'state_machines/core'

module ReleaseTools
  module Metrics
    module EnvironmentState
      class ProductionCanary
        include ::SemanticLogger::Loggable
        extend StateMachines::MacroMethods
        include EnvironmentState::Common

        state_machine :current_state, initial: lambda(&:query_current_state) do
          event :start do
            transition %i[initial ready locked baking_time] => :locked
          end

          event :success do
            transition %i[initial locked] => :ready
          end

          event :bake do
            transition %i[initial locked] => :baking_time
          end

          event :baking_complete do
            # gprd-cny goes from baking_time state to ready state
            transition %i[initial baking_time] => :ready
          end
        end

        def environment
          'gprd'
        end

        def stage
          'cny'
        end
      end
    end
  end
end
