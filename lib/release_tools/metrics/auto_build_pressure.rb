# frozen_string_literal: true

module ReleaseTools
  module Metrics
    class AutoBuildPressure
      include ::SemanticLogger::Loggable

      METRIC = 'auto_build_pressure'

      PROJECTS = [
        Project::OmnibusGitlab, Project::GitlabEe, Project::Gitaly,
        Project::GitlabElasticsearchIndexer, Project::GitlabPages, Project::GitlabShell,
        Project::CNGImage, Project::Kas
      ].freeze

      def initialize
        @client = Metrics::Client.new
      end

      def execute
        version = ProductVersion.last_auto_deploy

        PROJECTS.each do |project|
          count = commit_pressure(project, version[project.metadata_project_name].sha)
          logger.info('Auto-build pressure', latest_version: version.to_s, commits: count, project: project.metadata_project_name)

          @client.set(METRIC, count, labels: project.metadata_project_name)
        end
      end

      private

      def commit_pressure(project, commit)
        Retriable.with_context(:api) do
          GitlabClient
            .compare(project.security_path, from: commit, to: project.default_branch)
            .commits
            .length
        end
      rescue Gitlab::Error::ResponseError => ex
        logger.warn(
          "Failed to fetch build pressure",
          project: project,
          commit: commit,
          error: ex.message
        )

        0
      end
    end
  end
end
