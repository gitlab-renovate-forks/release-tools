# frozen_string_literal: true

require 'release_tools/mimir/utils'

module ReleaseTools
  module Mimir
    GRAFANA_URL = 'https://dashboards.gitlab.net/explore?schemaVersion=1&orgId=1'

    DATASOURCE_ALL_ENVS = 'e58c2f51-20f8-4f4b-ad48-2968782ca7d6' # Mimir - Gitlab - All Environments
  end
end
