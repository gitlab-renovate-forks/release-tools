# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module DeploymentPipeline
      module PackageSelection
        # Select the latest package that is ready for deployment
        class Latest
          include ::SemanticLogger::Loggable

          UnsuitableVersionError = Class.new(StandardError)

          # @return [ProductVersion, nil] Return a ProductVersion if a suitable version is found, or
          # nil if no suitable version is found
          def product_version
            logger.debug('Checking latest available versions for deployment suitability')

            # Loop through product versions in the release/metadata project in descending order
            ProductVersion.detect do |version|
              version_checker = DeploymentPipeline::Version.new(version)
              next unless version_checker.valid_auto_deploy_version?

              logger.info(
                'Checking version for deployment suitability',
                version: version.to_s,
                suitable: version_checker.suitable?
              )

              # Break out of the loop once we reach versions that are older than the ones being deployed
              break unless version_checker.newer_than_latest_deployment?

              version_checker.suitable?
            end
          end
        end
      end
    end
  end
end
