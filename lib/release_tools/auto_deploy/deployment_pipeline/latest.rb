# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module DeploymentPipeline
      # Get the latest deployment pipeline or version
      class Latest
        include ::SemanticLogger::Loggable

        class << self
          # This method finds and returns the latest deployment pipeline.
          #
          # @return [Gitlab::ObjectifiedHash] the latest coordinator pipeline or decoupled deployment pipeline.
          def pipeline
            # TODO (@rpereira2): Once we stop using coordinator pipelines, we can stop checking them here
            @pipeline ||= [
              latest_coordinator_pipeline,
              latest_decoupled_deployment_pipeline
            ].compact
             .max_by { |p| version_for(p) }
          end

          # @return [ProductVersion] the product version of the latest deployment pipeline
          def version
            version_for(pipeline)
          end

          private

          def latest_coordinator_pipeline
            pipeline = Retriable.with_context(:api) do
              GitlabOpsClient.pipelines(Project::ReleaseTools, name: 'Coordinator pipeline', per_page: 1).first
            end

            logger.info("Latest coordinator pipeline", version: pipeline.ref, id: pipeline.id)

            pipeline
          end

          def latest_decoupled_deployment_pipeline
            pipelines = Retriable.with_context(:api) do
              GitlabOpsClient.pipelines(
                Project::ReleaseTools,
                {
                  ref: Project::ReleaseTools.default_branch,
                  author: ReleaseTools::Bot::USERNAME,
                  source: 'api'
                }
              )
            end

            pipeline = nil
            Retriable.with_context(:api) do
              pipelines.auto_paginate do |p|
                next unless p.name&.starts_with?('Deployment pipeline')
                next unless version_for(p).auto_deploy?

                pipeline = p
                break
              end
            end

            logger.info("Latest deployment pipeline", version: pipeline.name, id: pipeline.id)

            pipeline
          end

          def version_for(pipeline)
            if pipeline.name == 'Coordinator pipeline'
              ProductVersion.new(pipeline.ref)
            elsif pipeline.name.starts_with?('Deployment pipeline')
              ProductVersion.new(pipeline.name.split.last)
            end
          end
        end
      end
    end
  end
end
