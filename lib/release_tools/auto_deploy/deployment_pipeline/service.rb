# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module DeploymentPipeline
      # Entry class for triggering a deployment pipeline
      class Service
        include ::SemanticLogger::Loggable

        CannotStartPipelineError = Class.new(StandardError)

        # @param version [Version, nil] Version to be deployed. Only used with strategy: :version.
        # @param strategy [Symbol] package selection strategy to be used. One of :latest, :version
        # @param force_version_deployment [Boolean] should be set to true to ignore the following (only works with the version strategy):
        #   - version suitability check - It does not check for suitability of the given version.
        #   - sequencing of pipelines - It does not check the Sequencer if it is the right time to start a pipeline.
        def initialize(strategy: :latest, version: nil, force_version_deployment: false)
          @version = version
          @strategy = strategy
          @force_version_deployment = force_version_deployment
        end

        def execute
          return unless time_to_deploy?

          if strategy == :version && version.blank?
            raise ArgumentError, 'The version argument is required to be specified when using the "version" strategy'
          end

          product_version = product_version_to_deploy
          if product_version.nil?
            logger.warn('Could not find suitable version to deploy with the selected strategy', strategy: strategy)
            return
          end

          Trigger.new(product_version).execute
        end

        private

        attr_reader :version, :strategy, :force_version_deployment

        def time_to_deploy?
          if strategy == :version
            return strategy_version_time_to_deploy?
          end

          # The version strategy is not used by automation, so it is not affected by the
          # do_not_auto_create_new_deployment_pipelines feature flag.
          if Feature.enabled?(:do_not_auto_create_new_deployment_pipelines)
            logger.warn('Cannot automatically create new deployment pipeline since feature flag do_not_create_new_deployment_pipelines is on')
            raise CannotStartPipelineError, 'Cannot start a deployment pipeline since the do_not_auto_create_new_deployment_pipelines feature flag is on'
          end

          can_start_deployment?
        end

        # Implements special behavior for the version strategy
        def strategy_version_time_to_deploy?
          # The version strategy is triggered by Release Managers when required, so it has a mechanism for RMs
          # to override the Sequencer check.
          if force_version_deployment
            logger.info('Skipping Sequencer check due to force_version_deployment flag', version: version, strategy: strategy)
            return true
          end

          if can_start_deployment?
            return true
          end

          # Raise error for `:version` strategy because release-tools is being specifically asked to start a deployment
          # pipeline for the given version. If it cannot start a pipeline, the CI job should fail.
          raise CannotStartPipelineError, 'Cannot start a deployment pipeline since the previous pipeline is not far enough along. Force create a pipeline to override this.'
        end

        def can_start_deployment?
          if Sequencer.can_start_deployment?
            logger.info('Sequencer check succeeded, proceeding', version: version, strategy: strategy)

            true
          else
            logger.info('Not triggering deployment pipeline since sequencer check failed', version: version, strategy: strategy)

            false
          end
        end

        def product_version_to_deploy
          case strategy
          when :latest
            PackageSelection::Latest.new.product_version
          when :version
            PackageSelection::VersionSelector.new(version:, skip_suitable_check: force_version_deployment).product_version
          else
            raise ArgumentError, "#{strategy} is not a supported strategy"
          end
        end
      end
    end
  end
end
