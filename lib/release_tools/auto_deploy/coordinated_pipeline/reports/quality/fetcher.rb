# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Reports
        module Quality
          class Fetcher
            include Helper

            def initialize(environment:, pipeline_id:)
              @environment = environment
              @pipeline_id = pipeline_id
            end

            def execute
              pipelines.map do |pipeline|
                Pipeline.new(pipeline.downstream_pipeline, environment, pipeline.name)
              end
            end

            private

            attr_reader :environment, :pipeline_id

            def pipelines
              Retriable.with_context(:api) do
                bridges.each do |bridge|
                  client.pipeline(ReleaseTools::Project::ReleaseTools, bridge.pipeline.id)
                end
              end
            end

            def bridges
              Retriable.with_context(:api) do
                client
                  .pipeline_bridges(pipeline_id)
                  .select { |bridge| quality_pipeline?(bridge) }
              end
            end

            def quality_pipeline?(bridge)
              quality_pipelines.include?(bridge.name) && bridge.status == 'failed'
            end

            def quality_pipelines
              ["qa:smoke-main:#{environment}", "qa:smoke:#{environment}"]
            end
          end
        end
      end
    end
  end
end
