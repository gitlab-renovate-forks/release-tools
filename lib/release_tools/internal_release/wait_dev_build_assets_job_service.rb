# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    # The service waits for the `build-assets-image` job to be completed
    # on the GitLab dev project. This is only required if the security fix
    # modifies the assets (javscript, views or CSS files).
    #
    # The waiting time is long (~50 minutes), so it is guard under a feature flag
    # to be enabled when required.
    class WaitDevBuildAssetsJobService
      include ::SemanticLogger::Loggable

      JobNotSuccessful = Class.new(StandardError) do
        def message
          'The job build-assets-image for the dev commit was not successful'
        end
      end

      CommitNotFound = Class.new(StandardError) do
        def message
          'The commit was not found in the GitLab dev project'
        end
      end

      # version - ReleaseTools::Version
      # commit - GitLab::ObjectifiedHash
      def initialize(version:, commit:)
        @version = version
        @commit = commit
        @client = ReleaseTools::GitlabDevClient
      end

      # This class:
      # 1. Waits for the commit to be mirrored.
      # 2. Fetch the sync pipeline on the stable branch.
      # 3. Wait for the build assets image job from the above pipeline to succeed.
      def execute
        wait_for_commit

        if Feature.disabled?(:internal_release_wait_for_build_assets_image)
          logger.info('internal_release_wait_for_build_assets_image disabled, skipping waiting for build-assets-image')

          return
        end

        pipeline = fetch_pipeline_on_stable_branch

        wait_for_build_assets_image(dev_pipeline: pipeline)
      end

      private

      attr_reader :client, :version, :commit

      def project
        Project::GitlabEe
      end

      def wait_for_commit
        logger.info('Searching for the commit on Dev', ref: commit.id)

        dev_commit = nil

        Retriable.with_context(:mirroring, on: Gitlab::Error::NotFound) do
          dev_commit = client.commit(
            project.dev_path,
            ref: commit.id
          )

          raise CommitNotFound unless dev_commit

          logger.info('Commit found in dev mirror', url: dev_commit.web_url)
        end
      end

      def fetch_pipeline_on_stable_branch
        logger.info('Fetching the pipeline on the stable branch')

        pipelines = Retriable.with_context(:api) do
          client.pipelines(
            project.dev_path,
            {
              ref: version.stable_branch(ee: true),
              sha: commit.id,
              order_by: 'id',
              sort: 'asc',
              per_page: 1
            }
          )
        end

        pipeline = pipelines.first
        raise StandardError, 'No pipeline found for commit' unless pipeline

        logger.info('Pipeline found', pipeline: pipeline.web_url)

        pipeline
      end

      def wait_for_build_assets_image(dev_pipeline: pipeline)
        logger.info('Waiting for build-assets-image in dev pipeline to finish')

        Retriable.with_context(:wait_build_assets_job_on_dev, on: JobNotSuccessful) do
          job = client.pipeline_job_by_name(
            project.dev_path,
            dev_pipeline.id,
            'build-assets-image'
          )

          raise JobNotSuccessful unless job&.status == 'success'

          logger.info('build-assets-image job was completed successfully', pipeline: dev_pipeline.web_url, job_url: job.web_url)
        end
      end
    end
  end
end
