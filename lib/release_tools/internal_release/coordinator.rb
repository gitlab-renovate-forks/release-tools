# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    class Coordinator
      attr_reader :versions

      def initialize(internal_number = 0)
        @versions = establish_versions(internal_number)
      end

      def supported_minor_versions
        versions.map(&:to_minor)
      end

      private

      # Returns the versions for the next internal release
      # e.g. if the next patch release versions are 16.10.1, 16.9.3, 16.8.5
      # this method will return the last two patch releases' internal versions
      # [ReleaseTools::Version.new("16.9.2-internal0"), ReleaseTools::Version.new("16.8.4-internal0")]
      # If there have already been internal releases in this patch cycle, pass in an internal_number
      def establish_versions(internal_number)
        supported_next_patch_versions = ReleaseTools::Versions.next_versions.last(2)

        supported_next_patch_versions.map do |version|
          supported_internal_version = version.previous_patch.to_internal(internal_number)

          ReleaseTools::Version.new(supported_internal_version)
        end
      end
    end
  end
end
