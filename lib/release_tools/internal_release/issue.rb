# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    class Issue < ReleaseTools::Issue
      include ::ReleaseTools::Security::IssueHelper
      include ::SemanticLogger::Loggable

      RELEASE_TYPE = 'internal'

      def initialize(*args)
        super

        @internal_number = iteration.to_i
      end

      def create
        release_pipeline

        super
      end

      def title
        "Internal Release: #{versions_title}"
      end

      def confidential?
        true
      end

      def labels
        'internal releases'
      end

      def project
        ReleaseTools::Project::Release::Tasks
      end

      def versions
        if version.present?
          [internal_version]
        else
          internal_release_coordinator.versions
        end
      end

      def regular_versions
        versions.map(&:to_minor).join(', ')
      end

      def assignees
        ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      protected

      def template_path
        File.expand_path('../../../templates/internal.md.erb', __dir__)
      end

      def internal_release_coordinator
        @internal_release_coordinator ||= ReleaseTools::InternalRelease::Coordinator.new(internal_number)
      end

      def versions_title
        versions.join(', ')
      end

      def pipeline_variables
        {
          INTERNAL_RELEASE_PIPELINE: 'true',
          ITERATION: internal_number,
          VERSIONS: versions.join(' ')
        }
      end

      private

      attr_reader :internal_number

      def internal_version
        ReleaseTools::Version.new(version.to_internal(internal_number))
      end
    end
  end
end
