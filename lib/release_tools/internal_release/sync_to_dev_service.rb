# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    class SyncToDevService
      include ::SemanticLogger::Loggable

      def initialize
        @client = ReleaseTools::GitlabClient
      end

      def execute
        logger.info('Sync GitLab security and GitLab dev repositories', remote_mirror: remote_mirror.url)

        Retriable.with_context(:api) do
          client.sync_remote_mirror(project.security_path, remote_mirror.id)
        end
      end

      private

      attr_reader :client

      def project
        ReleaseTools::Project::GitlabEe
      end

      def remote_mirror
        mirrors.find { |mirror| mirror.url.include?('dev.gitlab.org/gitlab/gitlab-ee.git') }
      end

      def mirrors
        @mirrors ||= Retriable.with_context(:api) do
          client.remote_mirrors(project.security_path)
        end
      end
    end
  end
end
