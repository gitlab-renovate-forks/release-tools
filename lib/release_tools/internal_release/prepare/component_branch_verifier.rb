# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    module Prepare
      class ComponentBranchVerifier
        def initialize(internal_versions)
          @internal_versions = internal_versions
        end

        def execute
          branches_status = ReleaseTools::Services::BranchesStatusService.new(
            release_type: :internal,
            versions: internal_versions
          )

          branches_status.execute
        end

        private

        attr_reader :internal_versions
      end
    end
  end
end
