# frozen_string_literal: true

module ReleaseTools
  module InternalRelease
    module Prepare
      class ComponentBranchVerifier
        def initialize(internal_number)
          @internal_number = internal_number
        end

        def execute
          branches_status = ReleaseTools::Services::BranchesStatusService.new(
            release_type: :internal,
            versions: internal_versions
          )

          branches_status.execute
        end

        private

        attr_reader :internal_number

        def internal_versions
          ReleaseTools::InternalRelease::Coordinator.new(internal_number).versions
        end
      end
    end
  end
end
