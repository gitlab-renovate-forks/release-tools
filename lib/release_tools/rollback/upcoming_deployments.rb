# frozen_string_literal: true

module ReleaseTools
  module Rollback
    # Verifies if there's a running deployment for a given environment.
    #
    # Verification is done by fetching the running deployments from a specific
    # environment through the API.
    class UpcomingDeployments
      include ::SemanticLogger::Loggable

      PROJECT = ReleaseTools::Project::GitlabEe
      STALE_DEPLOYMENT_CUTOFF = 5.hours
      ENVIRONMENTS = %w[gstg gprd].freeze

      def initialize(environment:)
        @environment = environment
      end

      def any?
        environment? && last_running_deployment.present?
      end

      # Cleans up Deployments left in a `running` state for too long
      #
      # A deployment pipeline canceled or failing in unexpected ways leaves a
      # tracked Deployment in a `running` state long after it's no longer
      # running.
      def stale_cleanup
        return unless Feature.enabled?(:cleanup_stale_deployments)

        running.auto_paginate do |deployment|
          next if Time.parse(deployment.updated_at) >= STALE_DEPLOYMENT_CUTOFF.ago

          logger.debug(
            'Marking stale deployment as failed',
            deployment: deployment.iid,
            updated_at: deployment.updated_at,
            sha: deployment.sha
          )

          next if SharedStatus.dry_run?

          GitlabClient.update_deployment(
            PROJECT.auto_deploy_path,
            deployment.id,
            status: 'failed'
          )
        end
      end

      def running
        GitlabClient.deployments(
          PROJECT.auto_deploy_path,
          environment,
          status: 'running',
          order_by: 'id',
          sort: 'desc'
        )
      end

      private

      attr_reader :environment

      def environment?
        ENVIRONMENTS.include?(environment)
      end

      def last_running_deployment
        @last_running_deployment ||= running.paginate_with_limit(1).first
      end
    end
  end
end
