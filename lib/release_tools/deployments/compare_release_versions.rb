# frozen_string_literal: true

module ReleaseTools
  module Deployments
    # Compares the expected version with actual version that is deployed to release.gitlab.net
    class CompareReleaseVersions
      include ::SemanticLogger::Loggable

      attr_reader :client, :expected_version

      def initialize(expected_version)
        @client = ReleaseTools::GitlabReleaseClient
        @expected_version = expected_version
      end

      def equal_to_actual_release_version?
        actual_version == expected_version
      end

      # "17.2.1-ee"
      def actual_version
        ReleaseTools::Version.new(client.current_version)
      end
    end
  end
end
