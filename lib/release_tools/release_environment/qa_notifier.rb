# frozen_string_literal: true

module ReleaseTools
  module ReleaseEnvironment
    class QaNotifier < ReleaseTools::ReleaseEnvironment::BaseNotifier
      def execute
        logger.debug('Executing release environment QA notifier',
                     pipeline_url: pipeline_url,
                     environment_name: environment_name,
                     release_environment_version: release_environment_version)

        qa_job = find_first_failed_qa_job

        if qa_job.present?
          logger.info('QA job found', pipeline_url: qa_job.web_url)

          send_slack_notification(qa_job)
        else
          logger.fatal('QA job not found')
        end
      end

      private

      # Currently, we only send a QA notification if the QA job fails.
      # There are QA jobs running in parallel, so this method search for the first failed job.
      def find_first_failed_qa_job
        logger.info('Fetching failed QA jobs from pipeline', pipeline_id: pipeline_id)
        path = project_path

        Retriable.with_context(:pipeline_created) do
          ReleaseTools::GitlabClient.pipeline_jobs(
            path,
            pipeline_id
          )
            .lazy_paginate
           .find { |job| job.status == 'failed' && job.name.start_with?('release-environments-qa') }
        end
      end

      def send_slack_notification(qa_job)
        ReleaseTools::Slack::ReleaseEnvironment::QaNotification.new(
          job: qa_job,
          environment_name: environment_name,
          release_environment_version: release_environment_version
        ).execute
      end
    end
  end
end
