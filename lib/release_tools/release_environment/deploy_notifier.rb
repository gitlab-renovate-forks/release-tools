# frozen_string_literal: true

module ReleaseTools
  module ReleaseEnvironment
    class DeployNotifier < ReleaseTools::ReleaseEnvironment::BaseNotifier
      def execute
        logger.debug('Executing release environment deploy notifier',
                     pipeline_url: pipeline_url,
                     environment_name: environment_name,
                     release_environment_version: release_environment_version)

        pipeline = find_downstream_pipeline

        if pipeline.present?
          logger.info('Downstream pipeline found', pipeline_url: pipeline.web_url)

          send_slack_notification(pipeline)
        else
          logger.fatal('Downstream pipeline not found')
        end
      end

      private

      def find_downstream_pipeline
        logger.info('Fetch downstream pipeline', pipeline_id: pipeline_id)

        bridge.downstream_pipeline
      rescue MissingPipelineError
        nil
      end

      def bridge
        logger.info('Fetching bridge from pipeline', pipeline_id: pipeline_id)
        path = project_path

        Retriable.with_context(:pipeline_created) do
          bridge = ReleaseTools::GitlabClient.pipeline_bridges(
            path,
            pipeline_id
          )
            .lazy_paginate
            .find { |job| job.name == 'release-environments-deploy' }

          raise MissingPipelineError unless bridge&.downstream_pipeline

          bridge
        end
      end

      def send_slack_notification(pipeline)
        ReleaseTools::Slack::ReleaseEnvironment::DeployNotification.new(
          pipeline: pipeline,
          environment_name: environment_name,
          release_environment_version: release_environment_version
        ).execute
      end
    end
  end
end
