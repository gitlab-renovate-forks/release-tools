# frozen_string_literal: true

module ReleaseTools
  module ReleaseEnvironment
    class BaseNotifier
      include ::SemanticLogger::Loggable
      include ReleaseTools::AutoDeploy::Pipeline

      def initialize(pipeline_url:, environment_name:, release_environment_version:)
        @pipeline_url = pipeline_url
        @environment_name = environment_name
        @release_environment_version = release_environment_version
      end

      private

      attr_reader :pipeline_url, :environment_name, :release_environment_version

      def pipeline_id
        @pipeline_id ||= pipeline_url.match(/\/(\d+)$/).captures.first
      end

      def project_path
        if pipeline_url.include?('gitlab-org/gitlab')
          ReleaseTools::Project::GitlabEe.path
        elsif pipeline_url.include?('gitlab-org/security/gitlab')
          ReleaseTools::Project::GitlabEe.security_path
        else
          raise ArgumentError, "Invalid pipeline URL: #{pipeline_url}"
        end
      end

      def send_slack_notification(*args)
        raise NotImplementedError
      end
    end
  end
end
