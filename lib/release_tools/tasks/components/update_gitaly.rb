# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Components
      class UpdateGitaly
        include Helper
        include ::SemanticLogger::Loggable
        include ReleaseTools::Tasks::Components::Updater

        def send_slack_notification
          Slack::AutoDeployNotification.on_stale_gitaly_merge_request(merge_request)
        end

        def project
          Project::Gitaly
        end

        def source_branch
          'release-tools/update-gitaly'
        end

        def merge_request
          @merge_request ||= UpdateGitalyMergeRequest.new(source_branch: source_branch)
        end
      end
    end
  end
end
