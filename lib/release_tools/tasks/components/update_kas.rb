# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Components
      class UpdateKas
        include Helper
        include ::SemanticLogger::Loggable
        include ReleaseTools::Tasks::Components::Updater

        def send_slack_notification
          Slack::AutoDeployNotification.on_stale_kas_merge_request(merge_request)
        end

        def project
          Project::Kas
        end

        def source_branch
          'release-tools/update-kas'
        end

        def merge_request
          @merge_request ||= UpdateKasMergeRequest.new(source_branch: source_branch)
        end
      end
    end
  end
end
