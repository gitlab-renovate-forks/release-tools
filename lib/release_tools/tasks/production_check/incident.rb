# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ProductionCheck
      # Perform a production health check when an incident is declared
      #
      # Results are posted to a provided incident issue.
      class Incident
        include ::SemanticLogger::Loggable

        def initialize
          # Allow overriding the incident project path via environment variable
          @project = ENV.fetch(
            'GITLAB_PRODUCTION_PROJECT_PATH',
            Project::Infrastructure::Production
          )

          @issue_iid = ENV.fetch('PRODUCTION_ISSUE_IID')
        end

        def execute
          issue = GitlabClient.issue(@project, @issue_iid)

          return if SharedStatus.dry_run?

          Retriable.with_context(:api) do
            GitlabClient.create_issue_note(
              @project,
              issue: issue,
              body: status.to_issue_body
            )
          end
        end

        private

        def status
          @status ||= Promotion::ProductionStatus.new(:active_gprd_deployments, :incident_rollback)
        end
      end
    end
  end
end
