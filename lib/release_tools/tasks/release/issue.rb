# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Release
      class Issue
        include ReleaseTools::Tasks::Helper

        attr_reader :version

        def initialize(version)
          @version = version
        end

        def execute
          create_or_show_issue(release_issue)
        end

        def release_issue
          @release_issue ||=
            if version&.monthly?
              ReleaseTools::MonthlyIssue.new(version: version)
            else
              patch_release_issue
            end
        end

        private

        def patch_release_issue
          patch_version = if version.nil?
                            ReleaseTools::Versions.next_versions.first
                          else
                            version
                          end

          ReleaseTools::PatchRelease::Issue.new(version: patch_version)
        end
      end
    end
  end
end
