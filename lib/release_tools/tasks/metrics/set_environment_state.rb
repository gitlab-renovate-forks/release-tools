# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module Metrics
      # Class used for setting the auto_deploy_environment_state metric
      class SetEnvironmentState
        include ::SemanticLogger::Loggable

        METRIC = 'auto_deploy_environment_state'
        STATES = %w[ready locked baking_time awaiting_promotion].freeze

        UnexpectedEnvironmentError = Class.new(StandardError)

        def initialize(environment, event)
          @client = ReleaseTools::Metrics::Client.new
          @environment = environment
          @event = event
        end

        def execute
          edit_resource_group_process_mode

          validator = environment_transition_validator(environment)

          valid = validator.perform_event(event)

          unless valid
            logger.warn('Cannot trigger desired event', event: event, current_state: validator.current_state, environment: environment)
            return
          end

          logger.info('Setting env_state', environment: environment, env: validator.environment, stage: validator.stage, new_state: validator.current_state)
          return if SharedStatus.dry_run?

          # Set the metric to the state that the state machine has moved to after triggering the event.
          set_metric(validator.current_state, validator.environment, validator.stage)
        end

        private

        attr_reader :client, :environment, :event

        def set_metric(state, env, stage)
          client.set(METRIC, 1, labels: "#{state},#{env},#{stage}")

          (STATES - [state]).each do |other_state|
            client.set(METRIC, 0, labels: "#{other_state},#{env},#{stage}")
          end
        end

        def environment_transition_validator(environment)
          case environment
          when 'gstg'
            ReleaseTools::Metrics::EnvironmentState::Staging.new
          when 'gstg-cny'
            ReleaseTools::Metrics::EnvironmentState::StagingCanary.new
          when 'gprd'
            ReleaseTools::Metrics::EnvironmentState::Production.new
          when 'gprd-cny'
            ReleaseTools::Metrics::EnvironmentState::ProductionCanary.new
          else
            logger.fatal('Unexpected environment received for environment state validation', environment: environment)
            raise UnexpectedEnvironmentError, "#{environment} is not expected for environment state validation"
          end
        end

        def edit_resource_group_process_mode
          resp =
            Retriable.with_context(:api) do
              GitlabOpsClient.edit_resource_group(resource_group_key: "set-environment-state-metric-#{environment}", process_mode: 'unordered')
            end

          logger.info('Edited resource group', response: resp, resource_group: "set-environment-state-metric-#{environment}")
        rescue StandardError => ex
          logger.warn('Editing resource group failed', error: ex, resource_group: "set-environment-state-metric-#{environment}")
        end
      end
    end
  end
end
