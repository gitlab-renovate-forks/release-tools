# frozen_string_literal: true

require "gitlab"

module ReleaseTools
  module DockerHub
    class Tag < ::Gitlab::ObjectifiedHash
      def active?
        data["tag_status"] == "active"
      end

      def version(version)
        if ee?
          "#{version}-ee.0"
        else
          "#{version}-ce.0"
        end
      end

      def ee?
        data["name"].include?('ee')
      end
    end
  end
end
