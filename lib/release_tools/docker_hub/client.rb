# frozen_string_literal: true

module ReleaseTools
  module DockerHub
    class Client
      include ::SemanticLogger::Loggable

      class << self
        CE_PATH = "v2/namespaces/gitlab/repositories/gitlab-ce/tags"
        EE_PATH = "v2/namespaces/gitlab/repositories/gitlab-ee/tags"

        def base_uri
          'https://hub.docker.com'
        end

        # Fetches single tag using version number and repository type
        def tag(version, repository: "ee")
          response = HTTP.get(tag_path(version, repository))
          Tag.new(parse(response))
        end

        # Fetches last 50 tags using repository type
        #
        # Returns empty array if no tags found.
        def tags(repository: "ee")
          response = HTTP.get(base_path(repository), params: { page_size: 50 })
          parse(response).fetch("results", []).map do |result|
            Tag.new(result)
          end
        end

        def tag_path(version, repository)
          suffix = if repository == "ee"
                     "ee.0"
                   else
                     "ce.0"
                   end

          "#{base_path(repository)}/#{version}-#{suffix}"
        end

        def base_path(repository)
          if repository == "ee"
            "#{base_uri}/#{EE_PATH}"
          else
            "#{base_uri}/#{CE_PATH}"
          end
        end

        def parse(response)
          logger.fatal("Request to docker hub failed", response.status) unless response.status.success?
          response.parse
        end
      end

      private_class_method :parse, :tag_path, :base_uri
    end
  end
end
