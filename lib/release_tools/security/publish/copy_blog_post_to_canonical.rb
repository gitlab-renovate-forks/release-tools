# frozen_string_literal: true

module ReleaseTools
  module Security
    module Publish
      # Copies a security MR to the canonical project returns
      # a Gitlab::ObjectifiedHash object
      class CopyBlogPostToCanonical
        include ::SemanticLogger::Loggable

        BlogPostNotFoundError = Class.new(StandardError)

        # @param security_merge_request [Gitlab::ObjectifiedHash]
        def initialize(security_merge_request)
          @merge_request = security_merge_request
          @client = ReleaseTools::GitlabClient
        end

        def execute
          logger.info('Creating merge request in canonical', security_merge_request: merge_request.web_url)

          return if SharedStatus.dry_run?

          create_branch_in_canonical
          create_commit
          create_merge_request
          validate_canonical_blog_post_exist
        rescue StandardError => ex
          logger.fatal(error_message, error: ex)

          raise
        end

        private

        attr_reader :merge_request, :client

        def create_branch_in_canonical
          logger.info(
            'Creating branch',
            source_branch: canonical_source_branch,
            default_branch: project.default_branch,
            project: project
          )

          Retriable.with_context(:api) do
            client.create_branch(
              canonical_source_branch,
              project.default_branch,
              project
            )
          end
        rescue Gitlab::Error::BadRequest => ex
          logger.info('Branch exists on the canonical project, skipping', error: ex)
        end

        def canonical_source_branch
          "#{merge_request.source_branch}-canonical"
        end

        def project
          ReleaseTools::Project::WWWGitlabCom
        end

        def create_commit
          message = <<~MSG.strip
            Moving security merge request to #{project}

            Merge request: #{merge_request.web_url}
          MSG

          logger.info('Creating commit', source_branch: canonical_source_branch, file_path: file_path)

          Retriable.with_context(:api) do
            client.create_commit(
              project,
              canonical_source_branch,
              message,
              [
                {
                  action: 'create',
                  file_path: file_path,
                  content: content
                }
              ]
            )
          end
        rescue Gitlab::Error::BadRequest => ex
          logger.info('The file is already included in the branch, skipping commit', error: ex)
        end

        def file_path
          @file_path ||= Retriable.with_context(:api) do
            client
              .merge_request_diffs(project.security_path, merge_request.iid)
              .first
              .new_path
          end
        end

        def content
          logger.info(
            'Fetching content',
            merge_request: merge_request.web_url,
            file_path: file_path,
            branch: merge_request.source_branch
          )

          Retriable.with_context(:api) do
            client.file_contents(
              project.security_path,
              file_path,
              merge_request.source_branch
            )
          end
        end

        def create_merge_request
          description = <<~MSG.strip
            #{merge_request.description}

            Copied from #{merge_request.web_url}
          MSG

          canonical_merge_request = ReleaseTools::Security::BlogMergeRequest.new(
            project: project,
            title: merge_request.title.sub(/^draft:/i, '').lstrip,
            description: description,
            labels: merge_request.labels,
            source_branch: canonical_source_branch,
            target_branch: project.default_branch,
            assignee_ids: assignees.collect(&:id)
          )

          canonical_merge_request.create

          logger.info('Merge request created', web_url: canonical_merge_request.remote_issuable.web_url)
        end

        def assignees
          merge_request.assignees.concat(release_managers)
        end

        def release_managers
          ReleaseTools::ReleaseManagers::Schedule.new.active_release_managers
        rescue ReleaseManagers::Schedule::VersionNotFoundError
          logger.fatal('Could not find active release managers')
          nil
        end

        def validate_canonical_blog_post_exist
          blog_post = client.security_blog_merge_request(security: false)

          raise BlogPostNotFoundError if blog_post.nil?
        end

        def error_message
          <<~MSG.strip
            Blog post could not be found on the public handbook repository. Please review the logs
            and if necessary retry this job.
          MSG
        end
      end
    end
  end
end
