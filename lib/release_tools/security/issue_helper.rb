# frozen_string_literal: true

module ReleaseTools
  module Security
    module IssueHelper
      def security_tracking_issue
        @security_tracking_issue ||= Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.next_security_tracking_issue
        end
      end

      def security_task_issue
        @security_task_issue ||= Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.current_security_task_issue
        end
      end

      def reference_issue
        if ReleaseTools::SharedStatus.critical_patch_release?
          security_task_issue
        else
          security_tracking_issue
        end
      end

      def security_communication_issue
        @security_communication_issue ||= Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.security_communication_issue
        end
      end
    end
  end
end
