# frozen_string_literal: true

module ReleaseTools
  module Security
    class TargetIssueNotifier
      include ReleaseTools::Security::IssueHelper

      InvalidEvent = Class.new(StandardError)

      def initialize(event, issue)
        @event = event
        @issue = issue
      end

      def self.notify(event, issue)
        new(event, issue).execute
      end

      def execute
        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          client.create_issue_note(
            issue.project,
            issue: issue,
            body: notification_message
          )
        end
      end

      private

      attr_reader :event, :issue

      def client
        ReleaseTools::GitlabClient
      end

      def notification_message
        return linked_message if event == :linked
        return unlinked_message if event == :unlinked
        return not_ready_message if event == :not_ready

        raise InvalidEvent, "#{event} is not a valid event type."
      end

      def linked_message
        "#{assignees_string}, this issue has been linked to the [current patch release tracking issue](#{security_tracking_issue.web_url}). Barring any problems, it will be included in that patch release. There is no further action needed at this time."
      end

      def unlinked_message
        <<~MSG
          :warning: #{assignees_string}, this issue was unlinked from the [patch release tracking issue](#{security_tracking_issue.web_url}) and **will not** be included in the upcoming patch release. It failed to qualify for the following reasons:

          #{reasons_string}

          **To have this issue re-evaluated, please re-apply the ~"security-target" label.**
        MSG
      end

      def not_ready_message
        <<~MSG
          :warning: #{assignees_string}, this issue was reviewed to be included in an [upcoming patch release](#{security_tracking_issue.web_url}) and **will not** be included. It failed to qualify for the following reasons:

          #{reasons_string}

          **To have this issue re-evaluated, please re-apply the ~"security-target" label.**
        MSG
      end

      def assignees_string
        return "@#{issue.author.username}" if issue.assignees.empty?

        issue.assignees.map do |assignee|
          "@#{assignee.username}"
        end.join(', ')
      end

      def reasons_string
        issue.pending_reasons.map do |reason|
          "- #{reason}"
        end.join("\n")
      end
    end
  end
end
