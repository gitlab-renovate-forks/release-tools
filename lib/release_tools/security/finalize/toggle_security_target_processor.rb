# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class ToggleSecurityTargetProcessor
        include ::SemanticLogger::Loggable

        PIPELINE_SCHEDULE_ID = 390
        CRON = '0 * * * *' # hourly
        ACTIONS = %i[enable disable].freeze

        def execute(action:)
          raise ArgumentError, 'action must be :enable or :disable' unless ACTIONS.include?(action)

          logger.info("Toggling the processor pipeline schedule to #{action}")

          return if SharedStatus.dry_run?

          toggle_security_target_processor(action)
          send_slack_notification(:success, action)
        rescue StandardError => ex
          logger.fatal(failure_message(action), error: ex)

          send_slack_notification(:failed, action)

          raise
        end

        private

        def toggle_security_target_processor(action)
          Retriable.with_context(:api) do
            GitlabOpsClient.pipeline_schedule_take_ownership(Project::ReleaseTools, PIPELINE_SCHEDULE_ID)
          end

          Retriable.with_context(:api) do
            GitlabOpsClient.edit_pipeline_schedule(
              Project::ReleaseTools.ops_path,
              PIPELINE_SCHEDULE_ID,
              active: action == :enable,
              cron: CRON
            )
          end
        end

        def failure_message(action)
          <<~MSG
            The security target processor scheduled pipeline could not be updated.
            If the job continues to fail, you can manually #{action} the `security-target issue processor` pipeline schedule:
            https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
          MSG
        end

        def send_slack_notification(status, action)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: "#{action.capitalize} security-target issue processor",
            status: status,
            release_type: :patch
          ).send_notification
        end
      end
    end
  end
end
