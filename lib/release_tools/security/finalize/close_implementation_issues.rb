# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class CloseImplementationIssues
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::IssueHelper

        CouldNotCompleteError = Class.new(StandardError)

        def initialize
          @client = ReleaseTools::GitlabClient
        end

        def execute
          return if security_issues.empty?

          security_issues.each do |issue|
            next unless issue.processed?

            logger.info('Security implementation issue processed', issue: issue.web_url)

            next if SharedStatus.dry_run?

            client.close_issue(issue.project_id, issue)
          end

          send_slack_notification(:success)
        rescue Gitlab::Error::Error => ex
          logger.fatal(error_message, error: ex)

          send_slack_notification(:failed)

          raise CouldNotCompleteError
        end

        private

        attr_reader :client

        def security_issues
          @security_issues ||= ReleaseTools::Security::IssueCrawler
            .new
            .upcoming_security_issues_and_merge_requests
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Close security issues',
            status: status,
            release_type: :patch
          ).send_notification
        end

        def error_message
          <<~MSG
            Closing the security implementation issues failed, review the error log and
            consider retrying this job. If the failure persist, manually close the security issues
            associated with the security tracking issue: #{security_tracking_issue.web_url} by executing
            the following command on Slack:

            /chatops run release close_issues --security
          MSG
        end
      end
    end
  end
end
