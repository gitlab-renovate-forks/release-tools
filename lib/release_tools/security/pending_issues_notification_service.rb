# frozen_string_literal: true

module ReleaseTools
  module Security
    class PendingIssuesNotificationService
      include ::SemanticLogger::Loggable
      include ReleaseTools::Security::IssueHelper

      PendingIssuesError = Class.new(StandardError)

      def initialize
        issue_crawler = Security::IssueCrawler.new

        @linked_security_issues = issue_crawler.related_security_issues
      end

      def execute
        if pending_issues.empty?
          logger.info("All the issues linked to the security tracking issue are ready.")
          send_slack_notification(:success)
        else
          logger.info(log_failure_message)

          raise PendingIssuesError
        end
      rescue StandardError => ex
        error_message = "Pending issues check failed. If this job continues to fail, manually check that the issues linked to the tracking issue (#{security_tracking_issue.url}) are ready to be processed (labeled with `security-target`), and unlink them if they are not ready."
        logger.fatal(error_message, error: ex)

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :linked_security_issues

      ## Returns a list of ReleaseTools::Security::ImplementationIssue that are linked, but not ready to be processed.
      def pending_issues
        pending_issues = []

        linked_security_issues.each do |security_issue|
          unless security_issue.ready_to_be_processed?
            pending_issues << security_issue
          end
        end

        pending_issues
      end

      def send_slack_notification(status)
        ::ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: 'Pending issues',
          status: status,
          release_type: :patch
        ).send_notification
      end

      def log_failure_message
        message = "\n\n\u{274C} Some linked issues for the tracking issue #{security_tracking_issue.url} are not ready to be processed:\n\n"

        pending_issues_message = pending_issues.map do |issue|
          "- #{issue.web_url}"
        end

        message + pending_issues_message.join("\n")
      end
    end
  end
end
