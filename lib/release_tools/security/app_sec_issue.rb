# frozen_string_literal: true

module ReleaseTools
  module Security
    class AppSecIssue < ReleaseTools::Issue
      include ::SemanticLogger::Loggable
      include ReleaseTools::Security::IssueHelper

      def title
        "AppSec Task Issue for Patch Release: #{versions_title}"
      end

      def confidential?
        true
      end

      def labels
        'AppSecWorkType::SecurityReleaseRotation'
      end

      def security_issues
        IssuesFetcher
          .new(ReleaseTools::Security::Client.new)
          .execute
      end

      def due_date
        security_tracking_issue.due_date
      end

      def assignees
        ReleaseManagers::Schedule.new.active_appsec_release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      def project
        ReleaseTools::Project::GitlabEe
      end

      # This method is used by the Task::Helper, intentionally set
      # to nil since we don't want to link this issue to another one.
      def link!
        nil
      end

      private

      def versions_title
        versions.join(', ')
      end

      def template_path
        File.expand_path('../../../templates/patch_release/app_sec_issue.md.erb', __dir__)
      end

      def versions
        ReleaseTools::PatchRelease::Coordinator.new.versions
      end
    end
  end
end
