# frozen_string_literal: true

module ReleaseTools
  module Security
    class ImplementationIssueProcessor
      include ::SemanticLogger::Loggable

      ERROR_TEMPLATE = <<~TEMPLATE.strip
        %<appsec_usernames>s

        This security implementation issue does not meet the requirement for automation.
        Please resolve all the errors listed below to ensure the implementation security issue
        can be processed by the release automation:

        ## Errors

        The following errors were detected:

        %<errors>s

        #{Messaging::COMMENT_FOOTNOTE}
      TEMPLATE

      # @param [ReleaseTools::Security::ImplementationIssue] issue
      def initialize(issue)
        @issue = issue
        @client = ReleaseTools::Security::Client.new
        @validator = ReleaseTools::Security::ImplementationIssueValidator.new(issue)
      end

      def execute
        validator.validate

        if validator.errors.any?
          logger.fatal('Invalid security implementation issue', issue: issue.web_url)

          add_note_to_implementation_issue
        else
          logger.info('Valid security implementation issue', issue: issue.web_url)
        end
      end

      private

      attr_reader :issue, :validator, :client

      def add_note_to_implementation_issue
        logger.trace(__method__, issue: issue.web_url, errors: validator.errors.count)

        return if SharedStatus.dry_run?

        Retriable.with_context(:api) do
          client.create_issue_note(
            issue.project_id,
            issue.iid,
            format(
              ERROR_TEMPLATE,
              appsec_usernames: appsec_usernames,
              errors: validator.errors.join("\n\n")
            )
          )
        end
      end

      def appsec_usernames
        appsec_release_managers
          .collect(&:username)
          .map { |username| "@#{username}" }
          .join(', ')
      end

      def appsec_release_managers
        ReleaseTools::ReleaseManagers::Schedule
          .new
          .active_appsec_release_managers
      end
    end
  end
end
