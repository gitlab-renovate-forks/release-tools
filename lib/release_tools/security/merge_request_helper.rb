# frozen_string_literal: true

module ReleaseTools
  module Security
    module MergeRequestHelper
      def security_blog_merge_request
        @security_blog_merge_request ||= Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.security_blog_merge_request
        end
      end

      def canonical_blog_merge_request
        @canonical_blog_merge_request ||= Retriable.with_context(:api) do
          ReleaseTools::GitlabClient.security_blog_merge_request(security: false)
        end
      end
    end
  end
end
