# frozen_string_literal: true

module ReleaseTools
  module Security
    class PendingDeployNotificationService
      include ::SemanticLogger::Loggable

      PendingDeployError = Class.new(StandardError)

      def initialize
        @gitlab_client = ReleaseTools::GitlabClient.client
        @project = ReleaseTools::Project::GitlabEe
        @pending_mrs = []
      end

      def execute
        collect_pending_mrs

        if pending_mrs.empty?
          logger.info("All merged MRs in #{project.security_path} have been deployed to Production.")
          send_slack_notification(:success)
        else
          logger.info(log_failure_message)

          raise PendingDeployError
        end
      rescue StandardError => ex
        error_message = "Pending deployment check failed. If this job continues to fail, the deploy statuses of the default MRs should be checked manually or through chatops command: `/chatops run auto_deploy security_status`."
        logger.fatal(error_message, error: ex)

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :gitlab_client, :project, :pending_mrs

      def collect_pending_mrs
        merged_query = { state: 'merged', target_branch: project.default_branch, per_page: 50 }
        deployed_query = merged_query.merge(environment: 'gprd')

        merged_mrs = gitlab_client.merge_requests(project.security_path, merged_query).map(&:to_h)
        deployed_mrs = gitlab_client.merge_requests(project.security_path, deployed_query).map(&:to_h)

        @pending_mrs = merged_mrs - deployed_mrs
      end

      def send_slack_notification(status)
        ::ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: 'Pending deploy',
          status: status,
          release_type: :patch
        ).send_notification
      end

      def log_failure_message
        message = "\n\n\u{274C} Some merged MRs in #{project.security_path} haven't been deployed to Production:\n\n"

        pending_mrs_message = pending_mrs.filter_map do |mr|
          "- #{mr['web_url']}"
        end.compact

        message + pending_mrs_message.join("\n")
      end
    end
  end
end
