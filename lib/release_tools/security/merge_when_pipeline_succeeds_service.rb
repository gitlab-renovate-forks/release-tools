# frozen_string_literal: true

module ReleaseTools
  module Security
    class MergeWhenPipelineSucceedsService < ReleaseTools::Services::MergeWhenPipelineSucceedsService
      def initialize(client, merge_request)
        @client = client
        @merge_request = merge_request
      end

      def execute
        Retriable.with_context(:api) do
          trigger_pipeline

          wait_for_mr_pipeline_to_start

          merge_when_pipeline_succeeds
        end
      rescue PipelineNotReadyError
        logger.warn('Pipeline not ready', merge_request: merge_request.web_url)
      end

      private

      def trigger_pipeline
        if pipeline_running?
          logger.info('Not triggering pipeline since one is already running', merge_request: merge_request.web_url)
          return
        end

        logger.info('Triggering pipeline for merged results', merge_request: merge_request.web_url)

        return if SharedStatus.dry_run?

        GitlabClient.create_merge_request_pipeline(
          merge_request.project_id,
          merge_request.iid
        )
      end

      def pipeline_running?
        pipeline =
          Retriable.with_context(:api) do
            latest_pipeline
          end

        logger.debug('Latest pipeline', merge_request: merge_request.web_url, pipeline_id: pipeline&.id, status: pipeline&.status)

        PIPELINE_READY_STATES.include?(pipeline&.status)
      end

      def wait_for_mr_pipeline_to_start
        logger.info('Waiting for a merge request pipeline')

        return if SharedStatus.dry_run?

        # pipeline for merge requests will be generated only after creating the merge request.
        # here we wait until the pipeline is created with an exponential back-off
        # In case of a high load, creating the pipeline may take more than 1 minute.
        # base_interval 5 and tries 10 will hit the 1 minute timeline on retry n 5 (in average)
        # and wait up to 6 minutes (in average) on the worst case.
        Retriable.with_context(:pipeline_created) do
          pipeline = latest_pipeline

          raise PipelineNotReadyError unless PIPELINE_READY_STATES.include?(pipeline&.status)

          logger.info('Merge request pipeline', pipeline: pipeline.web_url, status: pipeline.status.to_s)

          pipeline
        end
      end

      def merge_when_pipeline_succeeds_options
        {
          merge_when_pipeline_succeeds: true,
          squash: true,
          should_remove_source_branch: true
        }
      end

      def latest_pipeline
        GitlabClient.pipelines(merge_request.project_id, ref: merge_pipeline_ref, per_page: 1).first
      end

      def merge_pipeline_ref
        "refs/merge-requests/#{merge_request.iid}/merge"
      end
    end
  end
end
