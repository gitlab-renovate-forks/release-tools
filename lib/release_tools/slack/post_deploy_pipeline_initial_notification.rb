# frozen_string_literal: true

module ReleaseTools
  module Slack
    class PostDeployPipelineInitialNotification
      def no_pending_post_migrations_message
        return if SharedStatus.dry_run?

        text = 'No pending post migrations available'
        message = "#{text}, skipping #{post_deploy_pipeline_url} execution"
        foreword_message = ":ci_skipped: #{message}"

        blocks = foreword_slack_blocks(foreword_message)

        send_chatops_notification(text, blocks)
      end

      def wraparound_vacuum_on_post_migrations_message
        return if SharedStatus.dry_run?

        text = 'Wraparound autovacuum detected'
        linked_text = wraparound_vacuum_view_link(text)
        message = "#{linked_text}, skipping #{post_deploy_pipeline_url} execution. Please retry the PDM at a later time"
        foreword_message = ":ci_skipped: #{message}"

        blocks = foreword_slack_blocks(foreword_message)

        send_chatops_notification(text, blocks)
      end

      def production_status_failed_message(production_status)
        return if SharedStatus.dry_run?

        message = 'Post-deployment pipeline is blocked'

        foreword_message =
          ":red_circle: #{release_managers_mention} #{message}"

        blocks =
          foreword_slack_blocks(foreword_message) + production_status.to_slack_blocks

        send_chatops_notification(message, blocks)
      end

      private

      def post_deploy_pipeline_url
        "<#{ENV.fetch('CI_PIPELINE_URL')}|post-deploy pipeline>"
      end

      def wraparound_vacuum_view_link(text)
        Slack::Utilities.api_link(text, grafana_wraparound_vacuum_view_link)
      end

      def grafana_wraparound_vacuum_view_link
        Mimir::Utils.explore_pane_url(
          query: 'pg_stat_activity_autovacuum_age_in_seconds{type="patroni-ci", relname=~".*wraparound.*"}',
          from: 'now-2h',
          to: 'now'
        )
      end

      def foreword_slack_blocks(message)
        text = StringIO.new
        text.puts(message)
        text.puts

        blocks = ::Slack::BlockKit.blocks
        blocks.section { |block| block.mrkdwn(text: text.string) }
        blocks.as_json
      end

      def release_managers_mention
        "<!subteam^#{ReleaseTools::Slack::RELEASE_MANAGERS}>"
      end

      def send_chatops_notification(text, blocks)
        Retriable.retriable do
          ReleaseTools::Slack::ChatopsNotification.fire_hook(
            text: text,
            channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
            blocks: blocks
          )
        end
      end
    end
  end
end
