# frozen_string_literal: true

module ReleaseTools
  module Slack
    module ReleasePipelineHelpers
      EMOJI = {
        patch: ":security-tanuki:",
        monthly: ":release:",
        internal: ":internal-handbook:"
      }.freeze
    end
  end
end
