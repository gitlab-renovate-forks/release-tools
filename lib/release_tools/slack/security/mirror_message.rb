# frozen_string_literal: true

module ReleaseTools
  module Slack
    module Security
      class MirrorMessage
        def initialize(security_mirrors:)
          @security_mirrors = security_mirrors
        end

        def general_status
          blocks = ::Slack::BlockKit.blocks

          security_mirrors.each do |mirror|
            mirror_block(mirror, blocks)
          end

          post_status(blocks)
        end

        def job_status
          blocks = ::Slack::BlockKit.blocks

          blocks.section do |section|
            section.mrkdwn(text: job_message)
          end

          post_status(blocks)
        end

        def synced_repositories?
          @synced_repositories ||= security_mirrors.all?(&:complete?)
        end

        private

        attr_reader :security_mirrors

        def mirror_block(mirror, blocks)
          canonical = mirror.canonical

          blocks.context do |context|
            unless canonical['avatar_url'].nil?
              context
                .image(url: canonical['avatar_url'], alt_text: canonical['name'])
            end

            context
              .mrkdwn(text: "*#{canonical['name']}* -- #{mirror.mirror_chain}")
          end

          # rubocop:disable Style/GuardClause
          if mirror.security_error
            blocks.section do |s|
              s.mrkdwn(text: "*Security*:\n```#{mirror.security_error}```")
            end
          end

          if mirror.build_error
            blocks.section do |s|
              s.mrkdwn(text: "*Build*:\n```#{mirror.build_error}```")
            end
          end
          # rubocop:enable Style/GuardClause
        end

        def post_status(blocks)
          Slack::Message
            .post(channel: channel, blocks: blocks.as_json, message: job_message)
        end

        def job_message
          [].tap do |text|
            text << ':security-tanuki:'
            text << (synced_repositories? ? ':ci_passing:' : ':ci_failing:')
            text << job_text_message
          end.join(' ')
        end

        def job_text_message
          if synced_repositories?
            "*Mirror check <#{job_url}|successfully> executed*"
          else
            "*Some projects are out of sync, review the Slack output or the <#{job_url}|job log> for details*"
          end
        end

        def channel
          ENV.fetch('CHAT_CHANNEL', F_UPCOMING_RELEASE)
        end

        def job_url
          ENV.fetch('CI_JOB_URL')
        end
      end
    end
  end
end
