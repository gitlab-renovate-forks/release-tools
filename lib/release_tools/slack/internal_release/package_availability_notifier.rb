# frozen_string_literal: true

module ReleaseTools
  module Slack
    module InternalRelease
      class PackageAvailabilityNotifier
        include ::SemanticLogger::Loggable

        def initialize(versions)
          @versions = versions
        end

        def execute
          logger.info('Notifying release managers about package availability via Slack')

          send_notification
        end

        private

        attr_reader :versions

        def send_notification
          message = <<~BODY
            :white_check_mark: :mega: #{release_managers_mention} The internal packages for #{regular_versions} are available on the pre-release channel.
            Please notify the SIRT channel and coordinate with the Dedicated team.
          BODY

          Retriable.retriable do
            ReleaseTools::Slack::ChatopsNotification.fire_hook(
              text: "The internal packages are now ready.",
              channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
              blocks: [
                {
                  type: 'section',
                  text: {
                    type: 'mrkdwn',
                    text: message
                  }
                }
              ]
            )
          end
        end

        def release_managers_mention
          "<!subteam^#{ReleaseTools::Slack::RELEASE_MANAGERS}>"
        end

        def regular_versions
          versions.map(&:to_minor).join(' and ')
        end
      end
    end
  end
end
