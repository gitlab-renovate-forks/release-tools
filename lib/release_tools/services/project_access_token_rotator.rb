# frozen_string_literal: true

module ReleaseTools
  module Services
    class ProjectAccessTokenRotator
      include ::SemanticLogger::Loggable

      RELEASE_TOOLS = Project::ReleaseTools

      def initialize(token_name, project:, client: GitlabClient, min_available_days: 30, renew_for: 3.month)
        @token_name = token_name
        @client = client
        @project = project
        @min_available_days = min_available_days
        @renew_for = renew_for

        @token_id = nil
        @rotated = false
        @new_token = nil
      end

      def execute
        return if expires_in_days > @min_available_days

        rotate_token!
      end

      def rotated?
        @rotated
      end

      def expires_in_days
        (token_info.expires_at.to_date - Date.today).days.in_days
      end

      def update_ci_var(key)
        return unless @rotated && @new_token

        logger.info('Updating CI variable', project: RELEASE_TOOLS.ops_path, key: key)

        Retriable.with_context(:api) do
          GitlabOpsClient.update_project_variable(RELEASE_TOOLS, key, @new_token).tap do
            @new_token = nil
          end
        end
      end

      private

      attr_reader :client

      def token_info
        logger.info('Searching active token', project: @project, token_name: @token_name)

        Retriable.with_context(:api) do
          client.project_access_tokens(@project).auto_paginate do |token|
            next unless token.name == @token_name
            next unless token.active
            next if token.revoked

            logger.info('Got token info', project: @project, token_name: token.name, token_id: token.id, revoked: token.revoked, expiry_date: token.expires_at)

            @token_id = token.id
            return token
          end
        end
      end

      def rotate_token!
        logger.info('Rotating token', project: @project, token_id: @token_id)

        Retriable.with_context(:api) do
          client.rotate_project_access_token(@project, @token_id, expires_at: (Date.today + @renew_for).iso8601).tap do |token|
            logger.info('Rotated token', project: @project, token_id: token.id, name: token.name, revoked: token.revoked, expiry_date: token.expires_at)

            @token_id = token.id
            @new_token = token.token
            @rotated = true
          end
        end
      end
    end
  end
end
