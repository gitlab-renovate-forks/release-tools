# frozen_string_literal: true

module ReleaseTools
  module Services
    # MergeWhenPipelineSucceedsService will approve a merge request and set it
    # to merge when pipeline succeeds
    class MergeWhenPipelineSucceedsService
      include ::SemanticLogger::Loggable

      PipelineNotReadyError = Class.new(StandardError)
      MWPSNotSetError = Class.new(StandardError)

      # we consider a pipeline ready only when in one of the following states
      PIPELINE_READY_STATES = %w[running pending].freeze

      MERGE_STATUS_WAIT_FOR_CI = %w[ci_must_pass ci_still_running].freeze

      attr_reader :client, :merge_request

      # Initialize the service
      #
      # @param merge_request [ReleaseTools::MergeRequest] the merge request to approve and MWPS
      # @param token [string] the GitLab private token, it should not belong to the merge request's author
      # @param endpoint [string] the GitLab API endpoint
      def initialize(merge_request, token:, endpoint: GitlabClient::DEFAULT_GITLAB_API_ENDPOINT)
        @merge_request = merge_request
        @token = token
        @client = Gitlab::Client.new(endpoint: endpoint, private_token: token, httparty: GitlabClient.httparty_opts)
      end

      # execute starts the service operations
      #
      # This method is idempotent
      #
      # @raise [PipelineNotReadyError] when cannot find a ready pipeline for the merge request
      def execute
        Services::ApproveService.new(merge_request, token: @token).execute

        Retriable.with_context(:api) do
          merge_when_pipeline_succeeds
        rescue Gitlab::Error::MethodNotAllowed => ex
          # The MR cannot be merged, and the API should be retried once the conditions for merging
          # are met (for example, pipeline needs to succeed). Or just retry after some time.
          logger.error(
            'Unable to set MWPS',
            merge_request: merge_request.web_url,
            error: ex.message,
            detailed_merge_status: merge_request.detailed_merge_status
          )
        end
      end

      def merge_when_pipeline_succeeds_options
        {
          merge_when_pipeline_succeeds: true,
          should_remove_source_branch: true
        }
      end

      def merge_when_pipeline_succeeds
        return if SharedStatus.dry_run?

        logger.info('Setting MWPS', merge_request: merge_request.web_url)

        response = client.accept_merge_request(
          merge_request.project_id,
          merge_request.iid,
          **merge_when_pipeline_succeeds_options
        )

        logger.info(
          'Called merge API',
          merge_request: merge_request.web_url,
          detailed_merge_status: response.detailed_merge_status,
          merge_when_pipeline_succeeds: response.merge_when_pipeline_succeeds
        )

        # ci_still_running or ci_must_pass is the response when the MR pipeline is still in progress. The merge API should be
        # retried after some time. Currently, this class is only used by UpdateGitaly and UpdateKas which run
        # every 30 mins in the component:update_gitaly scheduled pipeline.
        if response.state != 'merged' && !response.merge_when_pipeline_succeeds && MERGE_STATUS_WAIT_FOR_CI.exclude?(response.detailed_merge_status)
          raise MWPSNotSetError
        end

        response
      end
    end
  end
end
