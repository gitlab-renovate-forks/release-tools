# frozen_string_literal: true

module ReleaseTools
  module Services
    class CreateVersionService
      include ::SemanticLogger::Loggable

      def initialize(version:)
        @client = ReleaseTools::VersionClient
        @version = version
      end

      def execute
        logger.info('Creating release version',
                    version: version,
                    release_type: release_type,
                    version_type: version_type)

        return if SharedStatus.dry_run?

        if version_exists?
          logger.info('Version exist, skipping')
          return
        end

        Retriable.with_context(:api) do
          create_version
        end

        send_slack_notification(:success)
      rescue StandardError => ex
        logger.fatal(error_message, error: ex)

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :client, :version

      def monthly?
        ReleaseTools::Version.new(version).monthly?
      end

      def patch?
        ReleaseTools::Version.new(version).patch?
      end

      def patch_critical?
        patch? && ReleaseTools::SharedStatus.critical_patch_release?
      end

      def release_type
        patch? ? :patch : :monthly
      end

      def version_type
        if monthly?
          :monthly
        elsif patch_critical?
          :patch_critical
        else
          :patch_regular
        end
      end

      def create_version
        response = client.create_version(
          version,
          version_type: version_type
        )

        return if response.respond_to?(:version)

        logger.fatal(response.to_hash['message'])
        raise "Error: #{response.to_hash['message']}"
      end

      def version_exists?
        response = client.get_version(version)
        response.respond_to?(:version)
      end

      def error_message
        <<~MSG
          The version failed to be created, retry this job. If the failure persists, proceed to create the version with https://version.gitlab.com/versions/new?version=#{version}
        MSG
      end

      def send_slack_notification(status)
        ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: "Create new version #{version}",
          status: status,
          release_type: release_type
        ).send_notification
      end
    end
  end
end
