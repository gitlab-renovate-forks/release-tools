# frozen_string_literal: true

module ReleaseTools
  module Services
    class BranchesStatusService
      include ::ReleaseTools::Security::ComponentBranchHelper
      include ::SemanticLogger::Loggable

      PROJECTS = ReleaseTools::ManagedVersioning::PROJECTS + [ReleaseTools::Project::GitlabCe]

      FailedBranchError = Class.new(StandardError)

      # @param release_type [Symbol] type of release (:monthly, :patch, :internal)
      # @param versions [Array<ReleaseTools::Version>] An array of version objects to check, regardless of release_type
      def initialize(release_type:, versions:)
        @release_type = release_type
        @versions     = versions
        @projects     = PROJECTS
        @statuses     = []
      end

      def execute
        if branches_statuses.all? { |obj| obj[:status] == 'success' }
          logger.info('All branches are green! \u{2705}')
          send_slack_notification(:success)
        else
          logger.info(log_failure_message)

          raise FailedBranchError
        end
      rescue StandardError => ex
        logger.fatal(error_message, error: ex) unless ex.is_a?(FailedBranchError)

        send_slack_notification(:failed)

        raise
      end

      private

      attr_reader :projects, :versions, :statuses, :release_type

      def branches_statuses
        generate_link_list(projects, versions, release_type) do |project_name, branch, link|
          status = ::ReleaseTools::Services::ComponentStatusService.new(
            project: projects.detect { |p| p.metadata_project_name == project_name },
            branch: branch,
            release_type: release_type
          ).execute

          statuses << {
            project_name: project_name,
            branch: branch,
            status: status,
            link: link
          }
        end

        statuses
      end

      def send_slack_notification(status)
        ::ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: 'Branches Status',
          status: status,
          release_type: release_type
        ).send_notification
      end

      def error_message
        branch_statuses = statuses.map { |obj| "- #{obj[:project_name]} - #{obj[:branch]}: #{obj[:link]}" }

        message = "Project status check failed. If this job continues to fail, the status of the projects can be checked manually:\n\n"
        message + branch_statuses.join("\n")
      end

      def log_failure_message
        branch_statuses =
          statuses.filter_map do |obj|
            next if obj[:status] == "success"

            "- #{obj[:project_name]} - #{obj[:branch]} - #{obj[:status]}: #{obj[:link]}"
          end.compact

        message = "\n\n\u{274C} The following branches do not have green pipelines:\n\n"
        message + branch_statuses.join("\n")
      end
    end
  end
end
