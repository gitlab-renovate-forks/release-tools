# frozen_string_literal: true

module ReleaseTools
  module Services
    module PackagesStatus
      class Functionality < Base
        private

        def bridge_job_name
          'check-packages-functionality'
        end
      end
    end
  end
end
