# frozen_string_literal: true

module ReleaseTools
  module Services
    module OmnibusPackages
      class Publishing < Base
        private

        def action_progressive_tense
          "publishing"
        end

        def packages_service
          PackagesStatus::Availability
        end
      end
    end
  end
end
