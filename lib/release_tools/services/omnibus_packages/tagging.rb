# frozen_string_literal: true

module ReleaseTools
  module Services
    module OmnibusPackages
      class Tagging < Base
        private

        def action_progressive_tense
          "tagging"
        end

        def packages_service
          PackagesStatus::Functionality
        end
      end
    end
  end
end
