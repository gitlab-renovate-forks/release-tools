# frozen_string_literal: true

module ReleaseTools
  module Support
    module UbiHelper
      DEFAULT_UBI_VERSION = '8'
      UBI_DROP_SUFFIX_VERSION = '17.3.0' # When https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1888 was merged

      def ubi?(version)
        version.ee?
      end

      def ubi_tag(version, ubi_version = nil)
        if version >= Version.new(UBI_DROP_SUFFIX_VERSION) && Feature.enabled?(:drop_ubi_version_suffix)
          version.tag(ee: true).gsub(/-ee$/, "-ubi")
        else
          version.tag(ee: true).gsub(/-ee$/, "-ubi#{ubi_version || DEFAULT_UBI_VERSION}")
        end
      end
    end
  end
end
