# frozen_string_literal: true

module ReleaseTools
  module Support
    module FIPSHelper
      GITLAB_FIPS_MINIMUM_VERSION = '15.1.0' # When https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/981 was introduced

      def fips?(version)
        version.ee?
      end

      def fips_tag(version)
        version.tag(ee: true).gsub(/-ee$/, "-fips")
      end
    end
  end
end
