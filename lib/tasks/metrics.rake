# frozen_string_literal: true

desc 'Execute all metrics tasks'
task :metrics do
  include ReleaseTools::ParallelMethods

  tasks = %w[
    auto_build_pressure
    auto_deploy_pressure
    mirror_status
    release_status
    patch_release_pressure
  ]

  errors = []

  parallel_each(tasks) do |task|
    Rake::Task["metrics:#{task}"].invoke
  rescue StandardError => ex
    ReleaseTools.logger.warn("Error updating metric", { task: task }, ex)

    errors << ex
  end

  abort if errors.any?
end

namespace :metrics do
  desc "Update auto-build pressure metric"
  task :auto_build_pressure do
    ReleaseTools::Metrics::AutoBuildPressure.new.execute
  end

  desc "Update auto-deploy pressure metric"
  task :auto_deploy_pressure do
    ReleaseTools::Metrics::AutoDeployPressure.new.execute
  end

  desc "Update mirror status metric"
  task :mirror_status do
    ReleaseTools::Metrics::MirrorStatus.new.execute
  end

  desc "Update release status metrics"
  task :release_status do
    ReleaseTools::Metrics::ReleaseMetricUpdater.new.execute
  end

  desc "Increment deployment_started_total for $DEPLOY_ENVIRONMENT environment"
  task :deployment_started do
    ReleaseTools::Tasks::Metrics::TrackDeploymentStarted.new.execute
  end

  desc "Increment deployment_failed_atleast_once for $DEPLOY_VERSION deploy version"
  task :deployment_failed_atleast_once do
    ReleaseTools::Tasks::Metrics::TrackDeploymentFailedAtleastOnce.new.execute
  end

  desc 'Track deployment metrics'
  task :deployment_metrics, [:environment, :version] do |_, args|
    env = args[:environment]
    version = args[:version]
    ReleaseTools::Tasks::Metrics::DeploymentMetrics::MergeRequestLeadTime.new.execute(env, version)
  end

  desc 'Track environment state'
  task :environment_state, [:environment, :event] do |_, args|
    env = args[:environment]
    event = args[:event]
    ReleaseTools::Tasks::Metrics::SetEnvironmentState.new(env, event).execute
  end

  desc 'Track locked state'
  task :environment_lock_state, [:environment, :lock_reason] do |_, args|
    env = args[:environment]
    lock_reason = args[:lock_reason].presence
    ReleaseTools::Tasks::Metrics::SetEnvironmentLockState.new(env, lock_reason).execute
  end

  desc "Increment deployment_completed_total for $DEPLOY_ENVIRONMENT environment"
  task :deployment_completed do
    ReleaseTools::Tasks::Metrics::TrackDeploymentCompleted.new.execute
  end

  desc "Update patch release pressure metric"
  task :patch_release_pressure do
    ReleaseTools::Metrics::PatchReleasePressure.new.execute
  end

  desc "Update pipeline and job duration metrics"
  task :pipeline_and_job_duration, [:pipeline_url, :deploy_version] do |_, args|
    pipeline_url = args[:pipeline_url]
    deploy_version = args[:deploy_version]

    ReleaseTools::PipelineTracer::MetricsService
      .from_pipeline_url(pipeline_url, version: deploy_version, depth: 3)
      .execute
  end

  desc "Set package state metric"
  task :package_state do
    ReleaseTools::Metrics::AutoDeployPackageState
      .new
      .execute
  end

  desc "Set package state metric for new packages"
  task :building_package_state, [:event] do |_, args|
    event = args[:event]
    metric_obj = ReleaseTools::Metrics::AutoDeployBuildingPackageState.new

    case event
    when 'building'
      metric_obj.package_building
    when 'success'
      metric_obj.package_built
    when 'failed'
      metric_obj.package_building_failed
    when 'deployment_completed'
      metric_obj.deployment_completed
    else
      raise "Unknown event: #{event}"
    end
  end

  namespace :monthly_release do
    task :open do
      ReleaseTools::Metrics::MonthlyReleaseStatus.new(status: :open).execute
    end

    task :announced do
      ReleaseTools::Metrics::MonthlyReleaseStatus.new(status: :announced).execute
    end

    task :tagged_rc do
      ReleaseTools::Metrics::MonthlyReleaseStatus.new(status: :tagged_rc).execute
    end
  end

  namespace :patch_release do
    task :open do
      ReleaseTools::Metrics::PatchReleaseStatus.new(status: :open).execute
    end

    task :warning do
      ReleaseTools::Metrics::PatchReleaseStatus.new(status: :warning).execute
    end

    task :closed do
      ReleaseTools::Metrics::PatchReleaseStatus.new(status: :closed).execute
    end
  end
end
