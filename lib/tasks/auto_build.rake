# frozen_string_literal: true

namespace :auto_build do
  desc "Lifecyle timer"
  task :timer do
    ReleaseTools::Tasks::AutoBuild::Timer.new.execute
  end
end
