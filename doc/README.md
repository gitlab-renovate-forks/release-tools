# Documentation

- [Rake tasks](./rake-tasks.md) contained in this project
- [ChatOps triggers](./chatops.md) available from this project
- [Issue templates](../templates) used for creating the release task lists
- [CI variables](./variables.md) used by this project
- [Metrics](./metrics.md) contains information on developing metrics gathered
  by this project
- [Releases using the API](./api-releases.md) provides information about the
  code used for performing releases using the API
- [Deployment health metrics](./deployment_health_metrics.md) contains information
  about metrics used to determine if production is healthy and can be deployed to.

## Development

### Tool versioning

This project has adopted [asdf
version-manager](https://github.com/asdf-vm/asdf) for tool
versioning. Using `asdf` is recommended, although not
mandatory. Please note that if you chose not to use `asdf`, you'll
need to ensure that all the required binaries, and the correct
versions, are installed and on your path.

#### Dependencies and required tooling

The following compilers and tools are required to work on this repository:

- Ruby programming language
- Go programming language
- `golangci-lint` linter
- `mockery` mock generator

#### Manage your dependencies using `mise`

Follow the [`mise` setup](https://mise.jdx.dev/) and then run `mise install`.

Running this command will automatically install the versions of each
tool, as specified in `.tool-versions` file.

To verify the result, please run `mise ls --current`.

``` shell
$ mise ls --current
Tool           Version             Config Source                            Requested
gitleaks       8.18.4              ~/src/infra/release-tools/.tool-versions 8.18.4
go             1.22.6              ~/src/infra/release-tools/.tool-versions 1.22.6
golangci-lint  1.59.1              ~/src/infra/release-tools/.tool-versions 1.59.1
mockery        2.45.0              ~/src/infra/release-tools/.tool-versions 2.45.0
ruby           3.3.4               ~/src/infra/release-tools/.tool-versions 3.3.4
yq             4.44.3              ~/src/infra/release-tools/.tool-versions 4.44.3
```

### Setup

Follow the steps described in [rake-tasks.md](rake-tasks.md#setup).

#### git-hooks static analysis

[Lefthook](https://github.com/Arkweid/lefthook) is a Git hooks manager
that allows custom logic to be executed prior to Git committing or
pushing.

We have a `lefthook.yml` checked in but it is ignored until Lefthook
is installed.

1. Install the `lefthook` Ruby gem:

   ```shell
   bundle install
   ```

1. Install Lefthook managed Git hooks:

   ```shell
   bundle exec lefthook install
   ```

1. Test Lefthook is working by running the Lefthook `pre-commit` Git hook:

   ```shell
   bundle exec lefthook run pre-commit
   ```

This should return the Lefthook version and the list of executable
commands with output.

To disable Lefthook temporarily, you can set the `LEFTHOOK`
environment variable to `0`. For instance:

```shell
LEFTHOOK=0 git commit ...
```

### Ruby console

Start a ruby console with:

```shell
bundle exec pry --gem
```

### Dry run mode

To execute code in dry-run mode, add the following environment variable when
executing rake tasks: `TEST='true'`.

Before performing any edit actions or any action that should not happen in dry-run mode, consider adding a dry-run check before it. For example:

```ruby
logger.debug("About to perform edit action", relevant_data: data)

return if ReleaseTools::SharedStatus.dry_run?

# Perform edit action
```

This check ensures that actions are only executed when not in dry-run mode, helping to avoid unintended side effects during testing.

#### Testing with dry_run

By default, all tests are executed with the `TEST=true` flag already set. If you
want to verify that code guarded by a `dry_run?` check is executed, use the
`without_dry_run` helper method:

```ruby
it "executes the API call" do
  instance = described_class.new

  # Be sure to stub API calls so nothing's actually executed
  expect(ReleaseTools::GitlabClient)
    .to receive(:some_api_request)
    .and_return(true)

  without_dry_run do
    instance.execute
  end
end
```

If you don't stub the API call, [WebMock] will block the request and issue a
test failure.

[WebMock]: https://github.com/bblimke/webmock

### Retrying API requests with Retriable

Developers should program defensively when interacting with the GitLab API by
retrying requests that fail due to timeouts or other intermittent failures.

This project utilizes the [Retriable] gem to make this easier, and a context
has been added to simplify this common use case:

```ruby
# Automatically retry the request in the event of a `Timeout::Error`,
# `Errno::ECONNRESET`, or `Gitlab::Error::ResponseError` exception
Retriable.with_context(:api) do
  # ...
end

# Contexts support all additional Retriable parameters
Retriable.with_context(:api, tries: 10, on: StandardError) do
  # ...
end

# WARNING: Supplying `on` to `with_context` will *override* the exception list
#
# This block will only retry on `SomeRareException` and nothing else!
Retriable.with_context(:api, on: SomeRareException) do
  # ...
end
```

[Retriable]: https://github.com/kamui/retriable

### Feature flags

#### Procedure to remove a feature flag

1. Create MR to remove feature flag from code.
1. Merge MR once reviewed and approved.
1. Make sure that all pipelines started before the MR was merged have been
   completed: <https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines>.

   This is required because pipelines started before the MR was merged use an older
   commit, which will look for the feature flag.

1. Delete the feature flag from <https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags>.

### Release pipelines

Guidelines for developing pipelines for automating releases, such as the security pipeline,
are documented in [the release-pipelines doc](release-pipelines.md).

## Testing

Project changes must be made via [merge requests] and go through code review by
members of the Delivery team.

The project has a [fairly comprehensive][coverage] RSpec-based test suite. New
functionality should be covered by automated testing.

[merge requests]: https://gitlab.com/gitlab-org/release-tools/-/merge_requests
[coverage]: http://gitlab-org.gitlab.io/release-tools/coverage/

### Running tests

To run the full test suite, use `bundle exec rspec`. This project includes the
[Fuubar formatter](https://github.com/thekompanee/fuubar) for rapid failure
feedback:

```sh
bundle exec rspec -f Fuubar
```

Targeted tests can be executed by passing a specific file to test, and even a
specific line:

```
bundle exec rspec -f Fuubar -- spec/lib/release_tools/my_class_spec.rb:5
```

### Factories

This project makes use of [factory_bot] to easily build stubbed objects for
testing. Instead of mimicking ActiveRecord-like objects, they're intended to
substitute `Gitlab::ObjectifiedHash` objects returned from GitLab API calls by
the `gitlab` gem. They're built as `RSpec::Mock::Double` instances.

#### Examples

```ruby
# Mimic a merge request returned by the API
merge_request = build(:merge_request, :merged, source_branch: 'add_factory_bot')
merge_request.iid  # => 1
merge_request.state  #=> "merged"
merge_request.web_url  # => "https://example.com/foo/bar/-/merge_requests/1"

# Mimic a pipeline
pipeline = build(:pipeline, :running, ref: 'main')
pipeline.id  # => 2
pipeline.status  # => "running"
pipeline.ref  # => "main"
pipeline.sha  # => "69689ed508d1c21da6091d1e90d09a87dd453f68"
```

### Slack

To test notification messages, the `#release_tools_test` Slack channel can be used. The channel is defined on the
[`slack.rb` file].

```ruby
ReleaseTools::Slack::Message.post(
  channel: ReleaseTools::Slack::NOTIFICATION_TESTS,
  message: 'foo',
  blocks: slack_blocks
)
```

The following environment variable can also be used to test the notification messages: `SLACK_TEST='true'`

This ensure all Slack notifications are redirected to the dedicated test channel `#release_tools_tests`, isolating them from other communication channels.

#### Slack app

Release-tools uses a Slack app called `Release-Tools`.

Here are some useful links:

- Slack app in app directory: <https://gitlab.slack.com/apps/A0385PGTMSQ-release-tools?tab=settings&next_id=0>
- App configuration: <https://api.slack.com/apps/A0385PGTMSQ/general>

##### Changing permissions of Slack app

Some changes to the app config, for example permission scopes, need the app to be reinstalled
into the workspace for the changes to take effect. When a re-install is required, a message will appear at the top of
the page. For example: `You’ve changed the permission scopes your app uses. Please reinstall your app for
these changes to take effect (and if your app is listed in the Slack App Directory, you’ll need to resubmit it as well).`

When the app needs to be re-installed into the workspace, you need to do the following:

- Open an Access Request (example: <https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/18532>)
- Then go to <https://api.slack.com/apps/A0385PGTMSQ/install-on-team> and click the
"Request to Install" button. In the text box that opens up, you can paste a link to your
access request, and submit the request.

[factory_bot]: https://github.com/thoughtbot/factory_bot
[`slack.rb` file]: https://gitlab.com/gitlab-org/release-tools/-/blob/master/lib/release_tools/slack.rb

### Rake tasks

See the [rake tasks guide](rake-tasks.md#testing).
