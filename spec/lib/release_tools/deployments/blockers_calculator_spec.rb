# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockersCalculator do
  subject(:calculator) { described_class.new(blockers) }

  let(:blocker_flaky_test) do
    instance_double(
      ReleaseTools::Deployments::BlockerIssue,
      title: 'Blocker 1',
      root_cause: "RootCause::Flaky-Test",
      hours_gstg_blocked: 5.0,
      hours_gprd_blocked: 3.0,
      with_deploys_blocked_label?: true
    )
  end

  let(:blocker_feature_flag) do
    instance_double(
      ReleaseTools::Deployments::BlockerIssue,
      title: 'Blocker 2',
      root_cause: "RootCause::Feature-Flag",
      hours_gstg_blocked: 1.0,
      hours_gprd_blocked: 4.0,
      with_deploys_blocked_label?: true
    )
  end

  let(:blocker_flaky_test2) do
    instance_double(
      ReleaseTools::Deployments::BlockerIssue,
      title: 'Blocker 3',
      root_cause: "RootCause::Flaky-Test",
      hours_gstg_blocked: 1.0,
      hours_gprd_blocked: 0.5,
      with_deploys_blocked_label?: true
    )
  end

  let(:blockers) { [blocker_flaky_test, blocker_feature_flag, blocker_flaky_test2] }

  describe '#failure_types' do
    context 'when there are blockers with different failure types' do
      let(:blockers) { [blocker_flaky_test, blocker_feature_flag] }

      it 'returns a hash with failure types and their values' do
        expect(calculator.failure_types).to eq(
          "RootCause::Flaky-Test" => { gstg: 5.0, gprd: 3.0, count: 1 },
          "RootCause::Feature-Flag" => { gstg: 1.0, gprd: 4.0, count: 1 }
        )
      end
    end

    context 'when there are blockers with the same failure type' do
      let(:blockers) { [blocker_flaky_test, blocker_flaky_test2] }

      it 'returns a hash with failure types and their values' do
        expect(calculator.failure_types).to eq(
          "RootCause::Flaky-Test" => { gstg: 6.0, gprd: 3.5, count: 2 }
        )
      end
    end
  end

  describe '#total_failure_type_gstg' do
    it 'returns the total hours gstg blocked for all failure types' do
      expect(calculator.total_failure_type_gstg).to eq(7.0)
    end
  end

  describe '#total_failure_type_gprd' do
    it 'returns the total hours gprd blocked for all failure types' do
      expect(calculator.total_failure_type_gprd).to eq(7.5)
    end
  end
end
