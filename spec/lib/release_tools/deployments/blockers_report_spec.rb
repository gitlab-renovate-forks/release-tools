# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockersReport do
  subject(:report) { described_class.new }

  let(:client) { class_double(ReleaseTools::GitlabClient) }
  let(:blocker_issue_fetcher) { instance_double(ReleaseTools::Deployments::BlockerIssueFetcher) }

  let(:blocker1) { instance_double(ReleaseTools::Deployments::BlockerIssue, root_cause: "~\"RootCause::Flaky-Test\"") }
  let(:blocker2) { instance_double(ReleaseTools::Deployments::BlockerIssue, root_cause: "~\"RootCause::Feature-Flag\"") }

  let(:start_time) { Date.new(2024, 4, 22) }
  let(:end_time) { Date.new(2024, 4, 28) }

  let(:job_for_blockers_report) { "https://example.com/foo/bar/-/jobs/4" }
  let(:existing_issue) { create(:issue, iid: 1, project_id: 1) }
  let(:issue) { create(:issue, iid: 2, project_id: 2) }
  let(:project) { ReleaseTools::Project::Release::Tasks }

  before do
    stub_const('ReleaseTools::GitlabClient', client)
    allow(client).to receive(:create_issue).and_return(issue)

    allow(ReleaseTools::Deployments::BlockerIssueFetcher)
      .to receive(:new).and_return(blocker_issue_fetcher)

    allow(blocker_issue_fetcher).to receive(:fetch)

    allow(blocker_issue_fetcher).to receive_messages(
      uncategorized_incidents: [],
      start_time: start_time,
      end_time: end_time,
      deployment_blockers: [blocker1, blocker2]
    )

    allow(blocker1).to receive_messages(
      title: 'Blocker 1',
      hours_gstg_blocked: 2.0,
      hours_gprd_blocked: 1.0
    )

    allow(blocker2).to receive_messages(
      title: 'Blocker 2',
      hours_gstg_blocked: 3.0,
      hours_gprd_blocked: 2.0
    )
  end

  describe '#create' do
    let(:calculator) { instance_double(ReleaseTools::Deployments::BlockersCalculator) }

    before do
      allow(report).to receive_messages(
        assignees: [1, 2],
        existing_issue: existing_issue,
        create_new_issue: issue,
        start_time: start_time,
        end_time: end_time
      )
      # Mock the BlockersCalculator to isolate the BlockersReport behavior
      allow(ReleaseTools::Deployments::BlockersCalculator)
        .to receive(:new).and_return(calculator)
      allow(calculator).to receive_messages(
        failure_types: {},
        total_failure_type_gstg: 0,
        total_failure_type_gprd: 0
      )
    end

    context 'when there is an existing issue' do
      it 'updates the existing issue and adds a comment' do
        expect(client).to receive(:edit_issue).with(
          project.path,
          existing_issue.iid,
          { description: instance_of(String) }
        )
        expect(client).to receive(:create_issue_note).with(
          project,
          { issue: existing_issue, body: instance_of(String) }
        )

        report.create
      end
    end

    context 'when there is no existing issue' do
      before do
        allow(report).to receive_messages(existing_issue: nil, create_new_issue: issue)
      end

      it 'creates a new issue' do
        expect(report).to receive(:create_new_issue)

        result = report.create
        expect(result).to eq(issue)
      end
    end
  end

  describe '#assignees' do
    let(:schedule) do
      instance_double(
        ReleaseTools::ReleaseManagers::Schedule,
        active_release_managers: [create(:user, id: 1), create(:user, id: 2)]
      )
    end

    before do
      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)
    end

    it 'returns the current RM user IDs' do
      expect(report.assignees).to eq([1, 2])
    end

    context 'when VersionNotFoundError is raised' do
      let(:schedule_with_error) do
        instance_double(
          ReleaseTools::ReleaseManagers::Schedule,
          active_release_managers: [],
          active_appsec_release_managers: []
        )
      end

      before do
        allow(ReleaseTools::ReleaseManagers::Schedule)
          .to receive(:new)
          .and_return(schedule_with_error)

        allow(schedule_with_error)
          .to receive(:active_release_managers)
          .and_raise(ReleaseTools::ReleaseManagers::Schedule::VersionNotFoundError)
      end

      it 'returns nil' do
        expect(report.assignees).to be_nil
      end
    end
  end

  describe '#title' do
    it 'returns the title of the report' do
      allow(report).to receive_messages(
        start_time: start_time,
        end_time: end_time
      )
      expect(report.title).to eq("Deployment Blockers - Week: #{start_time}-#{end_time}")
    end
  end

  describe '#description' do
    let(:blocker4) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 4',
        root_cause: "~\"RootCause::Flaky-Test\"",
        data: double('data', labels: ["Deploys-blocked-gstg::5.0hr", "Deploys-blocked-gprd::3.0hr"]),
        with_deploys_blocked_label?: true
      )
    end

    let(:blocker5) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 5',
        root_cause: "~\"RootCause::Feature-Flag\"",
        data: double('data', labels: ["Deploys-blocked-gstg::1.0hr", "Deploys-blocked-gprd::4.0hr"]),
        with_deploys_blocked_label?: true
      )
    end

    let(:blocker6) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 6',
        root_cause: "~\"RootCause::Flaky-Test\"",
        data: double('data', labels: ["Deploys-blocked-gstg::1.0", "Deploys-blocked-gprd::0.5"]),
        with_deploys_blocked_label?: true
      )
    end

    let(:blocker7) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 7'
      )
    end

    before do
      allow(blocker4).to receive_messages(
        hours_gstg_blocked: 5.0,
        hours_gprd_blocked: 3.0
      )

      allow(blocker5).to receive_messages(
        hours_gstg_blocked: 1.0,
        hours_gprd_blocked: 4.0
      )

      allow(blocker6).to receive_messages(
        hours_gstg_blocked: 1.0,
        hours_gprd_blocked: 0.5
      )
    end

    it 'returns the content of the report' do
      allow(report).to receive_messages(
        start_time: start_time,
        end_time: end_time,
        deployment_blockers: [blocker4, blocker5, blocker6],
        uncategorized_incidents: [blocker7],
        job_for_blockers_report: job_for_blockers_report
      )

      expected_content = File.read('spec/fixtures/reports/deployment_blockers.html.md')

      expect(report.description).to eq(expected_content)
    end
  end
end
