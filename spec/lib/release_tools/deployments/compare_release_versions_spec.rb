# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::CompareReleaseVersions do
  subject(:comparer) do
    described_class.new(expected_version)
  end

  let(:client) { stub_const('ReleaseTools::GitlabReleaseClient', spy) }
  let(:version_attributes) { '17.1.2-ee' }

  before do
    allow(client.client)
      .to receive(:current_version)
      .and_return(version_attributes)
  end

  describe '#equal_to_actual_release_version?' do
    context 'when the actual version deployed to release.gitlab.net is equal to the expected version' do
      let(:expected_version) { '17.1.2-ee' }

      it 'returns true' do
        expect(comparer.equal_to_actual_release_version?).to be(true)
      end
    end

    context 'when the actual version deployed to release.gitlab.net is not equal to the expected version' do
      let(:expected_version) { '17.2.1-ee' }

      it 'returns false' do
        expect(comparer.equal_to_actual_release_version?).to be(false)
      end
    end
  end
end
