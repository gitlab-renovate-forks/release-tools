# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::ReleaseEnvironment::QaNotifier do
  subject(:notifier) do
    described_class.new(pipeline_url: pipeline_url,
                        environment_name: environment_name,
                        release_environment_version: release_environment_version)
  end

  let(:pipeline_url) { 'https://gitlab.com/gitlab-org/gitlab/-/pipelines/1' }
  let(:environment_name) { '16-10-stable' }
  let(:release_environment_version) { '16-10-stable-9dce3c3d' }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notification) { stub_const('ReleaseTools::Slack::ReleaseEnvironment::QaNotification', spy) }

  let(:job) do
    create(
      :job,
      :failed,
      name: 'release-environments-qa 1/2'
    )
  end

  let(:job2) do
    create(
      :job,
      :success,
      name: 'release-environments-qa 2/2'
    )
  end

  let(:jobs) do
    Gitlab::PaginatedResponse.new([job, job2])
  end

  before do
    allow(client)
      .to receive(:pipeline_jobs)
      .and_return(jobs)

    allow(ReleaseTools::Slack::ReleaseEnvironment::QaNotification)
      .to receive(:new)
      .and_return(instance_double(ReleaseTools::Slack::ReleaseEnvironment::QaNotification, execute: nil))
  end

  describe '#execute' do
    it 'sends a slack notification' do
      expect(notification)
        .to receive(:new)
        .with({ job: job, environment_name: environment_name, release_environment_version: release_environment_version })

      notifier.execute
    end

    context 'when the job is not found' do
      let(:job) do
        create(
          :job,
          :failed,
          name: 'something else'
        )
      end

      it 'does not send a slack notification' do
        expect(notification).not_to receive(:new)

        notifier.execute
      end
    end
  end

  context 'when security mirror is used,' do
    let(:pipeline_url) { 'https://gitlab.com/gitlab-org/security/gitlab/-/pipelines/1' }

    it 'the security path is used' do
      expect(ReleaseTools::Project::GitlabEe).to receive(:security_path)

      notifier.execute
    end
  end
end
