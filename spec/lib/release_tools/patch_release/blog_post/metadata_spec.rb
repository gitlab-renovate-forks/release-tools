# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::BlogPost::Metadata do
  subject(:metadata) do
    described_class.new(
      versions: versions,
      patch_coordinator: patch_coordinator
    )
  end

  let(:versions) { ['9.1.3'] }

  let(:patch_content) do
    [
      {
        version: ReleaseTools::Version.new('9.1.3'),
        pressure: 1,
        merge_requests: {
          'gitlab-org/gitlab' => [],
          'gitlab-org/gitaly' => [],
          'gitlab-org/omnibus-gitlab' => [{ 'title' => 'baz', 'web_url' => 'https://baz.com' }]
        }
      }
    ]
  end

  let(:patch_coordinator) do
    instance_spy(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content, pressure: 2, single_version?: false)
  end

  it 'returns a constant release_name' do
    expect(metadata.release_name).to eq(ReleaseTools::PatchRelease::BlogPost::Metadata::RELEASE_NAME)
  end

  context 'non critical patch release' do
    before do
      allow(ReleaseTools::SharedStatus).to receive(:critical_patch_release?).and_return(false)
    end

    it 'returns a non-critial title' do
      expect(metadata.blog_title).to eq('GitLab Patch Release: 9.1.3')
    end

    it 'returns a non-critical description' do
      expect(metadata.blog_description).to eq('Learn more about GitLab Patch Release: 9.1.3 for GitLab Community Edition (CE) and Enterprise Edition (EE).')
    end

    it 'returns an empty title_header' do
      expect(metadata.title_header).to be_blank
    end
  end

  context 'critical patch release' do
    before do
      allow(ReleaseTools::SharedStatus).to receive(:critical_patch_release?).and_return(true)
    end

    it 'returns a critical title' do
      expect(metadata.blog_title).to eq('GitLab Critical Patch Release: 9.1.3')
    end

    it 'returns a critical description' do
      expect(metadata.blog_description).to eq('Learn more about GitLab Critical Patch Release: 9.1.3 for GitLab Community Edition (CE) and Enterprise Edition (EE).')
    end

    it 'returns "Critical" for the title_header' do
      expect(metadata.title_header).to eq(ReleaseTools::PatchRelease::BlogPost::Metadata::CRITICAL_TITLE_HEADER)
    end
  end

  context 'single version patch coordinator' do
    let(:patch_coordinator) do
      instance_spy(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content, pressure: 2, single_version?: true)
    end

    it 'returns empty tag' do
      expect(metadata.blog_tags).to be_blank
    end

    it 'returns an empty canonical_path' do
      expect(metadata.canonical_path).to be_blank
    end
  end

  context 'non-single version patch coordinator' do
    around do |ex|
      Timecop.freeze(Time.utc(2019, 12, 31), &ex)
    end

    let(:patch_coordinator) do
      instance_spy(ReleaseTools::PatchRelease::Coordinator, merge_requests: patch_content, pressure: 2, single_version?: false)
    end

    it 'returns security tag' do
      expect(metadata.blog_tags).to eq('security')
    end

    it 'returns a canonical_path' do
      expect(metadata.canonical_path).to eq('/releases/2020/01/01/patch-release-gitlab-9-1-3-released/')
    end
  end
end
