# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::DockerHub::Client do
  describe ".tag" do
    it 'returns an Array of CE tags entries', vcr: { cassette_name: 'docker/tags/ce/list' } do
      tags = described_class.tags(repository: "ce")

      expect(tags.length).to eq(50)
      expect(tags.first.tag_status).to eq('active')
    end

    it 'returns a single ce tag', vcr: { cassette_name: 'docker/tags/ce/get' } do
      tag = described_class.tag('16.11.0', repository: "ce")

      expect(tag.tag_status).to eq('active')
      expect(tag.name).to eq('16.11.0-ce.0')
    end

    it 'returns a single ee tag', vcr: { cassette_name: 'docker/tags/ee/get' } do
      tag = described_class.tag('16.11.0', repository: "ee")

      expect(tag.tag_status).to eq('active')
      expect(tag.name).to eq('16.11.0-ee.0')
    end

    context "When no tag is found" do
      it 'returns error message', vcr: { cassette_name: 'docker/tags/ee/empty_tag' } do
        tag = described_class.tag('0.0.0', repository: "ee")

        expect(tag.message).to match(/httperror 404/)
      end
    end
  end

  describe ".tags" do
    it 'returns an Array of CE tags entries', vcr: { cassette_name: 'docker/tags/ce/list' } do
      tags = described_class.tags(repository: "ce")

      expect(tags.length).to eq(50)
      expect(tags.first.tag_status).to eq('active')
    end

    it 'returns an Array of EE tags entries', vcr: { cassette_name: 'docker/tags/ee/list' } do
      tags = described_class.tags(repository: "ee")

      expect(tags.length).to eq(50)
      expect(tags.first.tag_status).to eq('active')
    end

    context "when no tags found" do
      it 'returns an empty Array of EE tags if no entries found', vcr: { cassette_name: 'docker/tags/ee/empty_list' } do
        tags = described_class.tags(repository: "ee")

        expect(tags.length).to eq(0)
        expect(tags).to eq([])
      end

      it 'returns an empty Array of CE tags if no entries found', vcr: { cassette_name: 'docker/tags/ce/empty_list' } do
        tags = described_class.tags(repository: "ce")

        expect(tags.length).to eq(0)
        expect(tags).to eq([])
      end
    end
  end
end
