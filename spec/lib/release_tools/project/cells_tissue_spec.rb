# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::CellsTissue do
  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-com/delivery/cells-tissue' }
  end

  describe '.ops_path' do
    it { expect(described_class.ops_path).to eq 'gitlab-com/delivery/cells-tissue' }
  end

  describe '.ops_group' do
    it { expect(described_class.ops_group).to eq 'gitlab-com/delivery' }
  end

  describe '.default_branch' do
    it { expect(described_class.default_branch).to eq 'main' }
  end

  describe '.group' do
    it { expect { described_class.group }.to raise_error RuntimeError, "Invalid remote for gitlab-com/delivery/cells-tissue: canonical" }
  end

  describe '.dev_path' do
    it { expect { described_class.dev_path }.to raise_error RuntimeError, 'Invalid remote for gitlab-com/delivery/cells-tissue: dev' }
  end

  describe '.dev_group' do
    it { expect { described_class.dev_group }.to raise_error RuntimeError, 'Invalid remote for gitlab-com/delivery/cells-tissue: dev' }
  end
end
