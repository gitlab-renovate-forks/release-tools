# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::GlSecurity::SecurityCommunications::Communications do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .to_s'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-com/gl-security/security-communications/communications' }
  end

  describe '.group' do
    it { expect(described_class.group).to eq 'gitlab-com/gl-security/security-communications' }
  end
end
