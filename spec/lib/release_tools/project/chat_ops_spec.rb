# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::ChatOps do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .to_s'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-com/chatops' }
  end

  describe 'ops_path' do
    it { expect(described_class.ops_path).to eq 'gitlab-com/chatops' }
  end

  describe '.default_branch' do
    it { expect(described_class.default_branch).to eq 'master' }
  end
end
