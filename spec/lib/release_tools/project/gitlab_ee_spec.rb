# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::GitlabEe do
  it_behaves_like 'project .auto_deploy_path'
  it_behaves_like 'project .canonical_or_security_path'
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .security_group'
  it_behaves_like 'project .security_path', 'gitlab-org/security/gitlab'
  it_behaves_like 'project .to_s'
  it_behaves_like 'project IDs'

  describe '.path' do
    it { expect(described_class.path).to eq 'gitlab-org/gitlab' }
  end

  describe '.dev_path' do
    it { expect(described_class.dev_path).to eq 'gitlab/gitlab-ee' }
  end

  describe '.group' do
    it { expect(described_class.group).to eq 'gitlab-org' }
  end

  describe '.dev_group' do
    it { expect(described_class.dev_group).to eq 'gitlab' }
  end

  describe '.default_branch' do
    context 'when swith_to_main_branch is enabled' do
      before do
        enable_feature(:switch_to_main_branch)
      end

      it { expect(described_class.default_branch).to eq 'main' }
    end

    it { expect(described_class.default_branch).to eq 'master' }
  end

  describe '.metadata_project_name' do
    it { expect(described_class.metadata_project_name).to eq('gitlab-ee') }
  end

  describe '.ee_branch?' do
    it { expect(described_class).to be_ee_branch }
  end

  describe '.ee_tag?' do
    it { expect(described_class).to be_ee_tag }
  end

  describe '.tag_for' do
    it 'returns the tag for the specific version' do
      version = ReleaseTools::Version.new('15.5.4')

      expect(described_class.tag_for(version)).to eq('v15.5.4-ee')
    end
  end

  describe '.stable_branch_for' do
    it 'returns the stable branch for the specific version' do
      version = ReleaseTools::Version.new('15.5.4')

      expect(described_class.stable_branch_for(version)).to eq('15-5-stable-ee')
    end
  end
end
