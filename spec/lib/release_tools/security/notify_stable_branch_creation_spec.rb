# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::NotifyStableBranchCreation do
  let(:issue_crawler) { instance_spy(ReleaseTools::Security::IssueCrawler, security_tracking_issue: build(:issue)) }
  let(:stable_branch) { '16-4-stable' }
  let(:project) { ReleaseTools::Project::Gitaly }
  let(:usernames_in_note) { "@#{assignee1.username}, @#{assignee2.username}" }
  let(:assignee1) { build(:user) }
  let(:assignee2) { build(:user) }

  let(:issue1) do
    build(:security_implementation_issue, issue: build(:issue, assignees: [assignee1, assignee2]))
  end

  let(:note_body) do
    <<~EOF.squish
      #{usernames_in_note} The stable branch `#{stable_branch}` has been created. To be included in
      #{issue_crawler.security_tracking_issue.web_url}, please create a backport targeting the stable branch,
      following the security MR template. If this backport is not needed, please
      notify the `@gitlab-org/release/managers` and apply the ~"reduced backports" label
      to this issue. **When this issue is ready, apply the ~"security-target" label for it
      to be processed and linked to the upcoming patch release.**
    EOF
  end

  subject(:notify) { described_class.new(issue_crawler, project, stable_branch).execute }

  before do
    allow(issue_crawler)
      .to receive(:notifiable_security_issues_for)
      .and_return([issue1])

    allow(ReleaseTools::GitlabClient)
      .to receive(:issue_notes)
      .with(issue1.project_id, issue_iid: issue1.iid)
      .and_return([])
  end

  it 'posts note to issue' do
    expect(ReleaseTools::GitlabClient)
      .to receive(:create_issue_note)
      .with(issue1.project_id, { issue: issue1.issue, body: note_body })

    without_dry_run { notify }
  end

  context 'when issue has no assignees' do
    let(:issue1) { build(:security_implementation_issue) }
    let(:usernames_in_note) { "@#{issue1.author.username}" }

    it 'pings author' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:create_issue_note)
        .with(
          issue1.project_id,
          {
            issue: issue1.issue,
            body: note_body
          }
        )

      without_dry_run { notify }
    end
  end

  context 'with dry run' do
    it 'does not post note to issue' do
      expect(ReleaseTools::GitlabClient).not_to receive(:create_issue_note)

      notify
    end
  end

  context 'when issue already has backport' do
    let(:issue1) do
      build(:security_implementation_issue, merge_requests: [build(:merge_request, target_branch: stable_branch)])
    end

    it 'does not post note' do
      expect(ReleaseTools::GitlabClient).not_to receive(:create_issue_note)

      without_dry_run { notify }
    end
  end

  context 'when notification has already been posted to issue' do
    let(:issue1) do
      build(:security_implementation_issue)
    end

    before do
      allow(issue_crawler)
        .to receive(:notifiable_security_issues_for)
        .and_return([issue1])

      allow(ReleaseTools::GitlabClient)
        .to receive(:issue_notes)
        .with(issue1.project_id, issue_iid: issue1.iid)
        .and_return([
          build(
            :note,
            author: build(:user, username: 'gitlab-release-tools-bot'),
            body: "The stable branch `#{stable_branch}` has been created. Foo bar."
          )
        ])
    end

    it 'does not post note' do
      expect(ReleaseTools::GitlabClient).not_to receive(:create_issue_note)

      without_dry_run { notify }
    end
  end
end
