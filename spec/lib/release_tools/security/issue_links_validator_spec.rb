# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::IssueLinksValidator do
  let(:client) { double(:client) }
  let(:issue_crawler) { instance_spy(ReleaseTools::Security::IssueCrawler) }
  let(:upcoming_security_tracking_issue_iid) { 1 }
  let(:upcoming_security_tracking_issue) { double(project_id: 1, due_date: Date.current.to_s, iid: upcoming_security_tracking_issue_iid) }

  let(:instance) { described_class.new(client) }

  subject(:validate) do
    instance.execute
  end

  before do
    allow(ReleaseTools::Security::IssueCrawler).to receive(:new).and_return(issue_crawler)
    allow(ReleaseTools::GitlabClient).to receive(:next_security_tracking_issue).and_return(upcoming_security_tracking_issue)
    allow(instance.logger).to receive(:info)
    allow(instance.logger).to receive(:debug)
  end

  it 'does nothing if feature flag is disabled' do
    expect(ReleaseTools::GitlabClient).not_to receive(:client)
    expect(client).not_to receive(:create_issue_note)

    without_dry_run do
      validate
    end
  end

  context 'when current time is before cutoff time' do
    let(:upcoming_security_tracking_issue) { double(due_date: (Date.current + 2.days).to_s) }

    before do
      enable_feature(:unlink_late_security_issues)
    end

    it 'does nothing' do
      expect(ReleaseTools::GitlabClient).not_to receive(:client)
      expect(client).not_to receive(:create_issue_note)

      without_dry_run do
        validate
      end
    end
  end

  context 'when current time is after cutoff time' do
    let(:gitlab_client) { double }

    let(:security_issue1) do
      double(
        link_created_at: Time.current.utc.iso8601,
        project_id: 1,
        iid: 1,
        issue_link_id: 1,
        web_url: 'issues/1',
        assignees: [],
        link_type: 'relates_to',
        author: double(username: 'person1')
      )
    end

    let(:security_issue2) do
      double(
        link_created_at: (Time.current.utc - 1.hour).iso8601,
        project_id: 2,
        iid: 2,
        issue_link_id: 2,
        web_url: 'issues/2',
        link_type: 'relates_to',
        assignees: [double(username: 'person2_1'), double(username: 'person2_2')]
      )
    end

    let(:security_issue3) do
      double(
        link_created_at: (Time.current.utc - 2.days).iso8601,
        project_id: 3,
        iid: 3,
        issue_link_id: 3,
        web_url: 'issues/3',
        link_type: 'relates_to',
        assignees: [double(username: 'person3')]
      )
    end

    before do
      enable_feature(:unlink_late_security_issues)

      allow(ReleaseTools::GitlabClient).to receive(:client).and_return(gitlab_client)
      allow(gitlab_client).to receive(:delete_issue_link)

      allow(client).to receive(:create_issue_note)

      allow(issue_crawler)
        .to receive(:security_issues_for)
        .with(upcoming_security_tracking_issue_iid)
        .and_return([security_issue1, security_issue2, security_issue3])
    end

    context 'in dry run mode' do
      it 'logs messages about unlinking issues' do
        expect(instance.logger).to receive(:debug).with(
          'Checking security issues added after cutoff', a_hash_including(:cutoff, :current_time)
        )
        expect(gitlab_client).not_to receive(:delete_issue_link)

        validate
      end

      it 'logs debug level message about commenting on security tracking issue' do
        expect(instance.logger).to receive(:debug).with(
          'Adding comment about removed issues',
          a_hash_including(
            body: a_string_matching(/The following security issues have been unlinked/)
          )
        )

        expect(gitlab_client).not_to receive(:create_issue_note)

        validate
      end
    end

    context 'without dry run' do
      it 'ignores issues that block the tracking issue' do
        allow(security_issue2).to receive(:link_type).and_return('is_blocked_by')

        expect(gitlab_client).to receive(:delete_issue_link).with(1, 1, 1)
        expect(gitlab_client).not_to receive(:delete_issue_link).with(2, 2, 2)

        without_dry_run do
          validate
        end
      end

      it 'removes issues added after cutoff time' do
        expect(instance.logger).to receive(:info).with(
          'Removing late issue',
          a_hash_including(
            issue_url: security_issue1.web_url,
            issue_link_created_at: security_issue1.link_created_at
          )
        )
        expect(instance.logger).to receive(:info).with(
          'Removing late issue',
          a_hash_including(
            issue_url: security_issue2.web_url,
            issue_link_created_at: security_issue2.link_created_at
          )
        )
        expect(gitlab_client).to receive(:delete_issue_link).with(1, 1, 1)
        expect(gitlab_client).to receive(:delete_issue_link).with(2, 2, 2)

        without_dry_run do
          validate
        end
      end

      it 'adds comment about late issues' do
        freeze_time_at = Time.new(2021, 7, 28, 14, 20, 1, 'UTC')

        Timecop.freeze(freeze_time_at) do
          allow(upcoming_security_tracking_issue).to receive(:due_date).and_return((Date.current + 1.day).to_s)
          allow(security_issue1).to receive(:link_created_at).and_return(Time.current.utc.iso8601)
          allow(security_issue2).to receive(:link_created_at).and_return((Time.current.utc - 1.hour).iso8601)
          allow(security_issue3).to receive(:link_created_at).and_return((Time.current.utc - 2.days).iso8601)
        end

        late_comment_body = <<~TEMPLATE.strip
          The following security issues have been unlinked from this tracking issue
          since they were added after the cutoff time of Jul 28, 2021, 00:00 UTC.

          If it is absolutely essential that these security issues be included in this
          patch release, they can be linked as blockers to this patch release.

          See the [patch release process docs](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md#planned-security-releases)
          for more information about patch release due dates.

          1. issues/1 added on Jul 28, 2021, 14:20 UTC - @person1
          1. issues/2 added on Jul 28, 2021, 13:20 UTC - @person2_1, @person2_2

          #{ReleaseTools::Messaging::COMMENT_FOOTNOTE}
        TEMPLATE

        expect(client).to receive(:create_issue_note).with(
          1,
          upcoming_security_tracking_issue_iid,
          late_comment_body
        )

        without_dry_run do
          Timecop.freeze(freeze_time_at) do
            validate
          end
        end
      end
    end
  end

  context 'when there are non-security issues' do
    let(:gitlab_client) { stub_const('ReleaseTools::GitlabClient', spy) }

    let(:security_issue4) do
      build(:issue,
            :opened,
            project_id: 4,
            iid: 4,
            web_url: 'gitlab-org/gitlab/issues/4',
            assignees: [],
            author: build(:user, username: 'person4'),
            link_created_at: '2021-07-28T14:20:00Z',
            issue_link_id: 4)
    end

    let(:security_tracking_issue) do
      build(:issue,
            due_date: '2023-07-01',
            project_id: 1,
            iid: upcoming_security_tracking_issue_iid)
    end

    before do
      enable_feature(:unlink_non_security_issues)
      allow(ReleaseTools::GitlabClient).to receive(:client).and_return(gitlab_client)
      allow(gitlab_client).to receive(:delete_issue_link)
      allow(instance).to receive_messages(tracking_issue_links: [security_issue4], security_tracking_issue: security_tracking_issue)
    end

    context 'in dry run mode' do
      it 'logs messages about unlinking issues' do
        expect(instance.logger).to receive(:debug).with(
          'Checking linked non-security issues'
        )
        expect(gitlab_client).not_to receive(:delete_issue_link)

        validate
      end

      it 'logs debug level message about commenting on security tracking issue' do
        expect(instance.logger).to receive(:debug).with('Checking linked non-security issues')
        expect(instance.logger).to receive(:debug).with(
          'Adding comment about removed issues',
          a_hash_including(
            body: a_string_matching(/The following issues have been unlinked/)
          )
        )

        expect(gitlab_client).not_to receive(:create_issue_note)

        validate
      end
    end

    context 'without dry run' do
      it 'removes non-security issues' do
        expect(instance.logger).to receive(:info).with(
          'Removing non-security issue',
          a_hash_including(
            issue_url: security_issue4.web_url,
            issue_link_created_at: security_issue4.link_created_at
          )
        )
        expect(gitlab_client).to receive(:delete_issue_link).with(4, 4, 4)
        expect(client).to receive(:create_issue_note).with(any_args)

        without_dry_run do
          validate
        end
      end

      it 'adds comment about non-security issues' do
        non_security_comment_body = <<~TEMPLATE.strip
          The following issues have been unlinked from this tracking issue
          since they were not from the security namespace.

          We should only associate issues that are created on [GitLab Security](https://gitlab.com/gitlab-org/security/) projects.

          1. gitlab-org/gitlab/issues/4 added on Jul 28, 2021, 14:20 UTC - @person4

          #{ReleaseTools::Messaging::COMMENT_FOOTNOTE}
        TEMPLATE

        expect(client).to receive(:create_issue_note).with(
          1,
          upcoming_security_tracking_issue_iid,
          non_security_comment_body
        )

        without_dry_run do
          validate
        end
      end
    end
  end
end
