# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::PendingDeployNotificationService do
  subject(:notification_service) { described_class.new }

  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:project) { stub_const('ReleaseTools::Project::GitlabEe', spy) }
  let(:security_path) { 'gitlab-org/security/gitlab' }
  let(:merged_query) { { state: 'merged', target_branch: "master", per_page: 50 } }
  let(:deployed_query) { { state: 'merged', target_branch: "master", per_page: 50, environment: 'gprd' } }
  let(:merged_mr) { build(:merge_request, web_url: 'mr1 url') }
  let(:deployed_mr) { merged_mr }
  let(:merged_mrs) { [merged_mr] }
  let(:deployed_mrs) { [deployed_mr] }

  before do
    allow(client)
      .to receive(:merge_requests)
      .with(security_path, merged_query)
      .and_return(merged_mrs)

    allow(client)
      .to receive(:merge_requests)
      .with(security_path, deployed_query)
      .and_return(deployed_mrs)

    allow(project)
      .to receive_messages(
        security_path: security_path,
        default_branch: 'master'
      )

    allow(notifier)
      .to receive(:new)
      .and_return(notifier)
  end

  describe '#execute' do
    context 'when all the default merge requests are deployed to production' do
      it 'returns all mrs have been deployed on success status' do
        expect(notification_service.logger).to receive(:info).with("All merged MRs in #{security_path} have been deployed to Production.")

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Pending deploy',
            status: :success,
            release_type: :patch
          )

        notification_service.execute
      end
    end

    context 'when there are pending default merge requests' do
      let(:deployed_mr) { build(:merge_request, web_url: 'mr2 url') }

      it 'outputs information about pending merge issues and raises' do
        logger_output = "

❌ Some merged MRs in #{security_path} haven't been deployed to Production:

- mr1 url"

        expect(notifier)
          .to receive(:new)
          .with(
            job_type: 'Pending deploy',
            status: :failed,
            release_type: :patch
          )

        expect(notification_service.logger).to receive(:info).with(logger_output)

        expect { notification_service.execute }.to raise_error(described_class::PendingDeployError)
      end
    end

    context 'when the job is unable to complete and an error is raised' do
      before do
        allow(client)
          .to receive(:merge_requests)
          .with(security_path, merged_query)
          .and_raise(Gitlab::Error::Error)
      end

      it 'outputs manual instructions and notifies slack' do
        logger_output = "Pending deployment check failed. If this job continues to fail, the deploy statuses of the default MRs should be checked manually or through chatops command: `/chatops run auto_deploy security_status`."

        expect(notification_service.logger).to receive(:fatal).with(logger_output, error: anything)

        expect { notification_service.execute }.to raise_error(Gitlab::Error::Error)
      end
    end
  end
end
