# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::ImplementationIssueValidator do
  subject(:validator) { described_class.new(implementation_issue) }

  let(:implementation_issue) do
    ReleaseTools::Security::ImplementationIssue.new(build(:issue), [])
  end

  let(:cve_data) do
    build(
      :issue,
      title: 'Bypass some auth',
      description: File.read('spec/fixtures/cve_issue_description.md'),
      labels: ['foo', 'cve::1234-1234', 'cve']
    )
  end

  let(:cves_issue) do
    ReleaseTools::Security::CvesIssue.new(cve_data)
  end

  before do
    allow(implementation_issue)
      .to receive(:cves_issue)
      .and_return(cves_issue)
  end

  describe '#validate' do
    it 'calls the validation methods' do
      validation_methods = %i[
        validate_cves_information
        validate_cves_versions_presence
        validate_cves_affected_versions
        validate_cves_fixed_versions
      ]

      validation_methods.each do |method|
        allow(validator).to receive(method)
      end

      validator.validate

      validation_methods.each do |method|
        expect(validator).to have_received(method)
      end
    end

    context 'with no CVES issue' do
      let(:cves_issue) { nil }

      it 'skips CVES methods' do
        validation_methods = %i[
          validate_cves_versions_presence
          validate_cves_affected_versions
          validate_cves_fixed_versions
        ]

        [:validate_cves_information].concat(validation_methods).each do |method|
          allow(validator).to receive(method)
        end

        validator.validate

        validation_methods.each do |method|
          expect(validator).not_to have_received(method)
        end
      end
    end
  end

  describe '#validate_cves_information' do
    context 'with a valid CVES issue' do
      it 'returns nothing' do
        validator.validate_cves_information

        expect(validator.errors).to be_empty
      end
    end

    context 'with a CVES issue with no YAML defined' do
      let(:cve_data) { build(:issue, title: 'Bypass some auth', description: '') }

      it 'returns an error' do
        validator.validate_cves_information

        expect(validator.errors.first).to include('Missing YAML')
      end
    end

    context 'with a CVES issue with invalid YAML' do
      let(:cve_data) { build(:issue, title: 'Bypass some auth', description: "```yaml\na: something, b: other\n```") }

      it 'returns an error' do
        validator.validate_cves_information

        expect(validator.errors.first).to include('Invalid YAML')
      end

      it 'returns the invalid yaml' do
        validator.validate_cves_information

        expect(validator.errors.first).to include('Psych::SyntaxError')
      end
    end
  end

  describe '#validate_cves_versions_presence' do
    context 'when versions data is filled in' do
      it 'returns nothing' do
        validator.validate_cves_versions_presence

        expect(validator.errors).to be_empty
      end
    end

    context 'when versions data is not available' do
      it 'returns an error' do
        allow(cves_issue)
          .to receive_messages(pending_affected_versions?: true, pending_fixed_versions?: true)

        validator.validate_cves_versions_presence

        expect(validator.errors.count).to eq(2)
        expect(validator.errors.first).to include('Affected versions missing')
        expect(validator.errors.last).to include('Fixed versions missing')
      end
    end
  end

  describe '#validate_cves_affected_versions' do
    let(:affected_versions) do
      ['>=14.0, <16.5.6', '>=16.6, <16.6.4', '>=16.7, <16.8.2']
    end

    before do
      allow(cves_issue)
        .to receive(:affected_versions)
        .and_return(affected_versions)

      security_versions = ['16.8.1', '16.7.1', '16.6.1']
        .map { |version| ReleaseTools::Version.new(version) }

      allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(security_versions)
    end

    context 'when the patch release versions match the affected versions' do
      it 'returns nothing' do
        validator.validate_cves_affected_versions

        expect(validator.errors).to be_empty
      end
    end

    context 'when the patch release versions do not match the affected versions' do
      let(:affected_versions) do
        ['>=14.0, <16.5.6', '>=16.6, <16.6.4', '>=16.7, <16.7.2']
      end

      it 'returns an error' do
        validator.validate_cves_affected_versions

        expect(validator.errors.first)
          .to include("`affected_versions` don't match the patch release versions")
      end
    end

    context 'when affected versions are not defined' do
      it 'returns nothing' do
        allow(cves_issue)
          .to receive(:pending_affected_versions?)
          .and_return(true)

        validator.validate_cves_affected_versions

        expect(validator.errors).to be_empty
      end
    end
  end

  describe '#validate_cves_fixed_versions' do
    let(:fixed_versions) do
      ['16.6.3', '16.7.4', '16.8.1']
    end

    before do
      allow(cves_issue)
        .to receive(:fixed_versions)
        .and_return(fixed_versions)

      security_versions = ['16.8.1', '16.7.1', '16.6.1']
        .map { |version| ReleaseTools::Version.new(version) }

      allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(security_versions)
    end

    context 'when the patch release versions match the fixed versions' do
      it 'returns nothing' do
        validator.validate_cves_fixed_versions

        expect(validator.errors).to be_empty
      end
    end

    context 'when the patch release versions do not match the fixed versions' do
      let(:fixed_versions) do
        ['16.5.3', '16.6.4', '16.7.1']
      end

      it 'returns an error' do
        validator.validate_cves_fixed_versions

        expect(validator.errors.first)
          .to include("`fixed_versions` don't match the patch release versions")
      end
    end

    context 'when fixed versions are not defined' do
      it 'returns nothing' do
        allow(cves_issue)
          .to receive(:pending_fixed_versions?)
          .and_return(true)

        validator.validate_cves_fixed_versions

        expect(validator.errors).to be_empty
      end
    end
  end
end
