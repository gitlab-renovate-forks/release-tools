# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Prepare::NotifyJihu do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }

  let(:versions) do
    [
      ReleaseTools::Version.new('16.1.1'),
      ReleaseTools::Version.new('16.0.1'),
      ReleaseTools::Version.new('15.11.1')
    ]
  end

  let(:tracking_issue) do
    create(
      :issue,
      due_date: '2022-05-22',
      web_url: 'https://test.com/foo/bar/-/issues/1'
    )
  end

  let(:notifier) do
    stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy)
  end

  let(:jihu_issue) { create(:issue) }

  subject(:notify_jihu) { described_class.new }

  describe '#execute' do
    before do
      allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

      allow(client)
        .to receive(:security_tracking_issue)
        .and_return(tracking_issue)

      allow(notify_jihu)
        .to receive(:jihu_issue)
        .and_return(jihu_issue)
    end

    it 'post a message on a JiHu issue' do
      expect(client)
        .to receive(:create_issue_note)
        .with(
          ReleaseTools::Project::Jihu,
          { issue: jihu_issue, body: instance_of(String) }
        )

      expect(notifier)
          .to receive(:send_notification)

      without_dry_run do
        notify_jihu.execute
      end
    end

    context 'during a critical patch release' do
      before do
        allow(ReleaseTools::SharedStatus)
          .to receive(:critical_patch_release?)
          .and_return(true)
      end

      it 'posts a message referencing the release task issue' do
        expect(client)
          .to receive(:current_security_task_issue)
          .and_return(tracking_issue)

        expect(client)
          .to receive(:create_issue_note)
          .with(
            ReleaseTools::Project::Jihu,
            { issue: jihu_issue, body: instance_of(String) }
          )

        expect(notifier)
            .to receive(:send_notification)

        without_dry_run do
          notify_jihu.execute
        end
      end
    end

    context 'when something goes wrong' do
      it 'sends a slack notification and raises exception' do
        allow(client)
          .to receive(:create_issue_note)
          .and_raise(Gitlab::Error::Error)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          expect { notify_jihu.execute }
            .to raise_error(described_class::CouldNotNotifyError)
        end
      end
    end

    context 'with a dry-run' do
      it 'does nothing' do
        expect(client)
          .not_to receive(:create_issue_note)

        expect(notifier)
          .not_to receive(:send_notification)

        notify_jihu.execute
      end
    end
  end
end
