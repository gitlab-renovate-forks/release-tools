# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Prepare::IssueCreator do
  subject(:issue_creator) { described_class.new(issue: issue, issue_type: issue_type) }

  let(:issue_type) { 'appsec_task_issue' }

  let(:issue) do
    instance_double(
      ReleaseTools::Security::AppSecIssue,
      title: 'foo',
      url: 'http://test.gitlab.com/-/gitlab/issue/1/',
      create: true
    )
  end

  let(:appsec_notifier) do
    stub_const('ReleaseTools::Slack::Security::AppSecNotifier', spy)
  end

  let(:notifier) do
    stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy)
  end

  before do
    allow(issue).to receive(:exists?).and_return(false)
  end

  describe '#execute' do
    it 'creates an issue, notifies AppSec and sends slack notification' do
      expect(issue).to receive(:create)
      expect(appsec_notifier).to receive(:send_notification)
      expect(notifier).to receive(:send_notification)

      without_dry_run do
        issue_creator.execute
      end
    end

    context 'when the issue exists' do
      it 'skips issue creation' do
        allow(issue).to receive(:exists?).and_return(true)

        expect(issue).not_to receive(:create)
        expect(appsec_notifier).not_to receive(:send_notification)
        expect(notifier).to receive(:send_notification)

        without_dry_run do
          issue_creator.execute
        end
      end
    end

    context 'with the comms issue' do
      let(:issue) do
        instance_double(
          ReleaseTools::Security::CommsTaskIssue,
          title: 'foo',
          url: 'http://test.gitlab.com/-/gitlab/issue/2/',
          create: true
        )
      end

      let(:issue_type) { 'comms_security_task_issue' }

      it 'creates an issue, notifies AppSec and sends slack notification' do
        expect(issue).to receive(:create)
        expect(appsec_notifier).to receive(:send_notification)
        expect(notifier).to receive(:send_notification)

        without_dry_run do
          issue_creator.execute
        end
      end
    end

    context 'with dry-run mode' do
      let(:issue) do
        instance_double(
          ReleaseTools::Security::AppSecIssue,
          title: 'foo',
          create: true
        )
      end

      it 'skips issue creation and slack notification' do
        expect(issue).not_to receive(:create)
        expect(appsec_notifier).to receive(:send_notification)
        expect(notifier).to receive(:send_notification)

        issue_creator.execute
      end
    end

    context 'when something goes wrong' do
      it 'sends slack notification' do
        allow(issue)
          .to receive(:create)
          .and_raise(gitlab_error(:BadRequest))

        expect(notifier).to receive(:send_notification)

        without_dry_run do
          expect { issue_creator.execute }
            .to raise_error(StandardError)
        end
      end
    end
  end
end
