# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::CloseTrackingIssue do
  let(:project) { ReleaseTools::Project::GitlabEe }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:tracking_issue) { create(:issue, web_url: 'foo') }

  subject(:close_tracking_issue) { described_class.new }

  describe '#execute' do
    before do
      allow(client)
        .to receive(:next_security_tracking_issue)
        .and_return(tracking_issue)
    end

    context 'when the tracking issue exists' do
      it 'closes outdated security tracking issues' do
        expect(client)
          .to receive(:close_issue)
          .with(project, tracking_issue)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run { close_tracking_issue.execute }
      end

      it 'leaves issues open when performing a dry-run' do
        expect(client).not_to receive(:close_issue)

        expect(notifier)
          .to receive(:send_notification)

        close_tracking_issue.execute
      end

      context 'with a security pipeline' do
        it 'sends a slack notification' do
          expect(notifier)
            .to receive(:send_notification)

          ClimateControl.modify(SECURITY_RELEASE_PIPELINE: 'true') do
            without_dry_run { close_tracking_issue.execute }
          end
        end
      end
    end

    context 'when the tracking issue is not found' do
      let(:tracking_issue) { nil }

      it 'raises an error' do
        expect(client)
          .not_to receive(:close_issue)

        expect(notifier)
          .to receive(:send_notification)

        expect { close_tracking_issue.execute }
          .to raise_error(described_class::CouldNotCompleteError)
      end

      context 'with a security pipeline' do
        it 'sends a slack notification' do
          expect(notifier)
            .to receive(:send_notification)

          ClimateControl.modify(SECURITY_RELEASE_PIPELINE: 'true') do
            without_dry_run do
              expect { close_tracking_issue.execute }
                .to raise_error(described_class::CouldNotCompleteError)
            end
          end
        end
      end
    end
  end
end
