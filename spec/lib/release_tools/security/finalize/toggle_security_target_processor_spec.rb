# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor do
  let(:client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:action) { :enable }

  describe '#execute' do
    subject(:toggle) { described_class.new.execute(action: action) }

    context 'invalid action' do
      let(:action) { :invalid }

      it 'raises an error' do
        expect(notifier).to receive(:new)

        expect { toggle }.to raise_error(ArgumentError)
      end
    end

    context 'valid action' do
      context 'dry run' do
        it 'does not toggle the pipeline' do
          expect(client).not_to receive(:edit_pipeline_schedule)

          toggle
        end
      end

      it 'toggles the pipeline schedule' do
        expect(notifier).to receive(:new).with(
          job_type: "#{action.capitalize} security-target issue processor",
          status: :success,
          release_type: :patch
        )

        expect(client).to receive(:pipeline_schedule_take_ownership).with(
          ReleaseTools::Project::ReleaseTools, described_class::PIPELINE_SCHEDULE_ID
        )

        expect(client).to receive(:edit_pipeline_schedule).with(
          ReleaseTools::Project::ReleaseTools.ops_path,
          described_class::PIPELINE_SCHEDULE_ID,
          active: true,
          cron: described_class::CRON
        )

        without_dry_run do
          toggle
        end
      end
    end
  end
end
