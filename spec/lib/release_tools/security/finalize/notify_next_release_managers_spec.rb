# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Finalize::NotifyNextReleaseManagers do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:release_managers) { create_list(:user, 2) }

  let(:tracking_issue) do
    create(
      :issue,
      due_date: '2022-05-22',
      web_url: 'https://test.com/foo/bar/-/issues/1',
      assignees: release_managers
    )
  end

  let(:notifier) do
    stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy)
  end

  subject(:notify_next_release_managers) { described_class.new }

  describe '#execute' do
    let(:notified_users) do
      "@#{release_managers.first.username}, @#{release_managers.last.username}"
    end

    before do
      allow(client)
        .to receive(:next_security_tracking_issue)
        .and_return(tracking_issue)
    end

    it 'posts a message on the tracking issue and pings upcoming release managers' do
      expect(client)
        .to receive(:create_issue_note)
        .with(
          ReleaseTools::Project::GitlabEe,
          {
            issue: tracking_issue,
            body: a_string_matching(/#{notified_users}. The next patch release has been automatically scheduled/)
          }
        )

      expect(notifier).to receive(:send_notification)

      without_dry_run do
        notify_next_release_managers.execute
      end
    end

    context 'when something goes wrong' do
      it 'sends a slack notification and raises exception' do
        allow(client)
          .to receive(:create_issue_note)
          .and_raise(Gitlab::Error::Error)

        expect(notifier)
          .to receive(:send_notification)

        without_dry_run do
          expect { notify_next_release_managers.execute }
            .to raise_error(described_class::CouldNotNotifyError)
        end
      end
    end

    context 'with a dry-run' do
      it 'does nothing' do
        expect(client)
          .not_to receive(:create_issue_note)

        expect(notifier)
          .not_to receive(:send_notification)

        notify_next_release_managers.execute
      end
    end
  end
end
