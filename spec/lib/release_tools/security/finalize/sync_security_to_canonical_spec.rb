# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Security::Finalize::SyncSecurityToCanonical do
  subject(:sync) do
    described_class.new
  end

  let(:canonical_security) { instance_double(ReleaseTools::PipelineSchedule) }
  let(:notifier) { instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: true) }
  let(:client) { ReleaseTools::GitlabOpsClient }

  before do
    allow(ReleaseTools::PipelineSchedule).to receive(:new).and_return(canonical_security)
    allow(canonical_security).to receive_messages(description: 'Canonical to Security', id: 1, activate: anything, take_ownership: anything)
    allow(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).and_return(notifier)
  end

  describe '#execute' do
    it 'performs the sync process' do
      expect(canonical_security).to receive(:deactivate)
      expect(client).to receive(:create_pipeline).with(
        ReleaseTools::Project::MergeTrain,
        {
          MERGE_SECURITY: '1',
          SOURCE_PROJECT: 'gitlab-org/security/gitlab',
          SOURCE_BRANCH: 'master',
          TARGET_PROJECT: 'gitlab-org/gitlab',
          TARGET_BRANCH: 'master'
        }
      )
      expect(sync).to receive(:wait_for_merge_train_pipeline_to_complete).and_return(nil)
      expect(sync).to receive(:send_slack_notification).with(:success)

      without_dry_run do
        sync.execute
      end
    end

    context 'when in dry run mode' do
      it 'does not perform any actions' do
        expect(client).not_to receive(:new)

        sync.execute
      end
    end

    context 'when the sync pipeline fails' do
      before do
        allow(sync).to receive(:wait_for_merge_train_pipeline_to_complete).and_raise(StandardError)
      end

      it 'retries 5 times and then raise an error' do
        expect(canonical_security).to receive(:deactivate)
        expect(client).to receive(:create_pipeline).exactly(5).times
        expect(sync).to receive(:send_slack_notification).with(:failed)
        expect(sync).to receive(:manual_action_guide)
        without_dry_run do
          expect { sync.execute }.to raise_error(StandardError)
        end
      end
    end
  end

  describe '#wait_for_merge_train_pipeline_to_complete' do
    let(:pipeline) do
      create(:pipeline, :running)
    end

    before do
      allow(client).to receive(:pipeline).and_return(pipeline)
    end

    context 'when the pipeline is running' do
      it 'raises a PipelineInProgressError' do
        expect { sync.send(:wait_for_merge_train_pipeline_to_complete, pipeline) }.to raise_error(described_class::PipelineInProgressError)
      end
    end

    context 'when the pipeline is successful' do
      let(:pipeline) do
        create(:pipeline, :success)
      end

      it 'does not raise an error' do
        expect { sync.send(:wait_for_merge_train_pipeline_to_complete, pipeline) }.not_to raise_error
      end
    end

    context 'when the pipeline fails' do
      let(:pipeline) do
        create(:pipeline, :failed)
      end

      it 'raises a PipelineError' do
        expect { sync.send(:wait_for_merge_train_pipeline_to_complete, pipeline) }.to raise_error(described_class::PipelineError)
      end
    end
  end
end
