# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::ReleaseEnvironment::QaNotification do
  subject(:notifier) do
    described_class.new(
      release_environment_version: release_environment_version,
      environment_name: environment,
      job: job
    )
  end

  let(:release_environment_version) { '16-10-stable-9dce3c3d' }
  let(:environment) { 'my_environment' }
  let(:job) { create(:job, :running) }
  let(:status) { 'started' }
  let(:local_time) { Time.utc(2024, 9, 14, 15, 55, 0) }

  let(:context_elements) do
    [
      { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' }
    ]
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
  end

  describe '#execute' do
    context 'when the QA fails' do
      let(:status) { 'failed' }

      let(:context_elements) do
        [
          { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' }
        ]
      end

      it 'sends a slack message' do
        block_content = ":ci_failing: QA Release Environment *my_environment* <#{job.web_url}|#{status}> `#{release_environment_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: 'QA Release Environment my_environment: failed 16-10-stable-9dce3c3d',
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end
  end
end
