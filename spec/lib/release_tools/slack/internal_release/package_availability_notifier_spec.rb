# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Slack::InternalRelease::PackageAvailabilityNotifier do
  subject(:notification) { described_class.new(versions) }

  let(:versions) do
    [
      ReleaseTools::Version.new('15.2.1-internal1'),
      ReleaseTools::Version.new('15.1.2-internal1')
    ]
  end

  before do
    allow(ReleaseTools::Slack::ChatopsNotification)
      .to receive(:fire_hook)
      .and_return({})
  end

  describe '#execute' do
    it 'sends a Slack notification with the correct message' do
      expected_message = <<~BODY
        :white_check_mark: :mega: <!subteam^#{ReleaseTools::Slack::RELEASE_MANAGERS}> The internal packages for 15.2 and 15.1 are available on the pre-release channel.
        Please notify the SIRT channel and coordinate with the Dedicated team.
      BODY

      expect(ReleaseTools::Slack::ChatopsNotification)
        .to receive(:fire_hook)
        .with(
          text: "The internal packages are now ready.",
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: expected_message
              }
            }
          ]
        )

      notification.execute
    end
  end
end
