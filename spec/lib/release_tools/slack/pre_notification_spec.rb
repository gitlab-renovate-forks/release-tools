# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::PreNotification do
  subject(:notifier) do
    described_class.new(
      deploy_version: deploy_version,
      pipeline: pipeline
    )
  end

  let(:deploy_version) { '16.9.202402091206-d530de1fcd9.d71cbbdb4bc' }
  let(:pipeline) { create(:pipeline, :running) }
  let(:local_time) { Time.utc(2024, 9, 14, 15, 55, 0) }

  let(:context_elements) do
    [
      { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' }
    ]
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
  end

  describe '#execute' do
    context 'when the pipeline starts' do
      it 'sends a slack message' do
        block_content = ":pretzel: :ci_running: *pre* <#{pipeline.web_url}|started> `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: 'Pre environment started 16.9.202402091206-d530de1fcd9.d71cbbdb4bc',
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end

    context 'when the pipeline succeeds' do
      let(:pipeline) { create(:pipeline, :success, created_at: '2024-09-14T15:15:23.923Z') }

      let(:context_elements) do
        [
          { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' },
          { type: 'mrkdwn', text: ":timer_clock: 39 minutes" }
        ]
      end

      it 'sends a slack message' do
        block_content = ":pretzel: :ci_passing: *pre* <#{pipeline.web_url}|finished> `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: 'Pre environment finished 16.9.202402091206-d530de1fcd9.d71cbbdb4bc',
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end

    context 'when the pipeline fails' do
      let(:pipeline) { create(:pipeline, :failed, created_at: '2024-09-14T15:15:23.923Z') }

      let(:context_elements) do
        [
          { text: ':clock1: 2024-09-14 15:55 UTC', type: 'mrkdwn' },
          { type: 'mrkdwn', text: ":timer_clock: 39 minutes" }
        ]
      end

      it 'sends a slack message' do
        block_content = ":pretzel: :ci_failing: *pre* <#{pipeline.web_url}|failed> `#{deploy_version}`"

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::ANNOUNCEMENTS,
              message: 'Pre environment failed 16.9.202402091206-d530de1fcd9.d71cbbdb4bc',
              blocks: slack_mrkdwn_block(text: block_content, context_elements: context_elements)
            }
          )

        Timecop.freeze(local_time) do
          notifier.execute
        end
      end
    end
  end
end
