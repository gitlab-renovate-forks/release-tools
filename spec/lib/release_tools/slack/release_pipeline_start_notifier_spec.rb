# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::ReleasePipelineStartNotifier do
  let(:user_name) { 'foo' }
  let(:pipeline_url) { 'https://example.gitlab.com/pipeline/1234' }
  let(:local_time) { Time.utc(2023, 5, 14, 15, 55, 0) }
  let(:stage) { :start }
  let(:release_type) { :patch }

  let(:context_elements) do
    [
      { text: ':clock1: 2023-05-14 15:55 UTC', type: 'mrkdwn' }
    ]
  end

  subject(:pipeline_notifier) { described_class.new(stage: stage, release_type: release_type) }

  around do |ex|
    ClimateControl.modify(CI_PIPELINE_URL: pipeline_url, GITLAB_USER_LOGIN: user_name) do
      ex.run
    end
  end

  def expect_message(message)
    full_content = ":security-tanuki: #{message}"

    expect(ReleaseTools::Slack::Message)
      .to receive(:post)
      .with(
        {
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          message: message,
          blocks: slack_mrkdwn_block(
            text: full_content,
            context_elements: context_elements
          )
        }
      )
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
      .and_return({ 'ok' => true, 'channel' => 'channel' })
  end

  context 'with the start stage' do
    it 'posts a message' do
      message = "Release manager #{user_name} has started the #{release_type} release, initial steps will now be run in #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with a finalize stage' do
    let(:stage) { :finalize }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the final steps for the #{release_type} release, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the release_preparation stage' do
    let(:stage) { :release_preparation }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the release preparation steps, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the backport_merge stage' do
    let(:stage) { :backport_merge }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the backport merging steps for stable branches security MRs, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the early_merge stage' do
    let(:stage) { :early_merge }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the early merging steps for default branch security MRs, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the tag stage' do
    let(:stage) { :tag }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the tagging steps for the #{release_type} release, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the tag day stage' do
    let(:stage) { :tag_day }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the tag day steps for the #{release_type} release, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the publish stage' do
    let(:stage) { :publish }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the publishing steps of a #{release_type} release, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the release_day stage' do
    let(:stage) { :release_day }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the release day steps for the #{release_type} release, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the verify stage' do
    let(:stage) { :verify }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the verification steps for the #{release_type} release, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the disable issue processor schedule stage' do
    let(:stage) { :disable_issue_processor_schedule }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the disable issue processor schedule steps, these jobs will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end

  context 'with the release stage' do
    let(:stage) { :release }

    it 'posts a message' do
      message = "Release manager #{user_name} has started the #{release_type} release package creation, this will be executed as part of #{pipeline_url}."

      expect_message(message)

      Timecop.freeze(local_time) do
        pipeline_notifier.execute
      end
    end
  end
end
