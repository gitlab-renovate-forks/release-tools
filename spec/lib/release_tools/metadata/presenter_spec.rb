# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metadata::Presenter do
  subject(:diffs) { described_class.new(comparison, link_style: link_style).component_diffs }

  let(:comparison) do
    ReleaseTools::Metadata::Comparison.new(
      target: instance_spy(ReleaseTools::ProductVersion).as_null_object,
      source: instance_spy(ReleaseTools::ProductVersion).as_null_object
    )
  end

  before do
    stub_const(
      "ReleaseTools::Metadata::Comparison::PROJECTS",
      [ReleaseTools::Project::OmnibusGitlab, ReleaseTools::Project::GitlabEe]
    )

    allow(comparison)
      .to receive(:compare)
      .with(ReleaseTools::Project::OmnibusGitlab)
      .and_return(build(:comparison, web_url: 'gitlab.com/omnibus', commits: Array.new(5)))

    allow(comparison)
      .to receive(:source_sha)
      .with(ReleaseTools::Project::OmnibusGitlab)
      .and_return('omnibus_source_sha')

    allow(comparison)
      .to receive(:target_sha)
      .with(ReleaseTools::Project::OmnibusGitlab)
      .and_return('omnibus_target_sha')

    allow(comparison)
      .to receive(:compare)
      .with(ReleaseTools::Project::GitlabEe)
      .and_return(build(:comparison, web_url: 'gitlab.com/gitlab-ee', commits: Array.new(3)))

    allow(comparison)
      .to receive(:source_sha)
      .with(ReleaseTools::Project::GitlabEe)
      .and_return('gitlab_ee_source_sha')

    allow(comparison)
      .to receive(:target_sha)
      .with(ReleaseTools::Project::GitlabEe)
      .and_return('gitlab_ee_target_sha')
  end

  context 'slack link_style' do
    let(:link_style) { :slack }

    it 'returns links in the slack style' do
      expect(diffs)
        .to eq([
          "omnibus-gitlab (5) - <gitlab.com/omnibus|omnibus_tar...omnibus_sou>",
          "gitlab (3) - <gitlab.com/gitlab-ee|gitlab_ee_t...gitlab_ee_s>"
        ])
    end
  end

  context 'markdown link_style' do
    let(:link_style) { :markdown }

    it 'returns links in the markdown style' do
      expect(diffs)
        .to eq([
          "omnibus-gitlab (5) - [omnibus_tar...omnibus_sou](gitlab.com/omnibus)",
          "gitlab (3) - [gitlab_ee_t...gitlab_ee_s](gitlab.com/gitlab-ee)"
        ])
    end
  end
end
