# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::PipelineSchedule do
  subject(:schedule) do
    described_class.new(client: client, id: schedule_id, project: project)
  end

  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:project) { ReleaseTools::Project::GitlabCe }
  let(:data) { create(:pipeline_schedule) }
  let(:schedule_id) { data.id }

  before do
    allow(client)
      .to receive(:pipeline_schedule)
      .and_return(data)
  end

  describe '#initialize' do
    it 'sets the id and project attributes' do
      expect(schedule.id).to eq(schedule_id)
      expect(schedule.project).to eq(project)
      expect(schedule.data).to eq(data)
    end

    it 'creates reader methods' do
      expect(schedule).to respond_to(:description)
      expect(schedule.description).to eq('pipeline-schedule')
    end
  end

  describe '#activate' do
    let(:data) { create(:pipeline_schedule, active: false) }
    let(:modifed_data) { create(:pipeline_schedule, active: true) }

    it 'calls update_attribute with :active and the given value' do
      expect(schedule).to receive(:update_attribute).with(:active, true).and_call_original
      expect(client).to receive(:edit_pipeline_schedule).with(project, schedule_id, { active: true })
        .and_return(modifed_data)
      expect { schedule.activate }.to change(schedule, :active).from(false).to(true)
    end
  end

  describe '#deactivate' do
    let(:modifed_data) { create(:pipeline_schedule, active: false) }

    it 'calls update_attribute with :active and the given value' do
      expect(schedule).to receive(:update_attribute).with(:active, false).and_call_original
      expect(client).to receive(:edit_pipeline_schedule).with(project, schedule_id, { active: false })
        .and_return(modifed_data)
      expect { schedule.deactivate }.to change(schedule, :active).from(true).to(false)
    end
  end

  describe '#run' do
    it 'runs the pipeline schedule' do
      expect(client).to receive(:run_pipeline_schedule).with(project, schedule_id)
      schedule.run
    end
  end

  describe '#take_ownership' do
    it 'takes ownership of the pipeline schedule' do
      expect(client).to receive(:pipeline_schedule_take_ownership).with(project, schedule_id)
      schedule.take_ownership
    end
  end

  describe '#data' do
    it 'initializes the data and returns the data' do
      expect(schedule).to receive(:update_data).and_call_original
      expect(schedule.data).to eq(data)
    end

    it 'does not update the data if already initialized' do
      expect(schedule).to receive(:update_data).once.and_call_original
      expect(schedule.data).to eq(data)
      expect(schedule).not_to receive(:update_data)
      expect(schedule.data).to eq(data)
    end
  end

  describe '#last_pipeline' do
    it 'updates the data and returns the last pipeline' do
      expect(schedule).to receive(:update_data).and_call_original
      expect(schedule.last_pipeline).to eq(data.last_pipeline)
    end
  end

  describe '#pipeline' do
    let(:pipeline_id) { 123 }

    it 'calls the client to get the pipeline' do
      expect(client).to receive(:pipeline).with(project, pipeline_id)
      schedule.pipeline(pipeline_id)
    end
  end
end
