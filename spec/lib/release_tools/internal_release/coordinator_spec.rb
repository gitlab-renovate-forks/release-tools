# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::Coordinator do
  subject(:coordinator) { described_class.new }

  let(:next_patch_versions) { [ReleaseTools::Version.new('16.2.1'), ReleaseTools::Version.new('16.1.3'), ReleaseTools::Version.new('16.0.5')] }

  before do
    allow(ReleaseTools::Versions)
      .to receive(:next_versions)
      .and_return(next_patch_versions)
  end

  describe '#initialize' do
    it 'returns the last two supported minor versions' do
      expect(coordinator.versions).to eq(['16.1.2-internal0', '16.0.4-internal0'])
    end

    context 'when there have been internal releases in this patch cycle' do
      subject(:coordinator) { described_class.new(1) }

      let(:internal_versions) { ['16.1.2-internal1', '16.0.4-internal1'] }

      it 'returns the last two supported minor versions with the next internal number' do
        expect(coordinator.versions).to eq(internal_versions)
      end
    end
  end

  describe '#supported_minor_versions' do
    it 'returns the minor versions of the next internal release' do
      expect(coordinator.supported_minor_versions).to eq(['16.1', '16.0'])
    end
  end

  describe '#versions' do
    it 'returns an array of versions objects' do
      expect(coordinator.versions).to all(be_instance_of(ReleaseTools::Version))
    end
  end
end
