# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::Metadata do
  subject(:metadata) { described_class.new(version: internal_version) }

  let(:commit) { create(:commit, id: "123") }
  let(:gitlab_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:internal_version) { '16.1.0-internal0' }
  let(:release_metadata) { ReleaseTools::ReleaseMetadata.new }
  let(:stable_branch) { '16-1-stable' }
  let(:stable_branch_gitlab) { '16-1-stable-ee' }
  let(:uploader) { stub_const('ReleaseTools::ReleaseMetadataUploader', spy) }

  describe '#record' do
    before do
      allow(gitlab_client).to receive(:commit).and_return(commit)
      allow(ReleaseTools::ReleaseMetadata).to receive(:new).and_return(release_metadata)
      allow(ReleaseTools::ReleaseMetadataUploader).to receive(:new).and_return(uploader)
      allow(uploader).to receive(:upload)
    end

    context 'when recording metadata about an internal version' do
      it 'gets the latest commit on the stable branches for every project under managed versioning' do
        expect(gitlab_client)
          .to receive(:commit)
          .with("gitlab-org/security/charts/components/images", ref: stable_branch)

        expect(gitlab_client)
          .to receive(:commit)
          .with("gitlab-org/security/gitaly", ref: stable_branch)

        expect(gitlab_client)
          .to receive(:commit)
          .with("gitlab-org/security/gitlab", ref: stable_branch_gitlab)

        expect(gitlab_client)
          .to receive(:commit)
          .with("gitlab-org/security/gitlab-pages", ref: stable_branch)

        expect(gitlab_client)
          .to receive(:commit)
          .with("gitlab-org/security/omnibus-gitlab", ref: stable_branch)

        metadata.record
      end

      it 'adds the release metadata' do
        expect(release_metadata)
          .to receive(:add_release)
          .with(
            {
              name: 'gitlab-ee',
              version: '16.1.0-internal0',
              sha: '123',
              ref: '123',
              tag: false
            }
          )

        expect(release_metadata)
          .to receive(:add_release)
          .with(
            {
              name: 'cng-ee',
              version: '16.1.0-internal0',
              sha: '123',
              ref: '123',
              tag: false
            }
          )

        expect(release_metadata)
          .to receive(:add_release)
          .with(
            {
              name: 'gitaly',
              version: '16.1.0-internal0',
              sha: '123',
              ref: '123',
              tag: false
            }
          )

        expect(release_metadata)
          .to receive(:add_release)
          .with(
            {
              name: 'gitlab-pages',
              version: '16.1.0-internal0',
              sha: '123',
              ref: '123',
              tag: false
            }
          )

        expect(release_metadata)
          .to receive(:add_release)
          .with(
            {
              name: 'omnibus-gitlab-ee',
              version: '16.1.0-internal0',
              sha: '123',
              ref: '123',
              tag: false
            }
          )

        without_dry_run do
          metadata.record
        end
      end

      it 'uploads the release metadata' do
        expect(uploader)
          .to receive(:upload)
          .with(
            "16.1.0-internal0",
            release_metadata,
            auto_deploy: false
          )

        without_dry_run do
          metadata.record
        end
      end

      context 'when it is a dry run' do
        it 'does not upload the release metadata' do
          expect(uploader)
            .not_to receive(:upload)

          metadata.record
        end
      end
    end
  end
end
