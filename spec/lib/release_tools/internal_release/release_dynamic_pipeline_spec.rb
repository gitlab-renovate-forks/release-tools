# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::ReleaseDynamicPipeline do
  subject(:release_jobs) { described_class.new(versions) }

  let(:versions) { ['1.0.1', '1.1.1'] }

  describe '#generate' do
    it 'generates YAML output' do
      expected_yaml = {
        "stages" => versions.map { |version| "internal_release_release:build_package:#{version}" },
        "internal_release_release_build_package:1.0.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "internal_release_release:build_package:1.0.1",
          "script" => [
            "source scripts/setup_ssh.sh",
            "source scripts/setup_git.sh",
            "bundle exec rake internal:release:build_package[1.0.1]"
          ],
          "extends" => [
            ".with-bundle",
            ".common-ci-tokens"
          ],
          "needs" => [],
          "timeout" => "2h"
        },
        "internal_release_release_build_package:1.1.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "internal_release_release:build_package:1.1.1",
          "script" => [
            "source scripts/setup_ssh.sh",
            "source scripts/setup_git.sh",
            "bundle exec rake internal:release:build_package[1.1.1]"
          ],
          "extends" => [
            ".with-bundle",
            ".common-ci-tokens"
          ],
          "needs" => [],
          "timeout" => "2h"

        }
      }

      expect(release_jobs.generate).to eq(expected_yaml.to_yaml)
    end
  end
end
