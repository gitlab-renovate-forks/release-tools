# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::Prepare::ComponentBranchVerifier do
  subject(:component_branch_verifier) { described_class.new(internal_versions) }

  let(:internal_versions) do
    [
      ReleaseTools::Version.new('15.2.1-internal1'),
      ReleaseTools::Version.new('15.1.2-internal1')
    ]
  end

  describe '#execute' do
    it 'calls the BranchStatusService class with the correct parameters' do
      expect(ReleaseTools::Services::BranchesStatusService)
        .to receive(:new)
        .with(release_type: :internal, versions: internal_versions)
        .and_return(
          instance_double(ReleaseTools::Services::BranchesStatusService, execute: 'foo')
        )

      component_branch_verifier.execute
    end
  end
end
