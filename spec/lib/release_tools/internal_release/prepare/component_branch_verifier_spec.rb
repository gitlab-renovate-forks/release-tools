# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::Prepare::ComponentBranchVerifier do
  describe '#execute' do
    subject(:component_branch_verifier) { described_class.new(internal_number) }

    let(:internal_number) { 1 }
    let(:internal_versions) do
      [
        ReleaseTools::Version.new('15.2.1-internal1'),
        ReleaseTools::Version.new('15.1.2-internal1')
      ]
    end

    let(:coordinator) { instance_double(ReleaseTools::InternalRelease::Coordinator) }

    before do
      allow(ReleaseTools::InternalRelease::Coordinator)
        .to receive(:new).with(internal_number).and_return(coordinator)

      allow(coordinator).to receive(:versions).and_return(internal_versions)
    end

    it 'calls the BranchStatusService class with correct parameters' do
      expect(ReleaseTools::Services::BranchesStatusService)
        .to receive(:new)
        .with(release_type: :internal, versions: ["15.2.1-internal1", "15.1.2-internal1"])
        .and_return(instance_double(ReleaseTools::Services::BranchesStatusService, execute: 'foo'))

      component_branch_verifier.execute
    end
  end
end
