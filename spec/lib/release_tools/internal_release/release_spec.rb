# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::InternalRelease::Release do
  subject(:release) { described_class.new(version: version) }

  let(:version) { ReleaseTools::Version.new('16.1') }
  let(:dev_client) { stub_const('ReleaseTools::GitlabDevClient', spy) }
  let(:gitlab_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:metadata) { stub_const('ReleaseTools::InternalRelease::Metadata', spy) }
  let(:commit) { create(:commit, last_pipeline: create(:pipeline)) }

  let(:sync) do
    instance_double(ReleaseTools::InternalRelease::SyncToDevService, execute: true)
  end

  let(:updater) do
    instance_double(ReleaseTools::InternalRelease::UpdateVersionFile, execute: commit)
  end

  let(:wait_dev_service) do
    instance_double(ReleaseTools::InternalRelease::WaitDevBuildAssetsJobService, execute: true)
  end

  let(:notifier) do
    instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: true)
  end

  let(:omnibus_pipeline_variables) do
    {
      ee: 'true',
      GITLAB_ASSETS_TAG: '16-1-stable-ee',
      GITLAB_VERSION: '16-1-stable-ee',
      INTERNAL_RELEASE: 'true',
      INTERNAL_RELEASE_ITERATION: '0'
    }
  end

  let(:cng_ee_pipeline_variables) do
    {
      GITLAB_ASSETS_TAG: '16-1-stable-ee',
      GITLAB_REF_SLUG: '16.1.0-internal0',
      GITLAB_VERSION: '16-1-stable-ee',
      INTERNAL_RELEASE_VERSION: '16.1.0-internal0',
      INTERNAL_RELEASE: 'true'
    }
  end

  let(:cng_ubi_pipeline_variables) do
    {
      UBI_PIPELINE: 'true',
      GITLAB_ASSETS_TAG: '16-1-stable-ee',
      GITLAB_REF_SLUG: '16.1.0-internal0',
      GITLAB_VERSION: '16-1-stable-ee',
      INTERNAL_RELEASE_VERSION: '16.1.0-internal0',
      INTERNAL_RELEASE: 'true'
    }
  end

  let(:cng_fips_pipeline_variables) do
    {
      FIPS_PIPELINE: 'true',
      GITLAB_ASSETS_TAG: '16-1-stable-ee',
      GITLAB_REF_SLUG: '16.1.0-internal0',
      GITLAB_VERSION: '16-1-stable-ee',
      INTERNAL_RELEASE_VERSION: '16.1.0-internal0',
      INTERNAL_RELEASE: 'true'
    }
  end

  before do
    allow(ReleaseTools::InternalRelease::SyncToDevService)
      .to receive(:new)
      .and_return(sync)

    allow(ReleaseTools::InternalRelease::UpdateVersionFile)
      .to receive(:new)
      .and_return(updater)

    allow(gitlab_client)
      .to receive(:create_commit)
      .and_return(commit)

    allow(ReleaseTools::InternalRelease::WaitDevBuildAssetsJobService)
      .to receive(:new)
      .and_return(wait_dev_service)

    allow(dev_client)
      .to receive(:create_pipeline)
      .and_return(create(:pipeline, web_url: 'https://dev.gitlab.example.com/merge_requests/123'))

    allow(ReleaseTools::InternalRelease::Metadata)
      .to receive(:new).and_return(metadata)

    allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .and_return(notifier)
  end

  describe '#execute' do
    context 'when this is the first internal release in this patch cycle' do
      it 'triggers a dev sync' do
        expect(sync).to receive(:execute)

        without_dry_run do
          release.execute
        end
      end

      it 'triggers the commit updater service' do
        expect(updater).to receive(:execute)

        without_dry_run do
          release.execute
        end
      end

      it 'triggers the wait QA service' do
        expect(wait_dev_service).to receive(:execute)

        without_dry_run do
          release.execute
        end
      end

      it 'triggers a pipeline on Dev Omnibus' do
        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/omnibus-gitlab',
            omnibus_pipeline_variables,
            branch: '16-1-stable'
          )

        without_dry_run do
          release.execute
        end
      end

      it 'triggers EE, UBI, and FIPS pipelines on Dev CNG/Images' do
        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/charts/components/images',
            cng_ee_pipeline_variables,
            branch: '16-1-stable'
          )

        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/charts/components/images',
            cng_ubi_pipeline_variables,
            branch: '16-1-stable'
          )

        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/charts/components/images',
            cng_fips_pipeline_variables,
            branch: '16-1-stable'
          )

        without_dry_run do
          release.execute
        end
      end

      it 'records the release metadata' do
        expect(ReleaseTools::InternalRelease::Metadata)
          .to receive(:new)
          .with(version: '16.1.0-internal0')

        expect(metadata)
          .to receive(:record)

        without_dry_run do
          release.execute
        end
      end
    end

    context 'when there have been internal releases in this patch cycle' do
      subject(:release) { described_class.new(version: version, internal_number: 1) }

      let(:version) { ReleaseTools::Version.new('16.1.2') }

      let(:omnibus_pipeline_variables) do
        {
          ee: 'true',
          GITLAB_ASSETS_TAG: '16-1-stable-ee',
          GITLAB_VERSION: '16-1-stable-ee',
          INTERNAL_RELEASE: 'true',
          INTERNAL_RELEASE_ITERATION: '1'
        }
      end

      let(:cng_ee_pipeline_variables) do
        {
          GITLAB_ASSETS_TAG: '16-1-stable-ee',
          GITLAB_REF_SLUG: '16.1.2-internal1',
          GITLAB_VERSION: '16-1-stable-ee',
          INTERNAL_RELEASE_VERSION: '16.1.2-internal1',
          INTERNAL_RELEASE: 'true'
        }
      end

      let(:cng_ubi_pipeline_variables) do
        {
          UBI_PIPELINE: 'true',
          GITLAB_ASSETS_TAG: '16-1-stable-ee',
          GITLAB_REF_SLUG: '16.1.2-internal1',
          GITLAB_VERSION: '16-1-stable-ee',
          INTERNAL_RELEASE_VERSION: '16.1.2-internal1',
          INTERNAL_RELEASE: 'true'
        }
      end

      let(:cng_fips_pipeline_variables) do
        {
          FIPS_PIPELINE: 'true',
          GITLAB_ASSETS_TAG: '16-1-stable-ee',
          GITLAB_REF_SLUG: '16.1.2-internal1',
          GITLAB_VERSION: '16-1-stable-ee',
          INTERNAL_RELEASE_VERSION: '16.1.2-internal1',
          INTERNAL_RELEASE: 'true'
        }
      end

      it 'triggers the commit updater service' do
        expect(updater).to receive(:execute)

        without_dry_run do
          release.execute
        end
      end

      it 'triggers a pipeline on Dev Omnibus' do
        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/omnibus-gitlab',
            omnibus_pipeline_variables,
            branch: '16-1-stable'
          )

        without_dry_run do
          release.execute
        end
      end

      it 'triggers EE, UBI, and FIPS pipelines on Dev CNG/Images' do
        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/charts/components/images',
            cng_ee_pipeline_variables,
            branch: '16-1-stable'
          )

        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/charts/components/images',
            cng_ubi_pipeline_variables,
            branch: '16-1-stable'
          )

        expect(dev_client)
          .to receive(:create_pipeline)
          .with(
            'gitlab/charts/components/images',
            cng_fips_pipeline_variables,
            branch: '16-1-stable'
          )

        without_dry_run do
          release.execute
        end
      end

      it 'records the release metadata' do
        expect(ReleaseTools::InternalRelease::Metadata)
          .to receive(:new)
          .with(version: '16.1.2-internal1')

        expect(metadata)
          .to receive(:record)

        without_dry_run do
          release.execute
        end
      end
    end

    context 'when it is a dry run' do
      it 'does not trigger the wait service' do
        expect(wait_dev_service)
          .not_to receive(:execute)
      end

      it 'does not trigger a pipeline on Dev Omnibus or CNG/Images' do
        expect(dev_client)
          .not_to receive(:create_pipeline)

        release.execute
      end
    end

    it 'sends a success notification when execution completes' do
      expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
        .to receive(:new)
        .with(
          job_type: "Build internal release package 16.1.0-internal0",
          status: :success,
          release_type: :internal
        )

      expect(notifier).to receive(:send_notification)

      without_dry_run do
        release.execute
      end
    end

    it 'sends a failure notification when an error occurs' do
      allow(updater).to receive(:execute).and_raise(StandardError)

      expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
        .to receive(:new)
        .with(
          job_type: "Build internal release package 16.1.0-internal0",
          status: :failed,
          release_type: :internal
        )

      expect(notifier).to receive(:send_notification)

      expect { without_dry_run { release.execute } }.to raise_error(StandardError)
    end
  end
end
