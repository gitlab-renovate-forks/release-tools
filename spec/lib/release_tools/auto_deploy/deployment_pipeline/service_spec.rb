# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::DeploymentPipeline::Service do
  describe 'execute' do
    before do
      allow(ReleaseTools::GitlabOpsClient).to receive(:create_pipeline).and_return(build(:pipeline))

      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Sequencer)
        .to receive(:can_start_deployment?)
        .and_return(true)
    end

    it 'does not call create_pipeline if no version can be found' do
      expect(ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::Latest)
        .to receive(:new)
        .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::Latest, product_version: nil))

      expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger).not_to receive(:new)

      described_class.new(strategy: :latest).execute
    end

    context 'strategy: :version' do
      subject(:execute) { described_class.new(strategy: :version, version:).execute }

      let(:version) { '17.6.202411052200' }
      let(:product_version) { ReleaseTools::ProductVersion.new(version) }

      it 'triggers deployment' do
        version_selector_class = ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::VersionSelector
        expect(version_selector_class)
          .to receive(:new)
          .with(version: version, skip_suitable_check: false)
          .and_return(instance_double(version_selector_class, product_version: product_version))

        expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger)
          .to receive(:new)
          .with(product_version)
          .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger, execute: nil))

        execute
      end

      context 'if version is not provided' do
        let(:version) { nil }

        it 'raises error' do
          expect { execute }.to raise_error(ArgumentError)
        end
      end

      context 'when sequencer returns false' do
        before do
          allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Sequencer)
            .to receive(:can_start_deployment?)
            .and_return(false)
        end

        it 'raises error' do
          expect { execute }.to raise_error(described_class::CannotStartPipelineError)
        end
      end

      context 'when force_version_deployment is true' do
        subject(:execute) { described_class.new(strategy: :version, version:, force_version_deployment: true).execute }

        it 'sets skip_suitable_check to true' do
          version_selector_klass = ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::VersionSelector
          expect(version_selector_klass)
            .to receive(:new)
            .with(version: version, skip_suitable_check: true)
            .and_return(instance_double(version_selector_klass, product_version: product_version))

          execute
        end

        it 'does not call Sequencer with version strategy' do
          expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Sequencer).not_to receive(:can_start_deployment?)

          execute
        end
      end

      context 'when do_not_create_new_deployment_pipelines FF is on' do
        before do
          enable_feature(:do_not_auto_create_new_deployment_pipelines)
        end

        it 'ignores the feature flag' do
          version_selector_class = ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::VersionSelector
          expect(version_selector_class)
            .to receive(:new)
            .with(version: version, skip_suitable_check: false)
            .and_return(instance_double(version_selector_class, product_version: product_version))

          expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger)
            .to receive(:new)
            .with(product_version)
            .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger, execute: nil))

          execute
        end
      end
    end

    context 'strategy: :latest' do
      subject(:execute) { described_class.new(strategy: :latest).execute }

      let(:product_version) { ReleaseTools::ProductVersion.new('17.6.202411052200') }

      before do
        allow(ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::Latest)
          .to receive(:new)
          .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::Latest, product_version: product_version))
      end

      it 'triggers deployment' do
        expect(ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::Latest).to receive(:new)

        expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger)
          .to receive(:new)
          .with(product_version)
          .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger, execute: nil))

        execute
      end

      context 'with force_version_deployment' do
        # force_version_deployment does nothing when used with the latest strategy
        it 'does nothing' do
          expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Sequencer).to receive(:can_start_deployment?)

          execute
        end
      end

      context 'when do_not_create_new_deployment_pipelines FF is on' do
        before do
          enable_feature(:do_not_auto_create_new_deployment_pipelines)
        end

        it 'does not create pipeline' do
          expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Sequencer).not_to receive(:can_start_deployment?)

          expect { execute }.to raise_error(described_class::CannotStartPipelineError)
        end
      end
    end

    context 'if can_start_deployment? is false' do
      before do
        allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Sequencer)
          .to receive(:can_start_deployment?)
          .and_return(false)
      end

      it 'does not trigger deployment' do
        expect(ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::Latest).not_to receive(:new)
        expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Trigger).not_to receive(:new)

        described_class.new(strategy: :latest).execute
      end
    end

    context 'strategy: :invalid' do
      subject(:execute) { described_class.new(strategy: :invalid).execute }

      it 'raises error' do
        expect { execute }.to raise_error(ArgumentError, 'invalid is not a supported strategy')
      end
    end
  end
end
