# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::Latest do
  describe '#product_version' do
    subject(:version_to_deploy) { described_class.new.product_version }

    let(:first_version) { build(:product_version) }
    let(:second_version) { build(:product_version) }

    let(:first_version_checker) do
      instance_double(
        ReleaseTools::AutoDeploy::DeploymentPipeline::Version,
        newer_than_latest_deployment?: true,
        valid_auto_deploy_version?: true
      )
    end

    let(:second_version_checker) do
      instance_double(
        ReleaseTools::AutoDeploy::DeploymentPipeline::Version,
        newer_than_latest_deployment?: false,
        valid_auto_deploy_version?: true
      )
    end

    before do
      allow(ReleaseTools::ProductVersion)
        .to receive(:each)
        .and_yield(first_version)
        .and_yield(second_version)

      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Version)
        .to receive(:new)
        .with(first_version)
        .and_return(first_version_checker)

      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Version)
        .to receive(:new)
        .with(second_version)
        .and_return(second_version_checker)
    end

    it 'returns early if it finds a suitable version' do
      expect(first_version_checker).to receive(:suitable?).and_return(true).twice

      expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Version)
        .not_to receive(:new)
        .with(second_version)

      expect(version_to_deploy).to eq(first_version)
    end

    it 'breaks loop if it finds version older than latest deployed' do
      expect(first_version_checker).to receive(:newer_than_latest_deployment?).and_return(false)

      expect(first_version_checker).to receive(:suitable?).once

      expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Version)
        .not_to receive(:new)
        .with(second_version)

      expect(version_to_deploy).to be_nil
    end
  end
end
