# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::DeploymentPipeline::PackageSelection::VersionSelector do
  describe 'execute' do
    subject(:version_to_deploy) { described_class.new(version:).product_version }

    let(:version) { '17.6.202411052200' }
    let(:product_version) { ReleaseTools::ProductVersion.new(version) }

    it 'raises error if version not suitable for deployment' do
      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Version)
        .to receive(:new)
        .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Version, suitable?: false))

      expect { version_to_deploy }.to raise_error(described_class::UnsuitableVersionError, "'17.6.202411052200' is not suitable for deployment")
    end

    it 'returns product version' do
      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Version)
        .to receive(:new)
        .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Version, suitable?: true))

      expect(version_to_deploy).to eq(product_version)
    end

    context 'when skip_suitable_check is set to true' do
      subject(:version_to_deploy) { described_class.new(version:, skip_suitable_check: true).product_version }

      it 'calls DeploymentPipeline::Version with skip_checks true' do
        expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Version)
          .to receive(:new)
          .with(product_version, skip_checks: true)
          .and_call_original

        expect(version_to_deploy).to eq(product_version)
      end
    end
  end
end
