# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::DeploymentPipeline::Sequencer do
  describe '.can_start_deployment?' do
    subject(:can_start_deployment?) { described_class.can_start_deployment? }

    let(:pipeline) { build(:pipeline, :running) }
    let(:jobs) { [build(:job), build(:job, job_status, name: 'deploy:gprd-cny')] }

    before do
      allow(ReleaseTools::AutoDeploy::DeploymentPipeline::Latest)
        .to receive(:pipeline)
        .and_return(pipeline)

      allow(ReleaseTools::GitlabOpsClient)
        .to receive(:pipeline_bridges)
        .and_return(Gitlab::PaginatedResponse.new(jobs))
    end

    where(:job_status, :result) do
      [
        [:running, false],
        [:success, true],
        [:pending, false]
      ]
    end

    with_them do
      it 'returns true if the job has completed' do
        expect(can_start_deployment?).to eq(result)
      end
    end

    context 'with canceled gprd-cny deployer pipeline' do
      let(:job_status) { :canceled }
      let(:pipeline) { build(:pipeline, :manual) }

      it 'returns true' do
        expect(can_start_deployment?).to be_truthy
      end
    end

    context 'with canceled pipeline' do
      let(:job_status) { :skipped }
      let(:pipeline) { build(:pipeline, :canceled) }

      it 'returns true' do
        expect(can_start_deployment?).to be_truthy
      end
    end

    context 'with failed job' do
      let(:job_status) { :failed }
      let(:pipeline) { build(:pipeline, :manual, created_at: 90.minutes.ago.iso8601) }

      %i[skipped failed].each do |status|
        let(:job_status) { status }

        it 'returns true' do
          expect(can_start_deployment?).to be_truthy
        end
      end

      context 'when 90 minutes have not passed' do
        let(:pipeline) { build(:pipeline, :manual, created_at: 1.hour.ago.iso8601) }

        it 'returns false' do
          expect(can_start_deployment?).to be_falsey
        end
      end
    end
  end
end
