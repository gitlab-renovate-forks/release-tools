# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::PostDeployMigrations::Prepare do
  let(:post_migrations) do
    [
      '20220510192117_foo_bar.rb',
      '20220523171107_bar_foo_id.rb'
    ]
  end

  let(:release_note) do
    double('ReleaseTools::AutoDeploy::PostDeployMigrations::ReleaseNote')
  end

  let(:notification) do
    double('ReleaseTools::Slack::PostDeployPipelineInitialNotification')
  end

  let(:production_status) do
    instance_spy(
      ReleaseTools::Promotion::ProductionStatus,
      fine?: true
    )
  end

  let(:job_trace) { "a\n Canceling the prepare job" }

  let(:fake_client) do
    stub_const('ReleaseTools::GitlabOpsClient', spy)
  end

  subject(:prepare) { described_class.new }

  before do
    allow(ReleaseTools::Promotion::ProductionStatus)
      .to receive(:new)
      .and_return(production_status)

    allow(ReleaseTools::AutoDeploy::PostDeployMigrations::ReleaseNote)
      .to receive(:new)
      .and_return(release_note)

    allow(ReleaseTools::Slack::PostDeployPipelineInitialNotification)
      .to receive(:new)
      .and_return(notification)

    allow(fake_client)
      .to receive(:job_trace)
      .and_return(job_trace)

    allow(fake_client)
      .to receive(:cancel_job)
  end

  around do |ex|
    ClimateControl.modify(CI_JOB_ID: '123') do
      ex.run
    end
  end

  describe '#execute' do
    context 'with pending post migrations' do
      around do |ex|
        ClimateControl.modify(PENDING_POST_DEPLOY_MIGRATIONS: "[ '20220510192117 post 17.2 FooBar', '20220523171107 post 17.2 BarFooId' ]") do
          ex.run
        end
      end

      it 'parse raw migrations' do
        expect(prepare.pending_post_migrations).to eq(post_migrations)
      end

      it 'proceeds to execute the post-deploy pipeline' do
        expect(production_status).to receive(:fine?)
        expect(release_note).to receive(:execute)
        expect(fake_client).not_to receive(:job_trace)
        expect(fake_client).not_to receive(:cancel_job)

        prepare.execute
      end

      context 'when production is unhealthy' do
        let(:production_status) do
          instance_spy(
            ReleaseTools::Promotion::ProductionStatus,
            fine?: false,
            failed_checks: [
              double(:active_deployments, name: 'active_deployments'),
              double(:change_requests, name: 'change_requests')
            ]
          )
        end

        it 'skips post-deploy pipeline execution' do
          expect(notification)
            .to receive(:production_status_failed_message)
            .with(production_status)

          expect(release_note).not_to receive(:execute)
          expect(fake_client).to receive(:job_trace)
          expect(fake_client).to receive(:cancel_job)

          prepare.execute
        end
      end

      context 'when there is a blocking vacuum process' do
        before do
          allow(ReleaseTools::Prometheus::WraparoundVacuumChecks)
            .to receive(:running?)
            .with(post_migrations)
            .and_return(true)
        end

        it 'skips post-deploy pipeline execution' do
          expect(notification)
            .to receive(:wraparound_vacuum_on_post_migrations_message)

          expect(release_note).not_to receive(:execute)
          expect(fake_client).to receive(:job_trace)
          expect(fake_client).to receive(:cancel_job)

          prepare.execute
        end
      end
    end

    context 'without pending post-migrations' do
      around do |ex|
        ClimateControl.modify(PENDING_POST_DEPLOY_MIGRATIONS: "[]") do
          ex.run
        end
      end

      it 'skips post-deploy pipeline execution' do
        expect(notification)
          .to receive(:no_pending_post_migrations_message)

        expect(release_note).not_to receive(:execute)
        expect(fake_client).to receive(:job_trace)
        expect(fake_client).to receive(:cancel_job)

        prepare.execute
      end
    end
  end
end
