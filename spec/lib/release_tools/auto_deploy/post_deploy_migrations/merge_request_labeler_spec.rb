# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::PostDeployMigrations::MergeRequestLabeler do
  subject(:labeler) { described_class.new }

  describe '#label_merge_requests' do
    let(:deploy) do
      ReleaseTools::AutoDeploy::PostDeployMigrations::DeploymentTracker::Deployment
        .new(ReleaseTools::Project::GitlabEe.canonical_or_security_path, 1, 'success')
    end

    context 'when deploying to an environment without a workflow label' do
      it 'does not update any merge requests' do
        expect(ReleaseTools::Tracker::MergeRequestUpdater)
          .not_to receive(:for_successful_deployments)

        without_dry_run do
          labeler.label_merge_requests('kittens', [deploy])
        end
      end
    end

    context 'when deploying to valid environment' do
      it 'adds the workflow label to all merge requests' do
        merge_request =
          double(:mr, project_id: 2, iid: 3, labels: %w[foo], web_url: 'foo', source_project_id: 1)

        page = Gitlab::PaginatedResponse.new([merge_request])

        allow(ReleaseTools::GitlabClient)
          .to receive(:deployed_merge_requests)
          .with(ReleaseTools::Project::GitlabEe.canonical_or_security_path, 1)
          .and_return(page)

        expect(ReleaseTools::GitlabClient)
          .to receive(:update_merge_request)
          .with(2, 3, { labels: 'workflow::post-deploy-db-staging,foo' })

        without_dry_run do
          labeler.label_merge_requests('db/gstg', [deploy])
        end
      end
    end

    context 'when a deployment did not succeed' do
      let(:deploy) do
        ReleaseTools::AutoDeploy::PostDeployMigrations::DeploymentTracker::Deployment
          .new(ReleaseTools::Project::GitlabEe.canonical_or_security_path, 1, 'failed')
      end

      it 'skips the execution' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:create_merge_request_comment)

        without_dry_run do
          labeler.label_merge_requests('db/gstg', [deploy])
        end
      end
    end

    context 'with a dry-run' do
      it 'skips the execution' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:update_merge_request)

        labeler.label_merge_requests('db/gstg', [deploy])
      end
    end
  end
end
