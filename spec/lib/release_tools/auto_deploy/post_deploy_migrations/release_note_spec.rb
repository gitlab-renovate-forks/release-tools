# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::AutoDeploy::PostDeployMigrations::ReleaseNote do
  let(:user) { build(:user, username: 'liz.lemon') }
  let(:pipeline) { build(:pipeline, user: user, web_url: 'http://example.com/pipeline') }
  let(:definitions) { double(ReleaseTools::ReleaseManagers::Definitions) }

  let(:release_manager_schedule) do
    spy(
      ReleaseTools::ReleaseManagers::Schedule,
      active_version: ReleaseTools::Version.new('14.6')
    )
  end

  let(:monthly_issue) do
    instance_spy(ReleaseTools::MonthlyIssue, url: 'gitlab_url')
  end

  let(:release_manager_definition) do
    ReleaseTools::ReleaseManagers::Definitions::User.new(
      'liz.lemon',
      { 'gitlab.com' => 'liz.lemon' }
    )
  end

  let(:production_status) do
    double(
      ReleaseTools::Promotion::ProductionStatus,
      fine?: true,
      to_issue_body: 'a collection of check results'
    )
  end

  let(:pending_post_migrations) do
    [
      '20220510192117_index_expirable_unknown_artifacts_for_removal_n.rb',
      '20220523171107_drop_deploy_tokens_token_column.rb'
    ]
  end

  subject(:release_note) do
    described_class.new(production_status, pending_post_migrations)
  end

  before do
    allow(ReleaseTools::GitlabOpsClient)
      .to receive(:pipeline)
      .and_return(pipeline)

    allow(ReleaseTools::ReleaseManagers::Schedule)
      .to receive(:new)
      .and_return(release_manager_schedule)

    allow(ReleaseTools::ReleaseManagers::Definitions)
      .to receive(:new)
      .and_return(definitions)

    allow(definitions)
      .to receive(:find_user)
      .with(user.username, instance: :ops)
      .and_return(release_manager_definition)

    allow(ReleaseTools::MonthlyIssue)
      .to receive(:new).and_return(monthly_issue)
  end

  describe '#execute' do
    around do |test|
      ClimateControl.modify(CI_PIPELINE_URL: pipeline.web_url, CI_PIPELINE_ID: '123', RELEASE_USER: nil) do
        test.run
      end
    end

    before do
      allow(ReleaseTools::GitlabClient)
        .to receive(:last_successful_deployment)
        .with('gitlab-org/security/gitlab', 'gprd')
        .and_return(double(sha: 'sha'))
    end

    it 'creates a note in monthly issue' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:create_issue_note)
        .with(
          monthly_issue.project,
          {
            issue: monthly_issue,
            body: instance_of(String)
          }
        )

      without_dry_run do
        release_note.execute
      end
    end

    it 'links the pipeline' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:create_issue_note)
        .with(
          monthly_issue.project,
          hash_including(body: include("[pipeline](#{pipeline.web_url})"))
        )

      without_dry_run do
        release_note.execute
      end
    end

    it 'pings the user who triggered the chatops command' do
      allow(definitions)
        .to receive(:find_user)
        .with('jack.donaghy', instance: :ops)
        .and_return(build(:user, name: 'jack.donaghy', production: 'jack.donaghy'))

      expect(ReleaseTools::GitlabClient)
        .to receive(:create_issue_note)
        .with(monthly_issue.project, hash_including(body: include("@jack.donaghy")))

      ClimateControl.modify(RELEASE_USER: 'jack.donaghy') do
        without_dry_run { release_note.execute }
      end
    end

    it 'pings the release manager based on the pipeline if RELEASE_USER is not set' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:create_issue_note)
        .with(monthly_issue.project, hash_including(body: include("@liz.lemon")))

      without_dry_run do
        release_note.execute
      end
    end

    it 'includes the production status report' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:create_issue_note)
        .with(
          monthly_issue.project,
          hash_including(body: include(production_status.to_issue_body))
        )

      without_dry_run do
        release_note.execute
      end
    end

    it 'includes a list of post-deploy migrations' do
      expect(ReleaseTools::GitlabClient)
        .to receive(:create_issue_note)
        .with(
          monthly_issue.project,
          hash_including(
            body: include(
              "[#{pending_post_migrations.first}](https://gitlab.com/gitlab-org/security/gitlab/-/blob/sha/db/post_migrate/#{pending_post_migrations.first})",
              "[#{pending_post_migrations.last}](https://gitlab.com/gitlab-org/security/gitlab/-/blob/sha/db/post_migrate/#{pending_post_migrations.last})"
            )
          )
        )

      without_dry_run do
        release_note.execute
      end
    end

    context 'with dry run mode' do
      it 'does not create note' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:create_issue_note)

        release_note.execute
      end
    end
  end
end
