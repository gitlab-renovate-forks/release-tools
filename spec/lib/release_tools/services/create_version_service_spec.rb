# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::CreateVersionService do
  let(:client) { ReleaseTools::VersionClient }
  let(:version) { "17.0.0" }
  let(:response) { Gitlab::ObjectifiedHash.new({ "id" => 7, "version" => "17.2.1" }) }
  let(:error_response) { Gitlab::ObjectifiedHash.new({ "message" => { "version" => ["has already been taken"] } }) }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }
  let(:critical_patch_release?) { false }

  before do
    allow(ReleaseTools::SharedStatus)
      .to receive(:critical_patch_release?)
      .and_return(critical_patch_release?)
  end

  describe '#execute' do
    subject(:create_version) { described_class.new(version: version) }

    context 'when input is a monthly version' do
      it 'creates version on version.gitlab.com' do
        expect(client).to receive(:get_version).once.with(ReleaseTools::Version.new(version))
        expect(client).to receive(:create_version).once.with(
          ReleaseTools::Version.new(version),
          version_type: :monthly
        )
          .and_return(response)
        expect(notifier).to receive(:new).with(
          job_type: "Create new version #{version}",
          status: :success,
          release_type: :monthly
        )

        expect(notifier).to receive(:send_notification)

        without_dry_run do
          create_version.execute
        end
      end
    end

    context 'when input is a patch version' do
      let(:version) { "17.0.1" }

      it 'creates a version on version.gitlab.com' do
        expect(client).to receive(:get_version).once.with(ReleaseTools::Version.new(version))
        expect(client).to receive(:create_version).once.with(
          ReleaseTools::Version.new(version),
          version_type: :patch_regular
        )
          .and_return(response)
        expect(notifier).to receive(:new).with(
          job_type: "Create new version #{version}",
          status: :success,
          release_type: :patch
        )

        expect(notifier).to receive(:send_notification)

        without_dry_run do
          create_version.execute
        end
      end

      context 'when critical patch release' do
        let(:critical_patch_release?) { true }

        it 'creates a critical patch version on version.gitlab.com' do
          expect(client).to receive(:get_version).once.with(ReleaseTools::Version.new(version))

          expect(client).to receive(:create_version).once.with(
            ReleaseTools::Version.new(version),
            version_type: :patch_critical
          ).and_return(response)

          expect(notifier).to receive(:new).with(
            job_type: "Create new version #{version}",
            status: :success,
            release_type: :patch
          )

          expect(notifier).to receive(:send_notification)

          without_dry_run do
            create_version.execute
          end
        end
      end
    end

    context 'when version exists' do
      before do
        allow(create_version).to receive(:version_exists?).and_return(true)
      end

      it 'return without performing any further actions' do
        expect(client).not_to receive(:create_version)
        expect(notifier).not_to receive(:new)

        without_dry_run do
          create_version.execute
        end
      end
    end

    context 'when something goes wrong' do
      it 'raises an error' do
        allow(client)
          .to receive(:get_version)
          .and_raise(StandardError)
        expect(notifier).to receive(:new).with(
          job_type: "Create new version #{version}",
          status: :failed,
          release_type: :monthly
        )

        expect(notifier).to receive(:send_notification)
        without_dry_run do
          expect { create_version.execute }.to raise_error(StandardError)
        end
      end
    end

    context 'dry run' do
      it 'does not create the version' do
        expect(client).not_to receive(:get_version)
        expect(client).not_to receive(:create_version)
        expect(notifier).not_to receive(:send_notification)

        create_version
      end
    end
  end
end
