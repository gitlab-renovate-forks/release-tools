# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::PackagesStatus::Functionality do
  context 'with tag package type' do
    it_behaves_like 'packages status service',
                    'check-packages-functionality',
                    :tag
  end

  context 'with internal package type' do
    it_behaves_like 'packages status service',
                    'check-packages-functionality',
                    :internal
  end
end
