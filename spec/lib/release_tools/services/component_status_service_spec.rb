# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::ComponentStatusService do
  describe '#execute' do
    let(:project) { ReleaseTools::ManagedVersioning::PROJECTS.first }
    let(:project_path) { project.path }
    let(:branch) { project.default_branch }
    let(:client) { class_spy(ReleaseTools::GitlabClient) }
    let(:service) { described_class.new(project: project, branch: branch, client: client) }

    subject { service.execute }

    it 'returns the pipeline status' do
      commit_response = double(:commit_response, last_pipeline: double(status: 'success'))

      commits_double = double(:commit, id: 'abc123')

      expect(client).to receive(:commits).with(project_path, ref_name: branch)
        .and_return([commits_double])

      expect(client).to receive(:commit).with(project_path, ref: 'abc123')
        .and_return(commit_response)

      expect(subject).to eq('success')
    end

    context 'with commit depth greater than 1' do
      let(:project) { ReleaseTools::Project::GitlabEe }

      it 'continues until it finds a non nil or running pipeline' do
        commit_response1 = double(:commit_response, last_pipeline: double(status: nil))
        commit_response2 = double(:commit_response, last_pipeline: double(status: 'running'))
        commit_response3 = double(:commit_response, last_pipeline: double(status: 'skipped'))
        commit_response4 = double(:commit_response, last_pipeline: double(status: 'failed'))

        commits_double1 = double(:commit, id: '1')
        commits_double2 = double(:commit, id: '2')
        commits_double3 = double(:commit, id: '3')
        commits_double4 = double(:commit, id: '4')
        commits_double5 = double(:commit, id: '5')
        commits_response = [commits_double1, commits_double2, commits_double3, commits_double4, commits_double5]

        expect(client).to receive(:commits).with(project_path, ref_name: branch)
          .and_return(commits_response)

        expect(client).to receive(:commit).with(project_path, ref: '1')
          .and_return(commit_response1)

        expect(client).to receive(:commit).with(project_path, ref: '2')
          .and_return(commit_response2)

        expect(client).to receive(:commit).with(project_path, ref: '3')
          .and_return(commit_response3)

        expect(client).to receive(:commit).with(project_path, ref: '4')
          .and_return(commit_response4)

        expect(subject).to eq('failed')
      end
    end
  end
end
