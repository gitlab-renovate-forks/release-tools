# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::CNGPublishService do
  describe '#execute' do
    it_behaves_like 'returns failed pipelines'

    context 'when no pipeline exists', vcr: { cassette_name: 'pipelines/cng/no_pipeline' } do
      let(:version) { ReleaseTools::Version.new('83.7.2') }

      it 'raises PipelineNotFoundError' do
        service = described_class.new(version)

        expect { service.execute }
          .to raise_error(described_class::PipelineNotFoundError)
      end
    end

    context 'when one pipeline exists' do
      context 'and there are manual jobs', vcr: { cassette_name: 'pipelines/cng/pending' } do
        let(:version) { ReleaseTools::Version.new('15.2.0-rc32') }

        it 'plays all jobs in a release stage' do
          service = described_class.new(version)

          client = service.send(:client)
          allow(client).to receive(:pipelines).and_call_original
          allow(client).to receive(:pipeline_jobs).and_call_original

          # CE, EE, and FIPS each have 1 manual job and UBI has 2 manual jobs
          expect(client).to receive(:job_play).exactly(5).times

          played_pipelines, failed_pipelines =
            without_dry_run do
              service.execute
            end

          expect(played_pipelines.length).to eq(4)
          expect(failed_pipelines.length).to eq(0)
        end

        context 'and there are manual jobs for versions before when FIPS tags were introduced', vcr: { cassette_name: 'pipelines/cng/pending_old' } do
          let(:version) { ReleaseTools::Version.new('12.6.0-rc32') }

          it 'plays all jobs in a release stage' do
            service = described_class.new(version)

            client = service.send(:client)
            allow(client).to receive(:pipelines).and_call_original
            allow(client).to receive(:pipeline_jobs).and_call_original

            # CE, and EE each have 1 manual job and UBI has 2 manual jobs
            expect(client).to receive(:job_play).exactly(4).times

            played_pipelines, failed_pipelines =
              without_dry_run do
                service.execute
              end

            expect(played_pipelines.length).to eq(3)
            expect(failed_pipelines.length).to eq(0)
          end
        end
      end

      context 'and there are no manual jobs', vcr: { cassette_name: 'pipelines/cng/released' } do
        let(:version) { ReleaseTools::Version.new('15.2.0') }

        it 'does not play any job' do
          service = described_class.new(version)

          client = service.send(:client)
          expect(client).not_to receive(:job_play)

          expect(service.execute.flatten).to be_empty
        end
      end

      context 'and there are no manual jobs for versions before when FIPS tags were introduced', vcr: { cassette_name: 'pipelines/cng/released_old' } do
        let(:version) { ReleaseTools::Version.new('12.0.0') }

        it 'does not play any job' do
          service = described_class.new(version)

          client = service.send(:client)
          expect(client).not_to receive(:job_play)

          expect(service.execute.flatten).to be_empty
        end
      end
    end
  end

  describe '#release_versions' do
    let(:version) { ReleaseTools::Version.new('15.2.0') }

    it 'contains UBI and FIPS tags' do
      expect(described_class.new(version).release_versions).to eq %w(v15.2.0 v15.2.0-ee v15.2.0-ubi8 v15.2.0-fips)
    end

    context 'for versions before when FIPS tags were introduced' do
      let(:version) { ReleaseTools::Version.new('14.2.0') }

      it 'contains UBI and FIPS tags' do
        expect(described_class.new(version).release_versions).to eq %w(v14.2.0 v14.2.0-ee v14.2.0-ubi8)
      end
    end
  end
end
