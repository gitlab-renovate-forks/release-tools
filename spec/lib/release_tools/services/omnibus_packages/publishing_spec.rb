# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::OmnibusPackages::Publishing do
  packages_status_service = ReleaseTools::Services::PackagesStatus::Availability

  it_behaves_like 'check omnibus packages service',
                  packages_status_service
end
