# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::MergeWhenPipelineSucceedsService do
  let(:merge_request) { instance_double(ReleaseTools::MergeRequest, web_url: '') }
  let(:token) { 'a token' }
  let(:approve_service) { instance_double(ReleaseTools::Services::ApproveService) }

  subject(:service) { described_class.new(merge_request, token: token) }

  before do
    allow(ReleaseTools::Services::ApproveService).to receive(:new).and_return(approve_service)
  end

  describe '.new' do
    it 'initialize a Gitlab::Client with the given token and endpoint' do
      endpoint = 'https://gitlab.example.com'
      client = class_double(Gitlab::Client).as_stubbed_const
      expect(client)
        .to receive(:new)
        .with(
          {
            private_token: token,
            endpoint: endpoint,
            httparty: a_hash_including(logger: instance_of(SemanticLogger::Logger))
          }
        )
        .and_return(double(:client, user: double(:user, id: 2)))

      allow(merge_request).to receive(:author).and_return(double(:user, id: 1))
      described_class.new(merge_request, token: token, endpoint: endpoint)
    end
  end

  describe '#execute' do
    context 'when the pipeline is ready' do
      it 'approves it, wait for the pipeline to start, and accept the merge request' do
        expect(approve_service).to receive(:execute)
        expect(service).to receive(:merge_when_pipeline_succeeds)

        without_dry_run do
          service.execute
        end
      end
    end

    context 'when response is 405 method not allowed' do
      before do
        allow(approve_service).to receive(:execute)

        allow(merge_request).to receive_messages(iid: 1, project_id: 1)

        allow(service.client)
          .to receive(:accept_merge_request)
          .and_raise(gitlab_error(:MethodNotAllowed))

        allow(merge_request)
          .to receive(:detailed_merge_status)
          .and_return('ci_still_running')
      end

      it 'does not raise error' do
        without_dry_run do
          expect { service.execute }.not_to raise_error
        end
      end
    end
  end

  describe '#merge_when_pipeline_succeeds' do
    it 'delegates to Gitlab::Client#accept_merge_request' do
      allow(merge_request).to receive_messages(iid: double(:iid), project_id: double(:project_id))

      expect(service.client)
        .to receive(:accept_merge_request)
        .with(
          merge_request.project_id,
          merge_request.iid,
          {
            merge_when_pipeline_succeeds: true,
            should_remove_source_branch: true
          }
        )
        .and_return(
          build(:gitlab_response, state: 'open', merge_when_pipeline_succeeds: true, detailed_merge_status: '')
        )

      without_dry_run do
        service.merge_when_pipeline_succeeds
      end
    end

    it_behaves_like 'raises error when MWPS not set' do
      let(:merge_when_pipeline_succeeds_options) do
        {
          merge_when_pipeline_succeeds: true,
          should_remove_source_branch: true
        }
      end
    end
  end
end
