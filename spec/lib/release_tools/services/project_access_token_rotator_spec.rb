# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::ProjectAccessTokenRotator do
  subject(:rotator) { described_class.new(token_name, project: project, client: client, min_available_days: min_days, renew_for: renew_for) }

  let(:token_name) { 'my_token' }
  let(:project) { ReleaseTools::Project::GitlabCe }
  let(:client) { class_double(ReleaseTools::GitlabClient) }
  let(:min_days) { 8 }
  let(:renew_for) { 1.week }

  describe '#execute' do
    context 'when token expires after minimum days' do
      before do
        allow(rotator).to receive(:expires_in_days).and_return(min_days + 1)
      end

      it 'does not rotate the token' do
        expect(rotator).not_to receive(:rotate_token!)

        rotator.execute

        expect(rotator).not_to be_rotated
      end
    end

    context 'when token expires before minimum days' do
      it 'rotates the token' do
        Timecop.freeze(Time.new(2024, 4, 10)) do
          expired_token = build(:token, :expired, name: token_name)
          revoked_token = build(:token, :revoked, name: token_name)
          valid_token = build(:token, name: token_name, expires_at: (Date.today + 1.day).to_date.iso8601)
          tokens = build(:gitlab_response, auto_paginate: [])
          expect(tokens).to receive(:auto_paginate).and_yield(expired_token).and_yield(revoked_token).and_yield(valid_token)
          expect(client).to receive(:project_access_tokens).with(project).and_return(tokens)
          expect(rotator).to receive(:rotate_token!).and_call_original

          expires_at = '2024-04-17' # based on renew_for
          rotated_token = build(:token, name: token_name, token: 'new secret')
          expect(client).to receive(:rotate_project_access_token).with(project, valid_token.id, expires_at: expires_at).and_return(rotated_token)

          rotator.execute

          expect(rotator).to be_rotated
        end
      end
    end
  end

  describe '#update_ci_var' do
    let(:key) { 'MY_TOKEN' }

    context 'when token was not rotated' do
      it 'does not update CI variable' do
        expect(ReleaseTools::GitlabOpsClient).not_to receive(:update_project_variable)
        rotator.update_ci_var(key)
      end
    end

    context 'when token was rotated' do
      before do
        rotator.instance_variable_set(:@rotated, true)
        rotator.instance_variable_set(:@new_token, 'secret123')
      end

      it 'updates CI variable with new token' do
        expect(ReleaseTools::GitlabOpsClient).to receive(:update_project_variable).with(ReleaseTools::Project::ReleaseTools, key, 'secret123')
        rotator.update_ci_var(key)
      end

      it 'resets new token instance variable' do
        allow(ReleaseTools::GitlabOpsClient).to receive(:update_project_variable)
        rotator.update_ci_var(key)
        expect(rotator.instance_variable_get(:@new_token)).to be_nil
      end
    end
  end
end
