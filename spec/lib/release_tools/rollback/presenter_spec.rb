# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::Presenter do
  include RollbackHelper

  let(:link_style) { :markdown }

  describe '#present' do
    let(:deployment) { rollback_upcoming_deployment_stub }

    let(:component_diffs) do
      [
        "omnibus-gitlab - [sha1...sha2](https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/compare/sha1...sha2)",
        "gitlab - [sha3...sha4](https://gitlab.com/gitlab-org/security/gitlab/-/compare/sha3...sha4)"
      ]
    end

    before do
      allow(ReleaseTools::Metadata::Presenter)
        .to receive(:new)
        .and_return(instance_double(ReleaseTools::Metadata::Presenter, component_diffs: component_diffs))
    end

    it 'includes current and target package names' do
      attributes = {
        current: stub_product_version('bf7bc31ffb1', '2c2465c4dca'),
        target: stub_product_version('10438b18493', '6793946906e')
      }

      comparison = rollback_stub_comparison(attributes)
      instance = described_class.new(comparison, deployment, link_style: link_style)
      present = instance.present

      expect(present).to include("*Current:* `42.1.2021110116-bf7bc31ffb1.2c2465c4dca`\n")
      expect(present).to include("*Target:* `42.1.2021110116-10438b18493.6793946906e`\n")
    end

    it 'includes a comparison link' do
      comparison = rollback_stub_comparison(web_url: '<compare_url>')

      instance = described_class.new(comparison, deployment, link_style: link_style)

      expect(instance.present).to include("*Comparison:* <compare_url>\n")
    end

    it 'includes component diffs' do
      metadata_comparison = instance_double(ReleaseTools::Metadata::Comparison)
      comparison = rollback_stub_comparison(metadata_comparison: metadata_comparison)

      expect(ReleaseTools::Metadata::Presenter)
        .to receive(:new)
        .with(metadata_comparison, { link_style: link_style })

      instance = described_class.new(comparison, deployment, link_style: link_style)

      expect(instance.present).to include("*Comparison for each component:*\n")
      expect(instance.present).to include("- #{component_diffs[0]}\n")
      expect(instance.present).to include("- #{component_diffs[1]}\n")
    end

    context 'with slack link_style' do
      let(:link_style) { :slack }

      let(:component_diffs) do
        [
          "omnibus-gitlab - <https://gitlab.com/gitlab-org/security/omnibus-gitlab/-/compare/sha1...sha2|sha1...sha2>",
          "gitlab - <https://gitlab.com/gitlab-org/security/gitlab/-/compare/sha3...sha4|sha3...sha4>"
        ]
      end

      it 'includes component diffs' do
        metadata_comparison = instance_double(ReleaseTools::Metadata::Comparison)
        comparison = rollback_stub_comparison(metadata_comparison: metadata_comparison)

        expect(ReleaseTools::Metadata::Presenter)
          .to receive(:new)
          .with(metadata_comparison, { link_style: link_style })

        instance = described_class.new(comparison, deployment, link_style: link_style)

        expect(instance.present).to include("*Comparison for each component:*\n")
        expect(instance.present).to include("• #{component_diffs[0]}\n")
        expect(instance.present).to include("• #{component_diffs[1]}\n")
      end
    end
  end

  describe '#header' do
    context 'when rollback is available' do
      it 'states the rollback is available' do
        deployment = rollback_upcoming_deployment_stub
        comparison = rollback_stub_comparison(safe?: true)

        instance = described_class.new(comparison, deployment, link_style: link_style)

        expect(instance.header).to eq(':large_green_circle: Rollback available')
      end
    end

    context 'with post-deploy migrations' do
      it 'states rollback is available' do
        deployment = rollback_upcoming_deployment_stub
        comparison = rollback_stub_comparison(safe?: true)

        instance = described_class.new(comparison, deployment, link_style: link_style)

        expect(instance.header).to eq(':large_green_circle: Rollback available')
      end
    end

    context 'with a running deployment' do
      it 'states rollback is not available' do
        deployment = rollback_upcoming_deployment_stub(any?: true)
        comparison = rollback_stub_comparison(safe?: true)

        instance = described_class.new(comparison, deployment, link_style: link_style)

        expect(instance.header)
          .to eq(':red_circle: Rollback unavailable - A deployment is in progress')
      end
    end

    context 'when a minimum version is set' do
      it 'states rollback is not available' do
        deployment = rollback_upcoming_deployment_stub
        comparison = rollback_stub_comparison(
          safe?: false,
          minimum_version: ReleaseTools::ProductVersion.new('42.1.2021110116')
        )

        instance = described_class.new(comparison, deployment, link_style: link_style)

        expect(instance.header)
          .to eq(':red_circle: Rollback unavailable - Post-deployment migrations were executed after the target version on 42.1.2021110116')
      end
    end
  end

  describe '#unavailable_rollback_multireason?' do
    it 'returns true with multiple reasons' do
      deployment = rollback_upcoming_deployment_stub(any?: true)
      comparison = rollback_stub_comparison(safe?: false)

      instance = described_class.new(comparison, deployment, link_style: link_style)

      expect(instance).to be_unavailable_rollback_multireason
    end

    it 'returns false with a single reason' do
      deployment = rollback_upcoming_deployment_stub(any?: false)
      comparison = rollback_stub_comparison(safe?: true)

      instance = described_class.new(comparison, deployment, link_style: link_style)

      expect(instance).not_to be_unavailable_rollback_multireason
    end
  end

  describe '#rollback_unavailable_reasons_block' do
    it 'returns the unavailable reasons for rollback' do
      deployment = rollback_upcoming_deployment_stub(any?: true)
      comparison = rollback_stub_comparison(
        safe?: false,
        minimum_version: ReleaseTools::ProductVersion.new('42.1.2021110116')
      )

      instance = described_class.new(comparison, deployment, link_style: link_style)

      expect(instance.rollback_unavailable_reasons_block)
        .to eq("- A deployment is in progress\n- Post-deployment migrations were executed after the target version on 42.1.2021110116")
    end
  end
end
