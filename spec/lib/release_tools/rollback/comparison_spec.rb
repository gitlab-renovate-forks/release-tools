# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Rollback::Comparison do
  include MetadataHelper

  let!(:fake_client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:current) { ReleaseTools::ProductVersion.new('15.2.202206301720') }
  let(:target) { ReleaseTools::ProductVersion.new('15.2.202206301320') }
  let(:minimum_version) { ReleaseTools::ProductVersion.new('15.2.202206301320') }
  let(:environment) { 'gstg' }

  def stub_compare(diffs: [])
    comparison = create(
      :comparison,
      diffs: diffs
    )

    allow(fake_client).to receive(:compare).and_return(comparison)
  end

  subject(:comparison) do
    described_class.new(current: current, target: target, environment: environment)
  end

  before do
    metadata = build_metadata(gitlab_sha: 'ad89f6752f3', omnibus_sha: 'ef94506739c')

    allow(current)
      .to receive(:metadata)
      .and_return(metadata)

    allow(target)
      .to receive(:metadata)
      .and_return(metadata)

    allow(ReleaseTools::GitlabOpsClient)
      .to receive(:last_successful_deployment)
      .with(ReleaseTools::Project::Release::Metadata, "db/gstg")
      .and_return(double(sha: 'sha1'))

    allow(ReleaseTools::ProductVersion)
      .to receive(:from_metadata_sha)
      .with('sha1')
      .and_return(minimum_version)
  end

  describe '#execute' do
    it 'compares from the target to the current deployment' do
      expect(fake_client).to receive(:compare).with(
        described_class::PROJECT,
        from: target[ReleaseTools::Project::GitlabEe.metadata_project_name].sha,
        to: current[ReleaseTools::Project::GitlabEe.metadata_project_name].sha
      ).and_return(build(:compare))

      comparison.execute
    end
  end

  describe '#safe' do
    let(:environment) { 'gstg-cny' }

    it 'checks the main stage environment' do
      expect(ReleaseTools::GitlabOpsClient).to receive(:last_successful_deployment).with(anything, 'db/gstg')

      comparison.execute
    end

    context 'when target version is older than minimum version' do
      let(:minimum_version) { ReleaseTools::ProductVersion.new('15.2.202206301620') }

      it 'is unsafe' do
        expect(comparison.execute).not_to be_safe
      end
    end

    context 'when target version is same as minimum version' do
      let(:target) { ReleaseTools::ProductVersion.new('15.2.202206301320') }
      let(:minimum_version) { ReleaseTools::ProductVersion.new('15.2.202206301320') }

      it 'is safe' do
        diffs_with_post_migration =
          [{ 'new_path' => 'db/post_migrate/foo.rb', 'new_file' => true }]

        stub_compare(diffs: diffs_with_post_migration)

        expect(comparison.execute).to be_safe
      end
    end

    context 'when target version is newer than minimum version' do
      let(:target) { ReleaseTools::ProductVersion.new('15.2.202206301620') }
      let(:minimum_version) { ReleaseTools::ProductVersion.new('15.2.202206301320') }

      it 'is safe' do
        diffs = [
          { 'new_path' => 'README.md', 'new_file' => true },
          { 'new_path' => 'db/migrate/foo.rb', 'new_file' => false }
        ]

        stub_compare(diffs: diffs)

        expect(comparison.execute).to be_safe
      end
    end

    it 'considers a diff with migrations as safe' do
      diffs = [
        { 'new_path' => 'db/migrate/foo.rb', 'new_file' => true },
        { 'new_path' => 'db/migrate/bar.rb', 'new_file' => false }
      ]

      stub_compare(diffs: diffs)

      expect(comparison.execute).to be_safe
    end

    it 'considers a diff with post-deploy migrations as safe' do
      diffs = [
        { 'new_path' => 'db/post_migrate/foo.rb', 'new_file' => true },
        { 'new_path' => 'db/post_migrate/bar.rb', 'new_file' => false }
      ]

      stub_compare(diffs: diffs)

      expect(comparison.execute).to be_safe
    end

    it 'considers a diff otherwise safe' do
      diffs = [
        { 'new_path' => 'README.md', 'new_file' => true },
        { 'new_path' => 'db/migrate/foo.rb', 'new_file' => false }
      ]

      stub_compare(diffs: diffs)

      expect(comparison.execute).to be_safe
    end
  end

  describe '#current_auto_deploy_package' do
    it 'returns auto_deploy package' do
      expect(comparison.current_auto_deploy_package)
        .to eq('42.1.2021110116-ad89f6752f3.ef94506739c')
    end
  end

  describe '#target_auto_deploy_package' do
    it 'returns auto_deploy package' do
      expect(comparison.target_auto_deploy_package)
        .to eq('42.1.2021110116-ad89f6752f3.ef94506739c')
    end
  end

  describe '#metadata_comparison' do
    it 'returns instance of Metadata::Comparison' do
      result = comparison.metadata_comparison

      expect(result).to be_instance_of(ReleaseTools::Metadata::Comparison)
      expect(result.source).to eq(comparison.current)
      expect(result.target).to eq(comparison.target)
    end
  end
end
