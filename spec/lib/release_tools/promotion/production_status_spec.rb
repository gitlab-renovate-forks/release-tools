# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::ProductionStatus do
  subject(:status) { described_class.new }

  describe '.new' do
    it 'accepts extra checks' do
      extra_check = spy

      stub_const('ReleaseTools::Promotion::Checks::CustomCheck', extra_check)

      described_class.new(:custom_check)

      expect(extra_check).to have_received(:new)
    end

    it 'raises InvalidCheckError for an invalid check' do
      expect { described_class.new(:invalid_check) }
        .to raise_error(described_class::InvalidCheckError)
    end

    it 'defaults scope to :deployment' do
      expect(ReleaseTools::Promotion::Checks::ActiveIncidents).to receive(:new).with({ scope: :deployment })
      expect(ReleaseTools::Promotion::Checks::ChangeRequests).to receive(:new).with({ scope: :deployment })

      described_class.new
    end

    it 'sets scope to :feature_flag' do
      expect(ReleaseTools::Promotion::Checks::ActiveIncidents).to receive(:new).with({ scope: :feature_flag })
      expect(ReleaseTools::Promotion::Checks::ChangeRequests).to receive(:new).with({ scope: :feature_flag })
      expect(ReleaseTools::Promotion::Checks::ActiveGprdDeployments).to receive(:new).with(no_args)
      expect(ReleaseTools::Promotion::Checks::CanaryUp).to receive(:new).with(no_args)

      described_class.new(:canary_up, :active_gprd_deployments, scope: :feature_flag)
    end
  end

  describe '#fine?' do
    it 'returns true when all checks are fine' do
      expect(status).to receive(:checks).and_return([
        double(fine?: true),
        double(fine?: true)
      ])

      expect(status).to be_fine
    end

    it 'returns false when any check is not fine' do
      expect(status).to receive(:checks).and_return([
        double(fine?: true),
        double(fine?: false),
        double(fine?: true)
      ])

      expect(status).not_to be_fine
    end
  end

  describe '#failed_checks' do
    it 'returns checks that failed' do
      failed_check = double(fine?: false)
      allow(status).to receive(:checks).and_return([
        double(fine?: true),
        failed_check,
        double(fine?: true)
      ])

      expect(status.failed_checks).to contain_exactly(failed_check)
    end
  end

  describe '#to_issue_body' do
    it 'returns the successful status check' do
      expect(status).to receive(:checks).and_return([
        double(fine?: true, to_issue_body: 'result check 1'),
        double(fine?: true, to_issue_body: 'result check 2')
      ]).twice

      text = status.to_issue_body

      expect(text).to match("Production checks pass")
    end
  end

  describe '#to_slack_blocks' do
    context 'when deployment is about to start' do
      context 'when it can start' do
        before do
          allow(status).to receive(:checks).and_return([
            double(fine?: true, to_slack_blocks: { text: 'result check 1' }),
            double(fine?: true, to_slack_blocks: { text: 'result check 2' })
          ])
        end

        it 'includes a success summary' do
          text = status.to_slack_blocks.first.dig(:text, :text)

          expect(text).to eq(':white_check_mark: Production checks pass. :shipit: :fine:')
        end

        it 'shows check blocks' do
          slack_blocks = status.to_slack_blocks

          expect(slack_blocks.length).to eq(1)
        end
      end

      context 'when checks are not fine' do
        before do
          allow(status).to receive(:checks).and_return([
            double(fine?: true, to_slack_blocks: { text: 'result check 1' }),
            double(fine?: false, to_slack_blocks: { text: 'result check 2' })
          ])
        end

        it 'includes a failure summary' do
          text = status.to_slack_blocks.first.dig(:text, :text)

          expect(text).to eq(':red_circle: Production checks fail!')
        end

        it 'shows check blocks' do
          slack_blocks = status.to_slack_blocks

          expect(slack_blocks.length).to eq(2)
          expect(slack_blocks[1][:text]).to eq('result check 2')
        end
      end
    end
  end
end
