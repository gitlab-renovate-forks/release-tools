# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::Checks::ChangeRequests do
  let(:issues) { [] }
  let(:scope) { :deployment }

  subject(:check) { described_class.new(scope: scope) }

  before do
    allow(ReleaseTools::GitlabClient).to receive(:issues).and_return(issues)
  end

  describe '#fine?' do
    context 'when there are no issues on the production tracker' do
      it { is_expected.to be_fine }
    end

    context 'when there are C1 change issues on the production tracker' do
      let(:issues) do
        [
          double('C1', labels: %w(change::in-progress C1)),
          double('C4', labels: %w(change::in-progress C4))
        ]
      end

      context 'with deployment scope' do
        it { is_expected.to be_fine }
      end

      context 'with feature_flag scope' do
        let(:scope) { :feature_flag }

        it { is_expected.to be_fine }
      end
    end

    context 'when there are `blocks deployments` issues' do
      let(:issues) do
        [
          double('blocks_deployment', labels: ['change::in-progress', 'blocks deployments'], title: 'Change1', url: 'https://change1'),
          double('C4', labels: %w(change::in-progress C4))
        ]
      end

      context 'with deployment scope' do
        it { is_expected.not_to be_fine }
      end

      context 'with feature_flag scope' do
        let(:scope) { :feature_flag }

        it { is_expected.to be_fine }
      end
    end

    context 'when there are `blocks feature-flags` issues' do
      let(:issues) do
        [
          double('blocks_feature_flags', labels: ['change::in-progress', 'blocks feature-flags'], title: 'Change1', url: 'https://change1'),
          double('C4', labels: %w(change::in-progress C4))
        ]
      end

      context 'with deployment scope' do
        it 'is fine' do
          expect(ReleaseTools::GitlabClient)
            .to receive(:issues)
            .with(described_class::PRODUCTION, { state: 'opened', labels: 'change::in-progress' })

          expect(check).to be_fine
        end
      end

      context 'with feature_flag scope' do
        let(:scope) { :feature_flag }

        it { is_expected.not_to be_fine }
      end
    end
  end
end
