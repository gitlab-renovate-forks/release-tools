# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::Checks::GitlabDeploymentHealth do
  let(:service1) { ReleaseTools::Prometheus::ServiceHealth::Service.new('api', 'main', true) }
  let(:service2) { ReleaseTools::Prometheus::ServiceHealth::Service.new('api', 'cny', true) }
  let(:services) { [service1, service2] }

  subject(:check) { described_class.new }

  before do
    promql_checker = double(services: services)
    allow(ReleaseTools::Prometheus::ServiceHealth).to receive(:new).and_return(promql_checker)
  end

  context 'when all the services are fine' do
    describe '#fine?' do
      it { is_expected.to be_fine }
    end
  end

  context 'when there are no services' do
    let(:services) { [] }

    describe '#fine?' do
      it { is_expected.not_to be_fine }
    end

    describe '#to_slack_blocks' do
      let(:heading) { ":crystal_ball: GitLab Deployment Health Status - <https://dashboards.gitlab.net/explore?schemaVersion=1&orgId=1&panes=%7B%22abc%22%3A%7B%22datasource%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%2C%22queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22expr%22%3A%22gitlab_deployment_health%3Aservice%7Benv%3D%5C%22gprd%5C%22%7D%22%2C%22range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22uid%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D%2C%22editorMode%22%3A%22code%22%7D%5D%2C%22range%22%3A%7B%22from%22%3A%22now-6h%22%2C%22to%22%3A%22now%22%7D%7D%7D|overview>" }

      it 'has a field' do
        elements = check.to_slack_blocks[1][:elements]

        expect(elements).not_to be_empty
        expect(elements[0][:text]).to eq("#{check.failure_icon} no data")
      end

      it 'has a link to grafana' do
        text = check.to_slack_blocks[0][:text][:text]

        expect(text).to eq(heading)
      end
    end

    describe '#to_issue_body' do
      let(:heading) { ":crystal_ball: GitLab Deployment Health Status - [overview](https://dashboards.gitlab.net/explore?schemaVersion=1&orgId=1&panes=%7B%22abc%22%3A%7B%22datasource%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%2C%22queries%22%3A%5B%7B%22refId%22%3A%22A%22%2C%22expr%22%3A%22gitlab_deployment_health%3Aservice%7Benv%3D%5C%22gprd%5C%22%7D%22%2C%22range%22%3Atrue%2C%22instant%22%3Atrue%2C%22datasource%22%3A%7B%22type%22%3A%22prometheus%22%2C%22uid%22%3A%22e58c2f51-20f8-4f4b-ad48-2968782ca7d6%22%7D%2C%22editorMode%22%3A%22code%22%7D%5D%2C%22range%22%3A%7B%22from%22%3A%22now-6h%22%2C%22to%22%3A%22now%22%7D%7D%7D)" }

      it 'has a link to grafana' do
        expect(check.to_issue_body).to include(heading)
      end
    end
  end

  context 'when a service is not fine' do
    let(:service1) { ReleaseTools::Prometheus::ServiceHealth::Service.new('api', 'main', false) }

    describe '#fine?' do
      it { is_expected.not_to be_fine }
    end
  end
end
