# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Monthly::Finalize::UpdateProtectedBranches do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:current_version) { ReleaseTools::Version.new('16.1.0') }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  describe '#execute' do
    subject(:update_branches) { described_class.new.execute }

    before do
      allow(ReleaseTools::Versions).to receive(:current)
        .and_return([current_version])
    end

    it 'updates the protected branches for gitlab-ee, omnibus-gitlab, and CNG' do
      expect(client).to receive(:unprotect_branch).once.with(
        ReleaseTools::Project::GitlabEe, '16-0-stable-ee'
      )

      expect(client).to receive(:unprotect_branch).once.with(
        ReleaseTools::Project::OmnibusGitlab, '16-0-stable'
      )

      expect(client).to receive(:unprotect_branch).once.with(
        ReleaseTools::Project::CNGImage, '16-0-stable'
      )

      expect(client).to receive(:protect_branch).once.with(
        ReleaseTools::Project::GitlabEe,
        '16-1-stable-ee',
        merge_access_level: described_class::MAINTAINER_ACCESS_LEVEL,
        allowed_to_merge: anything,
        allowed_to_push: anything,
        code_owner_approval_required: false
      )

      expect(client).to receive(:protect_branch).once.with(
        ReleaseTools::Project::OmnibusGitlab,
        '16-1-stable',
        merge_access_level: described_class::MAINTAINER_ACCESS_LEVEL,
        allowed_to_merge: anything,
        allowed_to_push: anything,
        code_owner_approval_required: false
      )

      expect(client).to receive(:protect_branch).once.with(
        ReleaseTools::Project::CNGImage,
        '16-1-stable',
        merge_access_level: described_class::MAINTAINER_ACCESS_LEVEL,
        allowed_to_merge: anything,
        allowed_to_push: anything,
        code_owner_approval_required: false
      )

      expect(notifier).to receive(:send_notification)

      without_dry_run do
        update_branches
      end
    end

    context 'when something goes wrong' do
      it 'raises an error' do
        expect(ReleaseTools::Versions).to receive(:current).and_raise
        expect(notifier).to receive(:send_notification)

        expect { update_branches }.to raise_error(StandardError)
      end
    end

    context 'dry run' do
      it 'does not update the branches' do
        expect(client).not_to receive(:unprotect_branch)
        expect(client).not_to receive(:protect_branch)
        expect(notifier).to receive(:send_notification)

        update_branches
      end
    end
  end
end
