# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Metrics::TrackReleaseTagged do
  describe '#execute' do
    let(:instance) { described_class.new(version) }
    let(:version) { ReleaseTools::Version.new('1.2.3') }
    let(:fake_metrics) { instance_double(ReleaseTools::Metrics::Client) }
    let(:critical_patch_release?) { false }

    before do
      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(fake_metrics)

      allow(ReleaseTools::SharedStatus)
        .to receive(:critical_patch_release?)
        .and_return(critical_patch_release?)
    end

    context 'for monthly release' do
      let(:version) { ReleaseTools::Version.new('1.2.0') }

      it 'increments the package tagging metric for monthly releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", { labels: 'monthly,no' })

        instance.execute
      end
    end

    context 'for patch release with no security fixes' do
      let(:version) { ReleaseTools::Version.new('1.2.3') }

      it 'increments the package tagging metric for patch releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", { labels: 'patch,no' })

        instance.execute
      end
    end

    context 'for rc release' do
      let(:version) { ReleaseTools::Version.new('1.2.0-rc1') }

      it 'increments the package tagging metric for rc releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", { labels: 'rc,no' })

        instance.execute
      end
    end

    context 'for patch release with security fixes' do
      it 'increments the package tagging metric for regular patch releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", { labels: 'patch,regular' })

        ClimateControl.modify(SECURITY: 'true') do
          instance.execute
        end
      end
    end

    context 'for critical patch release' do
      let(:critical_patch_release?) { true }

      it 'increments the package tagging metric for critical patch releases' do
        expect(fake_metrics).to receive(:inc).with("packages_tagging_total", { labels: 'patch,critical' })

        instance.execute
      end
    end
  end
end
