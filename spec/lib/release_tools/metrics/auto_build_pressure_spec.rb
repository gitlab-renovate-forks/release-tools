# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::AutoBuildPressure do
  subject(:service) { described_class.new }

  let(:delivery_metrics) do
    instance_double(ReleaseTools::Metrics::Client)
  end

  before do
    allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(delivery_metrics)

    allow(ReleaseTools::ProductVersion).to receive(:last_auto_deploy).and_return(build(:product_version))

    allow(ReleaseTools::GitlabClient).to receive(:compare).and_return(build(:comparison, commits: [build(:commit)]))
  end

  describe '#execute' do
    it 'performs one comparison per project' do
      expect(ReleaseTools::GitlabClient).to receive(:compare).exactly(8).times
      described_class::PROJECTS.each do |project|
        expect(delivery_metrics).to receive(:set).with(described_class::METRIC, 1, labels: project.metadata_project_name)
      end

      service.execute
    end

    context 'when compare API returns error' do
      before do
        allow(ReleaseTools::GitlabClient).to receive(:compare).and_raise(gitlab_error(:NotFound))
      end

      it 'counts 0' do
        expect(ReleaseTools::GitlabClient).to receive(:compare).exactly(8 * 3).times
        described_class::PROJECTS.each do |project|
          expect(delivery_metrics).to receive(:set).with(described_class::METRIC, 0, labels: project.metadata_project_name)
        end

        service.execute
      end
    end
  end
end
