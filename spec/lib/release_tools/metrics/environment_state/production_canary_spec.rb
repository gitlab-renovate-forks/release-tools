# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::EnvironmentState::ProductionCanary do
  environment = 'gprd'
  stage = 'cny'

  include_context 'environment state transition context', environment, stage

  it_behaves_like 'environment state transition', environment, stage # Stubs request to Prometheus for `auto_deploy_environment_state` metric

  describe '#valid_transition?' do
    subject(:perform_event) { service.perform_event(event) }

    let!(:service) { described_class.new }

    where(:locked_state_metric_value, :ready_state_metric_value, :baking_time_metric_value, :awaiting_promotion, :event, :validity, :new_state) do
      [
        ['0', '0', '0', '0', 'success', true, 'ready'],
        ['0', '0', '0', '0', 'start', true, 'locked'],
        ['1', '0', '0', '0', 'success', true, 'ready'],
        ['1', '0', '0', '0', 'start', true, 'locked'],
        ['0', '1', '0', '0', 'start', true, 'locked'],
        ['0', '1', '0', '0', 'success', false, 'ready'],
        ['1', '0', '0', '0', 'bake', true, 'baking_time'],
        ['0', '1', '0', '0', 'bake', false, 'ready'],
        ['0', '0', '1', '0', 'baking_complete', true, 'ready']
      ]
    end

    with_them do
      it "returns validity" do
        valid = service.perform_event(event)

        expect(valid).to eq(validity)
        expect(service.current_state).to eq(new_state)
      end
    end
  end
end
