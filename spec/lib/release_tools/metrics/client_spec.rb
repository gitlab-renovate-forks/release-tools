# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::Client do
  let(:client) { instance_double(HTTP::Client) }

  before do
    allow(HTTP).to receive(:headers).and_return(client)
  end

  describe '#reset' do
    it 'sends a DELETE request' do
      metric = 'my_metric'

      expect(client).to receive(:delete)
                          .with("#{described_class::GATEWAY}/api/#{metric}")
                          .and_return(double(:response, status: HTTP::Response::Status.new(200)))

      without_dry_run { subject.reset(metric) }
    end

    context 'with dry run' do
      it 'does nothing' do
        metric = 'my_metric'

        expect(client).not_to receive(:delete)

        subject.reset(metric)
      end
    end
  end

  describe '#set' do
    it 'sends a POST request' do
      metric = 'my_metric'

      expect(client)
        .to receive(:post)
        .with(
          "#{described_class::GATEWAY}/api/#{metric}/set",
          { form: { labels: "", value: 1 } }
        )
        .and_return(double(:response, status: HTTP::Response::Status.new(200)))

      without_dry_run { subject.set(metric, 1) }
    end

    context 'with dry run' do
      it 'does nothing' do
        metric = 'my_metric'

        expect(client).not_to receive(:post)

        subject.set(metric, 1)
      end
    end
  end
end
