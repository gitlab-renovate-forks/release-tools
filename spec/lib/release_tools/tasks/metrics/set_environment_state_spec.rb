# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::Metrics::SetEnvironmentState do
  subject(:service) { described_class.new(environment, event) }

  let(:delivery_metrics) do
    instance_double(ReleaseTools::Metrics::Client)
  end

  let(:environment) { 'gstg-cny' }
  let(:event) { 'start' }
  let(:env) { 'gstg' }
  let(:stage) { 'cny' }

  let(:staging_validator) { instance_spy(ReleaseTools::Metrics::EnvironmentState::Staging) }
  let(:production_canary_validator) { instance_spy(ReleaseTools::Metrics::EnvironmentState::ProductionCanary) }
  let(:production_validator) { instance_spy(ReleaseTools::Metrics::EnvironmentState::Production) }

  let(:staging_canary_validator) do
    instance_spy(ReleaseTools::Metrics::EnvironmentState::StagingCanary, environment: 'gstg', stage: 'cny')
  end

  describe '#execute' do
    before do
      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(delivery_metrics)
      allow(ReleaseTools::GitlabOpsClient).to receive(:edit_resource_group)

      {
        ReleaseTools::Metrics::EnvironmentState::StagingCanary => staging_canary_validator,
        ReleaseTools::Metrics::EnvironmentState::Staging => staging_validator,
        ReleaseTools::Metrics::EnvironmentState::ProductionCanary => production_canary_validator,
        ReleaseTools::Metrics::EnvironmentState::Production => production_validator
      }.each do |klass, validator|
        allow(klass)
          .to receive(:new)
          .and_return(validator)
      end

      %w[ready locked awaiting_promotion baking_time].each do |state|
        allow(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_environment_state',
            0,
            { labels: "#{state},#{env},#{stage}" }
          )
      end
    end

    it 'sets metric' do
      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:edit_resource_group)
        .with(resource_group_key: "set-environment-state-metric-gstg-cny", process_mode: 'unordered')

      expect(staging_canary_validator)
        .to receive_messages(perform_event: true, current_state: 'locked')

      expect(delivery_metrics)
        .to receive(:set)
        .with(
          'auto_deploy_environment_state',
          1,
          { labels: "locked,#{env},#{stage}" }
        )

      %w[ready baking_time awaiting_promotion].each do |state|
        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_environment_state',
            0,
            { labels: "#{state},#{env},#{stage}" }
          )
      end

      without_dry_run { service.execute }
    end

    it 'calls EnvironmentStateTransition.perform_event and sets metric if valid' do
      expect(ReleaseTools::Metrics::EnvironmentState::StagingCanary)
        .to receive(:new)

      expect(staging_canary_validator)
        .to receive_messages(perform_event: true, current_state: 'locked')

      expect(delivery_metrics).to receive(:set)

      without_dry_run { service.execute }
    end

    it 'does not set metric if EnvironmentStateTransition.perform_event returns false' do
      expect(ReleaseTools::Metrics::EnvironmentState::StagingCanary)
        .to receive(:new)

      expect(staging_canary_validator)
        .to receive(:perform_event)
        .and_return(false)

      expect(delivery_metrics).not_to receive(:set)

      without_dry_run { service.execute }
    end

    context 'when resource group API raises error' do
      before do
        allow(ReleaseTools::GitlabOpsClient)
          .to receive(:edit_resource_group)
          .and_raise(gitlab_error(:NotFound))
      end

      it 'catches error' do
        expect(ReleaseTools::GitlabOpsClient)
          .to receive(:edit_resource_group)
          .with(resource_group_key: "set-environment-state-metric-gstg-cny", process_mode: 'unordered')

        expect(staging_canary_validator)
          .to receive_messages(perform_event: true, current_state: 'locked')

        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_environment_state',
            1,
            { labels: "locked,#{env},#{stage}" }
          )

        expect { without_dry_run { service.execute } }.not_to raise_error(Gitlab::Error::NotFound)
      end
    end

    context 'when in dry_run mode' do
      it 'does not set metrics' do
        expect(ReleaseTools::Metrics::EnvironmentState::StagingCanary).to receive(:new)
        expect(staging_canary_validator).to receive(:perform_event)
        expect(delivery_metrics).not_to receive(:set)

        service.execute
      end
    end

    context 'with unexpected environment' do
      let(:environment) { 'unexpected' }

      it 'raises error' do
        without_dry_run do
          expect { service.execute }.to raise_error(described_class::UnexpectedEnvironmentError)
        end
      end
    end

    context 'with expected environments' do
      before do
        allow(delivery_metrics).to receive(:set)
      end

      %w[gstg gstg-cny gprd gprd-cny].each do |env|
        let(:environment) { env }

        it 'does not raise error' do
          without_dry_run do
            expect { service.execute }.not_to raise_error
          end
        end
      end
    end
  end
end
