# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::Components::Updater do
  subject(:updater_class) do
    dummy_class.new(token)
  end

  let(:token) { 'a token' }

  let(:dummy_class) do
    Class.new do
      include ReleaseTools::Tasks::Helper
      include SemanticLogger::Loggable
      include ReleaseTools::Tasks::Components::Updater

      def initialize(token = nil)
        @gitlab_bot_token = token || ENV.fetch(ENV_TOKEN)
      end

      def send_slack_notification
        # no-op
      end

      def project
        ReleaseTools::Project::Kas
      end

      def source_branch
        'release-tools/updater-class'
      end

      def merge_request
        @merge_request ||= ReleaseTools::UpdateKasMergeRequest.new(source_branch: source_branch)
      end
    end
  end

  describe '#execute' do
    context 'when there are no changes' do
      it 'only check if token rotation is needed' do
        expect(updater_class).to receive(:changed?).and_return(false)

        expect(updater_class).not_to receive(:merge_request)
        expect(updater_class).not_to receive(:ensure_source_branch_exists)
        expect(updater_class).not_to receive(:create_or_show_merge_request)
        expect(ReleaseTools::Services::UpdateComponentService)
          .not_to receive(:new)
        expect(ReleaseTools::Services::AutoMergeService).not_to receive(:new)
        expect(updater_class).to receive(:rotate_token_if_needed)

        without_dry_run do
          updater_class.execute
        end
      end
    end

    context 'when rotating the token' do
      context 'when rotation is needed' do
        it 'updates the project CI variables' do
          expect(updater_class).to receive(:changed?).and_return(false)

          rotator = instance_double(ReleaseTools::Services::ProjectAccessTokenRotator, rotated?: true)
          expect(ReleaseTools::Services::ProjectAccessTokenRotator).to receive(:new).and_return(rotator)
          expect(rotator).to receive(:execute)
          expect(rotator).to receive(:update_ci_var).with(described_class::ENV_TOKEN)

          without_dry_run do
            updater_class.execute
          end
        end
      end

      context 'when rotation is not needed' do
        it 'does not update the CI variables' do
          expect(updater_class).to receive(:changed?).and_return(false)

          rotator = instance_double(ReleaseTools::Services::ProjectAccessTokenRotator, rotated?: false)
          expect(ReleaseTools::Services::ProjectAccessTokenRotator).to receive(:new).and_return(rotator)
          expect(rotator).to receive(:execute)
          expect(rotator).not_to receive(:update_ci_var)

          without_dry_run do
            updater_class.execute
          end
        end
      end
    end

    context 'when the merge request already exists' do
      context 'when the merge request is mergeable' do
        %w[mergeable unchecked not_approved].each do |merge_status|
          it 'sets auto merge for the merge request' do
            expect(updater_class).to receive(:changed?).and_return(true)
            merge_request = instance_double(ReleaseTools::UpdateKasMergeRequest,
                                            exists?: true, notifiable?: true, url: 'an url',
                                            detailed_merge_status: merge_status,
                                            merge_when_pipeline_succeeds?: false)
            allow(updater_class).to receive(:merge_request).and_return(merge_request)

            expect(updater_class).to receive(:create_merge_request_and_set_auto_merge)
            expect(updater_class).not_to receive(:notify_stale_merge_request)

            without_dry_run do
              updater_class.execute
            end
          end
        end
      end

      context 'when the merge request is not mergeable' do
        context 'when the merge request is notifiable' do
          it 'sends a notification if MR is stale and attempts token rotation' do
            expect(updater_class).to receive(:changed?).and_return(true)
            merge_request = instance_double(ReleaseTools::UpdateKasMergeRequest,
                                            exists?: true, notifiable?: true, url: 'an url',
                                            detailed_merge_status: 'ci_still_running',
                                            merge_when_pipeline_succeeds?: false)
            allow(updater_class).to receive(:merge_request).and_return(merge_request)

            expect(updater_class).not_to receive(:create_merge_request_and_set_auto_merge)
            expect(updater_class).to receive(:send_slack_notification)
            expect(merge_request).to receive(:mark_as_stale)
            expect(updater_class).to receive(:rotate_token_if_needed)

            without_dry_run do
              updater_class.execute
            end
          end
        end

        context 'when the merge request is not notifiable' do
          it 'only attempts token rotation' do
            expect(updater_class).to receive(:changed?).and_return(true)
            merge_request = instance_double(ReleaseTools::UpdateKasMergeRequest,
                                            exists?: true, notifiable?: false, url: 'an url',
                                            detailed_merge_status: 'ci_still_running',
                                            merge_when_pipeline_succeeds?: false)
            allow(updater_class).to receive(:merge_request).and_return(merge_request)

            expect(updater_class).not_to receive(:create_merge_request_and_set_auto_merge)
            expect(updater_class).not_to receive(:notify_stale_merge_request)
            expect(updater_class).to receive(:rotate_token_if_needed)

            without_dry_run do
              updater_class.execute
            end
          end
        end
      end
    end

    context 'when the merge request does not exist' do
      it 'makes sure the source branch exists, creates the merge request, updates kas versions, auto-merge, and verifies token rotation' do
        expect(updater_class).to receive(:changed?).and_return(true)
        merge_request = instance_double(ReleaseTools::UpdateKasMergeRequest, exists?: false, project: double)
        allow(updater_class).to receive(:merge_request).and_return(merge_request)

        expect(updater_class).to receive(:ensure_source_branch_exists)
        expect(updater_class).to receive(:create_or_show_merge_request).with(merge_request)

        update_component_service = instance_double(ReleaseTools::Services::UpdateComponentService)
        expect(ReleaseTools::Services::UpdateComponentService)
          .to receive(:new)
          .with(ReleaseTools::Project::Kas, updater_class.source_branch, { skip_ci: true })
          .and_return(update_component_service)

        commit = instance_double(ReleaseTools::Commits)
        expect(update_component_service).to receive(:execute).and_return(commit)

        auto_merge_service = instance_double(ReleaseTools::Services::AutoMergeService)
        expect(ReleaseTools::Services::AutoMergeService).to receive(:new).with(merge_request, { token: token, commit: commit }).and_return(auto_merge_service)
        expect(auto_merge_service).to receive(:execute)

        expect(updater_class).to receive(:rotate_token_if_needed)

        without_dry_run do
          updater_class.execute
        end
      end
    end
  end

  describe '#ensure_source_branch_exist' do
    it 'delegates to GitlabClient.find_or_create_branch' do
      branch = instance_double(ReleaseTools::Branch)
      expect(ReleaseTools::GitlabClient)
        .to receive(:find_or_create_branch)
        .with(updater_class.source_branch, 'master', updater_class.class::TARGET_PROJECT)
        .and_return(branch)

      without_dry_run do
        expect(updater_class.ensure_source_branch_exists).to eq(branch)
      end
    end
  end

  describe '#changed?' do
    it 'checks for changes in master' do
      changed = double('changed?')
      expect(ReleaseTools::Services::UpdateComponentService)
        .to receive(:new)
        .with(ReleaseTools::Project::Kas, 'master')
        .and_return(instance_double(ReleaseTools::Services::UpdateComponentService, changed?: changed))

      expect(updater_class.changed?).to eq(changed)
    end
  end

  describe '#merge_request' do
    it 'is a merge request from source_branch' do
      expect(ReleaseTools::UpdateKasMergeRequest)
        .to receive(:new)
        .with({ source_branch: updater_class.source_branch })

      updater_class.merge_request
    end
  end
end
