# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

RSpec.describe ReleaseTools::Tasks::Release::Issue do
  subject(:instance) { described_class.new(version) }

  describe '#execute' do
    context 'with monthly version' do
      let(:version) { ReleaseTools::Version.new('14.1') }

      it 'calls create_or_show_issue' do
        expect(instance).to receive(:create_or_show_issue).with(instance_of(ReleaseTools::MonthlyIssue))

        instance.execute
      end
    end

    context 'with patch version' do
      let(:version) { ReleaseTools::Version.new('14.1.9') }

      it 'calls create_or_show_issue' do
        expect(instance).to receive(:create_or_show_issue).with(instance_of(ReleaseTools::PatchRelease::Issue))

        instance.execute
      end
    end
  end

  describe '#release_issue' do
    context 'with monthly version' do
      let(:version) { ReleaseTools::Version.new('14.1') }

      it 'returns MonthlyIssue' do
        expect(instance.release_issue).to eq(ReleaseTools::MonthlyIssue.new(version: version))
      end
    end

    context 'with patch version' do
      let(:version) { ReleaseTools::Version.new('14.1.9') }

      it 'returns PatchRelease::Issue' do
        expect(instance.release_issue).to eq(ReleaseTools::PatchRelease::Issue.new(version: version))
      end
    end

    context 'with nil version' do
      let(:version) { nil }

      it 'returns PatchRelease::Issue with the current version' do
        current_version = ReleaseTools::Version.new('16.11.1')

        versions = [
          current_version,
          ReleaseTools::Version.new('16.10.5'),
          ReleaseTools::Version.new('16.9.7')
        ]

        allow(ReleaseTools::Versions)
          .to receive(:next_versions)
          .and_return(versions)

        expect(instance.release_issue).to eq(ReleaseTools::PatchRelease::Issue.new(version: current_version))
      end
    end
  end
end
