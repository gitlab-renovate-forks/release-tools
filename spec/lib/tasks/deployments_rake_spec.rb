# frozen_string_literal: true

require 'rake_helper'

RSpec.describe 'deployments', :rake do
  describe 'blockers_metrics', task: 'deployments:blockers_metrics' do
    it 'creates deployment blockers metrics' do
      expect(ReleaseTools::Deployments::BlockersMetrics).to receive(:new)
        .and_return(instance_double(ReleaseTools::Deployments::BlockersMetrics, execute: true))

      task.invoke
    end
  end

  describe 'blockers_annotate', task: 'deployments:blockers_annotate' do
    it 'reviews all deployment blockers issues and creates Grafana annotations' do
      expect(ReleaseTools::Deployments::BlockerAnnotations).to receive(:new)
        .and_return(instance_double(ReleaseTools::Deployments::BlockerAnnotations, execute: true))

      task.invoke
    end
  end
end
