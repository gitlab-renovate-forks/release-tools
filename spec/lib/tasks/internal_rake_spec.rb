# frozen_string_literal: true

require 'rake_helper'

describe 'internal tasks', :rake do
  describe 'issue', task: 'internal:issue' do
    let(:issue) { instance_double(ReleaseTools::InternalRelease::Issue) }
    let(:version) { ReleaseTools::Version.new('15.2.6') }

    before do
      allow(ReleaseTools::InternalRelease::Issue)
        .to receive(:new).and_return(issue)

      allow(issue).to receive_messages(title: 'title', description: 'description')

      allow(ReleaseTools::GitlabReleasesGemClient)
        .to receive(:latest_patch_for_version).and_return(nil)
    end

    context 'when no arguments are provided' do
      it 'calls the InternalRelease::Issue class with nil values' do
        expect(ReleaseTools::InternalRelease::Issue).to receive(:new)
          .with(iteration: nil)
          .and_return(issue)

        task.invoke
      end
    end

    context 'when only iteration is provided' do
      it 'calls the InternalRelease::Issue class with the iteration value' do
        expect(ReleaseTools::InternalRelease::Issue).to receive(:new)
          .with(iteration: '1')
          .and_return(issue)

        task.invoke('1')
      end
    end

    context 'when both iteration and version are provided' do
      it 'calls the InternalRelease::Issue class with both parameters' do
        expect(ReleaseTools::GitlabReleasesGemClient)
          .to receive(:latest_patch_for_version)
          .with('15.2').and_return('15.2.6')

        expect(ReleaseTools::InternalRelease::Issue).to receive(:new)
          .with(iteration: '1', version: version)
          .and_return(issue)

        task.invoke('1', '15.2')
      end
    end

    context 'when only version is provided' do
      it 'calls the InternalRelease::Issue class with nil iteration and the version parameter' do
        expect(ReleaseTools::GitlabReleasesGemClient).to receive(:latest_patch_for_version)
          .with('15.2').and_return('15.2.6')

        expect(ReleaseTools::InternalRelease::Issue).to receive(:new)
          .with(iteration: nil, version: version)
          .and_return(issue)

        task.invoke(nil, '15.2')
      end
    end
  end

  describe 'prepare:start', task: 'internal:prepare:start' do
    it 'starts the prepare stage' do
      expect(ReleaseTools::Slack::ReleasePipelineStartNotifier).to receive(:new)
        .with(release_type: :internal, stage: :start)
        .and_return(instance_double(ReleaseTools::Slack::ReleasePipelineStartNotifier, execute: true))

      task.invoke
    end
  end

  describe 'prepare:check_component_branch_pipeline_status',
           task: 'internal:prepare:check_component_branch_pipeline_status' do
    let(:versions) do
      [
        ReleaseTools::Version.new('15.2.1-internal1'),
        ReleaseTools::Version.new('15.1.2-internal1')
      ]
    end

    it 'ensures stable branches are green with the versions from VERSIONS env var' do
      versions_string = '15.2.1-internal1 15.1.2-internal1'

      ClimateControl.modify VERSIONS: versions_string do
        expect(ReleaseTools::InternalRelease::Prepare::ComponentBranchVerifier).to receive(:new)
          .with(versions)
          .and_return(
            instance_double(
              ReleaseTools::InternalRelease::Prepare::ComponentBranchVerifier, execute: true
            )
          )

        task.invoke
      end
    end
  end

  describe 'release:start', task: 'internal:release:start' do
    it 'starts the release stage' do
      expect(ReleaseTools::Slack::ReleasePipelineStartNotifier).to receive(:new).with(release_type: :internal, stage: :release)
        .and_return(instance_double(ReleaseTools::Slack::ReleasePipelineStartNotifier, execute: true))

      task.invoke
    end
  end

  describe 'release:build_package', task: 'internal:release:build_package' do
    let(:iteration) { 5 }
    let(:version) { ReleaseTools::Version.new('15.2.1-internal0') }

    it 'performs the steps required to build a private package' do
      release = instance_double(ReleaseTools::InternalRelease::Release)
      allow(ReleaseTools::InternalRelease::Release).to receive(:new).and_return(release)

      expect(release).to receive(:execute)

      expect(ReleaseTools::InternalRelease::Release).to receive(:new)
        .with(version: version, internal_number: iteration)
        .and_return(release)

      ClimateControl.modify ITERATION: iteration.to_s do
        task.invoke('15.2.1-internal0')
      end
    end
  end

  describe 'release:generate_dynamic_pipeline', task: 'internal:release:generate_dynamic_pipeline' do
    let(:versions) { ['1.0.1', '1.1.1'] }
    let(:expected_yaml) do
      {
        ".with-bundle" => {
          "before_script" => [
            "bundle install --jobs=$(nproc) --retry=3 --quiet"
          ]
        },
        "stages" => versions.map { |version| "internal_release_release:build_package:#{version}" },
        "internal_release_release_build_package:1.0.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "internal_release_release:build_package:1.0.1",
          "script" => [
            "bundle exec rake security:publish[1.0.1]"
          ],
          "extends" => [
            ".with-bundle",
            ".common-ci-tokens"
          ],
          "needs" => []
        },
        "internal_release_release_build_package:1.1.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "internal_release_release:build_package:1.1.1",
          "script" => [
            "bundle exec rake security:publish[1.1.1]"
          ],
          "extends" => [
            ".with-bundle",
            ".common-ci-tokens"
          ],
          "needs" => []
        }
      }
    end

    let(:release_jobs) { instance_double(ReleaseTools::InternalRelease::ReleaseDynamicPipeline) }
    let(:coordinator) { instance_double(ReleaseTools::InternalRelease::Coordinator) }

    before do
      allow(ReleaseTools::InternalRelease::ReleaseDynamicPipeline).to receive(:new).with(versions).and_return(release_jobs)
      allow(release_jobs).to receive(:generate).and_return(expected_yaml.to_yaml)

      allow(File).to receive(:write)

      allow(ReleaseTools::InternalRelease::Coordinator).to receive(:new).and_return(coordinator)
      allow(coordinator).to receive(:versions).and_return(versions)
    end

    it 'calls generate method on the ReleaseTools::InternalRelease::ReleaseDynamicPipeline instance' do
      expect(ReleaseTools::InternalRelease::ReleaseDynamicPipeline).to receive(:new).with(versions).and_return(release_jobs)
      expect(release_jobs).to receive(:generate)
      task.invoke
    end

    it 'writes the generated YAML content to dynamic-gitlab-ci.yml' do
      expect(File).to receive(:write).with('dynamic-gitlab-ci.yml', release_jobs.generate)
      task.invoke
    end
  end

  describe 'verify:start', task: 'internal:verify:start' do
    it 'starts the verify stage' do
      expect(ReleaseTools::Slack::ReleasePipelineStartNotifier).to receive(:new).with(release_type: :internal, stage: :verify)
        .and_return(instance_double(ReleaseTools::Slack::ReleasePipelineStartNotifier, execute: true))

      task.invoke
    end
  end

  describe 'verify:package_availability', task: 'internal:verify:package_availability' do
    let(:versions) do
      [
        ReleaseTools::Version.new('15.2.1-internal0'),
        ReleaseTools::Version.new('15.1.2-internal0')
      ]
    end

    it 'checks if the package has been built and available on the pre-release channel' do
      coordinator = instance_double(ReleaseTools::InternalRelease::Coordinator, versions: versions)
      allow(ReleaseTools::InternalRelease::Coordinator).to receive(:new).and_return(coordinator)

      check = instance_double(ReleaseTools::Services::OmnibusPackages::Tagging)
      allow(ReleaseTools::Services::OmnibusPackages::Tagging).to receive(:new).and_return(check)

      expect(check).to receive(:execute).twice

      expect(ReleaseTools::Services::OmnibusPackages::Tagging).to receive(:new)
        .with(version: versions[0], package_type: :internal)
        .and_return(check)

      expect(ReleaseTools::Services::OmnibusPackages::Tagging).to receive(:new)
        .with(version: versions[1], package_type: :internal)
        .and_return(check)

      task.invoke
    end
  end

  describe 'finalize:start', task: 'internal:finalize:start' do
    it 'starts the finalize stage' do
      expect(ReleaseTools::Slack::ReleasePipelineStartNotifier).to receive(:new).with(release_type: :internal, stage: :finalize)
        .and_return(instance_double(ReleaseTools::Slack::ReleasePipelineStartNotifier, execute: true))

      task.invoke
    end
  end

  describe 'finalize:notify_releases', task: 'internal:finalize:notify_releases' do
    let(:iteration) { 1 }

    it 'updates the internal release task issue that the internal package is available' do
      ClimateControl.modify ITERATION: iteration.to_s do
        expect(ReleaseTools::InternalRelease::NotifyPackageAvailability).to receive(:new)
          .with(iteration)
          .and_return(
            instance_double(
              ReleaseTools::InternalRelease::NotifyPackageAvailability, execute: true
            )
          )

        task.invoke
      end
    end
  end
end
