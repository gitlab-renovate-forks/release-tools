# frozen_string_literal: true

require 'rake_helper'

describe 'auto deploy tasks', :rake do
  around do |ex|
    ClimateControl.modify(AUTO_DEPLOY_BRANCH: '1-2-auto-deploy') do
      ex.run
    end
  end

  describe 'start', task: 'auto_deploy:start' do
    it 'calls AutoDeploy::Start with version' do
      expect(ReleaseTools::Tasks::AutoDeploy::Start)
        .to receive(:new)
        .with(product_version: instance_of(ReleaseTools::ProductVersion))
        .and_return(instance_double(ReleaseTools::Tasks::AutoDeploy::Start, execute: true))

      ClimateControl.modify(AUTO_DEPLOY_TAG: '17.6.202410302205') do
        task.invoke
      end
    end

    it 'does nothing if not deployment pipeline' do
      expect(ReleaseTools::Tasks::AutoDeploy::Start).not_to receive(:new)
      expect(ReleaseTools::AutoDeploy::Tag).not_to receive(:current)

      ClimateControl.modify(AUTO_DEPLOY_TAG: nil, CI_COMMIT_TAG: nil) do
        task.invoke
      end
    end
  end

  describe 'deploy_version', task: 'auto_deploy:deploy_version' do
    it 'calls AutoDeploy::DeploymentPipeline::Service' do
      expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Service)
        .to receive(:new)
        .with(strategy: :version, version: '17.6.202410302205', force_version_deployment: false)
        .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Service, execute: true))

      task.invoke('17.6.202410302205')
    end

    it 'raises error if version argument is not provided' do
      expect { task.invoke }.to raise_error(RuntimeError, 'version argument is required for this rake task')
    end

    it 'sets force_version_deployment to true if second argument is true' do
      expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Service)
        .to receive(:new)
        .with(strategy: :version, version: '17.6.202410302205', force_version_deployment: true)
        .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Service, execute: true))

      task.invoke('17.6.202410302205', 'true')
    end
  end

  describe 'deploy_latest', task: 'auto_deploy:deploy_latest' do
    it 'calls AutoDeploy::DeploymentPipeline::Service' do
      expect(ReleaseTools::AutoDeploy::DeploymentPipeline::Service)
        .to receive(:new)
        .with(strategy: :latest)
        .and_return(instance_double(ReleaseTools::AutoDeploy::DeploymentPipeline::Service, execute: true))

      task.invoke
    end
  end
end
