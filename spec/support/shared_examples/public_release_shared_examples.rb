# frozen_string_literal: true

RSpec.shared_examples 'public_release#create_target_branch' do
  let(:client) { class_spy(ReleaseTools::GitlabClient) }
  let(:version) { ReleaseTools::Version.new('42.1.0-ee') }

  it 'calls notify_stable_branch_creation' do
    release = described_class.new(version, client: client, commit: 'foo')

    expect(release).to receive(:notify_stable_branch_creation)

    without_dry_run do
      release.create_target_branch
    end
  end

  context 'with dry run enabled' do
    it 'calls notify_stable_branch_creation' do
      release = described_class.new(version, client: client, commit: 'foo')

      expect(release).to receive(:notify_stable_branch_creation)

      release.create_target_branch
    end
  end
end
