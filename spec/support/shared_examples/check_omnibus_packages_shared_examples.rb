# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_examples 'check omnibus packages service' do |packages_status_service|
  subject(:packages_service) { described_class.new(version: version, package_type: package_type) }

  let(:check_service) { instance_double(packages_status_service) }

  shared_examples 'common package checks' do
    before do
      allow(packages_status_service).to receive(:new)
        .with(version, package_type: package_type).and_return(check_service)

      allow(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new)
        .and_return(instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: nil))
    end

    context 'when no packages are found' do
      before do
        allow(check_service).to receive(:execute).and_return({})
      end

      it 'raises an error' do
        expect { packages_service.execute }.to raise_error(described_class::OmnibusPackagesError)
      end
    end

    context 'when all packages succeed' do
      before do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'success' })
      end

      it 'logs success and sends a success notification' do
        expect(packages_service).to receive(:log_success)
        expect(packages_service).to receive(:send_slack_notification).with(:success)

        packages_service.execute
      end
    end

    context 'when any package fails' do
      before do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'failed' })
      end

      it 'logs failure, sends a failed notification, and raises OmnibusPackagesError' do
        expect(packages_service).to receive(:log_failure)
        expect(packages_service).to receive(:send_slack_notification).with(:failed)

        expect { packages_service.execute }
          .to raise_error(described_class::OmnibusPackagesError)
      end
    end

    context 'when packages are still in progress' do
      before do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'in_progress' })
      end

      it 'logs in-progress status and retries' do
        expect(packages_service).to receive(:log_in_progress).exactly(15).times

        expect { packages_service.execute }
          .to raise_error(described_class::OmnibusPackagesInProgressError)
      end
    end

    context 'when retries are exhausted' do
      before do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'in_progress' })

        allow(Retriable).to receive(:with_context)
          .and_raise(described_class::OmnibusPackagesInProgressError)
      end

      it 'sends a failed notification and re-raises the error' do
        expect(packages_service).to receive(:send_slack_notification).with(:failed)

        expect { packages_service.execute }
          .to raise_error(described_class::OmnibusPackagesInProgressError)
      end
    end
  end

  context 'with a tag package type' do
    let(:package_type) { :tag }
    let(:version) { ReleaseTools::Version.new('15.0.0') }

    include_examples 'common package checks'

    context 'for a monthly version' do
      it 'uses the correct job_type and release_type for Slack notification when tagging' do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'success' })

        allow(packages_service).to receive(:action_progressive_tense).and_return('tagging')

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
          hash_including(
            job_type: "Verify package tagging for version 15.0.0",
            release_type: :monthly
          )
        )

        packages_service.execute
      end

      it 'uses the correct job_type and release_type for Slack notification when publishing' do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'success' })

        allow(packages_service).to receive(:action_progressive_tense).and_return('publishing')

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
          hash_including(
            job_type: "Verify package publishing for version 15.0.0",
            release_type: :monthly
          )
        )

        packages_service.execute
      end
    end

    context 'for a patch version' do
      let(:version) { ReleaseTools::Version.new('15.0.1') }

      it 'uses the correct job_type and release_type for Slack notification when tagging' do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'success' })

        allow(packages_service).to receive(:action_progressive_tense).and_return('tagging')

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
          hash_including(
            job_type: "Verify package tagging for version 15.0.1",
            release_type: :patch
          )
        )

        packages_service.execute
      end

      it 'uses the correct job_type and release_type for Slack notification when publishing' do
        allow(check_service).to receive(:execute)
          .and_return({ 'package1' => 'success', 'package2' => 'success' })

        allow(packages_service).to receive(:action_progressive_tense).and_return('publishing')

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
          hash_including(
            job_type: "Verify package publishing for version 15.0.1",
            release_type: :patch
          )
        )

        packages_service.execute
      end
    end
  end

  context 'with an internal package type' do
    let(:package_type) { :internal }
    let(:version) { ReleaseTools::Version.new('15.11.0-internal0') }

    include_examples 'common package checks'

    it 'uses the correct job type for Slack notification' do
      allow(check_service).to receive(:execute)
        .and_return({ 'package1' => 'success' })

      expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
        hash_including(
          job_type: "Verify internal package is built and available for version 15.11.0-internal0",
          release_type: :internal
        )
      )

      packages_service.execute
    end
  end
end
